#include "FileSystem.h"
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>

File::File(string path)
: mPath(path)
{}

string File::getName()
{
  return mPath.substr(mPath.rfind("/"));
}

string File::getPath()
{
  return mPath;
}


Directory::Directory(string path)
: File(path)
{}

vector<Directory> Directory::getSubDirectories()
{
  vector<Directory> result;
  DIR* dp;
  dirent* dirp;
  if ((dp = opendir(mPath.c_str())) == NULL)
  {
    fprintf(stderr, "Error opening in directory <%s>: %d\n", mPath.c_str(), errno);
  }
  else
  {
    while((dirp = readdir(dp))) {
      result.push_back(Directory(dirp->d_name));
    }
    closedir(dp);
  }
  return result;
}

vector<File> Directory::getFiles()
{
  vector<File> result;
  DIR* dp;
  dirent* dirp;
  if ((dp = opendir(mPath.c_str())) == NULL)
  {
    fprintf(stderr, "Error opening in directory <%s>: %d\n", mPath.c_str(), errno);
  }
  else
  {
    while((dirp = readdir(dp))) {
      result.push_back(File(dirp->d_name));
    }
    closedir(dp);
  }
  return result;
}
