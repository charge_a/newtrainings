#pragma once
#include <string>
#include <vector>

using namespace std;

class File
{
protected:
  string mPath;
public:
  File(string path);
  string getName();
  string getPath();
};

class Directory : public File
{
public:
  Directory(string path);
  vector<Directory> getSubDirectories();
  vector<File> getFiles();
};