#include "UListView.h"
#include <XMLParser.h>
#include <UViewFactory.h>
#include <MainWindow.h>

UListView::UListView(std::string name, URect frame, int frameFlag)
: UView(name, frame, frameFlag)
{
  mContainer = new UScrollView(name + std::string("_container"), URect(0,0,1,1), 0);
  addSubview(mContainer);
  mBackView = new UView("topView", URect(0,0,0,0), 0);
  mBackView->setBackgroundColor(URect(0,0.2,0,1));
  mContainer->addSubview(mBackView);
  mReferenceItemView = NULL;
  mItemProvider = NULL;
}

UListView* UListView::setItemView(std::string xmlPath)
{
  mXmlPath = xmlPath;
  return this;
}

UListView* UListView::setItemProvider(FUN2(void, UView*, int) provider)
{
  mItemProvider = provider;
  return this;
}

void UListView::render(URect parentRect)
{
  UView::render(parentRect);

  float offY = mContainer->getContentOffsetY() * mContainer->getRenderedFrame().w * VP_H;
  UView* cv = mContainer->getContentView();
  bool needRefresh = false;

  if (mItemViews.size() > 0)
  {
    double itemH = mReferenceItemView->getFrame().w;

    URect frame = mBackView->getFrame();
    frame.w = 10000 * itemH;
    mBackView->setFrame(frame, mBackView->getFrameFlag() | H_ABS);

    int itemsRequired = std::ceil(1.0/mReferenceItemView->getRenderedFrame().w)+1;
    if (itemsRequired < mItemViews.size())
    {
      while(mItemViews.size() != itemsRequired)
      {
        UView* v = mItemViews.front();
        bool isReference = (v == mReferenceItemView);
        cv->removeSubview(v);
        mItemViews.pop_front();

        if (isReference)
        {
          mReferenceItemView = mItemViews.front();
        }
        needRefresh = true;
      }
    }
    else if (itemsRequired > mItemViews.size())
    {
      while(mItemViews.size() != itemsRequired)
      {
        UView* v = XMLParser::parse(mXmlPath, UViewFactory::Instance(), false);
        URect frame = v->getFrame();
        frame.y = (computeIndexForView(mItemViews.back()) + 1) * itemH;
        v->setFrame(frame, v->getFrameFlag() | Y_ABS);
        cv->addSubview(v);
        mItemViews.push_back(v);
        needRefresh = true;
      }
    }

    while(mItemViews.front()->getFrame().y + itemH <= offY)
    {
      UView* v = mItemViews.front();
      mItemViews.pop_front();
        URect frame = v->getFrame();
        frame.y = (computeIndexForView(mItemViews.back()) + 1) * itemH;
        v->setFrame(frame, v->getFrameFlag() | Y_ABS);
      mItemViews.push_back(v);
      needRefresh = true;
    }

    while (mItemViews.front()->getFrame().y > offY)
    {
      UView* v = mItemViews.back();
      mItemViews.pop_back();
        URect frame = v->getFrame();
        frame.y = (computeIndexForView(mItemViews.front()) - 1) * itemH;
        v->setFrame(frame, v->getFrameFlag() | Y_ABS);
      mItemViews.push_front(v);
      needRefresh = true;
    }
  }
  else
  {
    UView* v = XMLParser::parse(mXmlPath, UViewFactory::Instance(), false);
    cv->addSubview(v);
    mItemViews.push_back(v);
    mReferenceItemView = v;
    needRefresh = true;
  }


  if (needRefresh)
  {
    refresh();
  }
}

int UListView::computeIndexForView(UView* v)
{
  double itemH = mReferenceItemView->getFrame().w;
  int index = ceil((v->getFrame().y) / itemH);
  return index;
}

UListView* UListView::refresh()
{
  if (mItemProvider)
  {
    for (UView* v : mItemViews)
    {
      mItemProvider(v, computeIndexForView(v));
    }
  }
}