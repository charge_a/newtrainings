#pragma once
#include <UScrollView.h>
#include <deque>

class UListView : public UView
{
  std::string mXmlPath;
  UScrollView* mContainer;
  std::deque<UView*> mItemViews;
  FUN2(void, UView*, int) mItemProvider;

  UView* mBackView;
  UView* mReferenceItemView;

  int computeIndexForView(UView* v);
public:
  UListView(std::string name, URect frame, int frameFlag);

  UListView* setItemView(std::string xmlPath);
  UListView* setItemProvider(FUN2(void, UView*, int) provider);
  UListView* refresh();

  virtual void render(URect parentRect);

  int itemCount;
};