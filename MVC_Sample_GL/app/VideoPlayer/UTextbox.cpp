#include "UTextbox.h"
#include <DrawingUtil.h>
#include <MainWindow.h>

#define CARET_TIME 40
#define CARET_WIDTH 2

UTextbox::UTextbox(std::string name, URect frame, int frameFlag)
: UView(name, frame, frameFlag)
{
  value = "";
  mPos = 0;
  mCaretTimer = 0;
  mTextChanged = NULL;
}

float UTextbox::getCaretAlpha()
{
  float lowCaretTime = 0.1 * CARET_TIME;
  float highCaretTime = 0.9 * CARET_TIME;
  if (mCaretTimer >= CARET_TIME)
  {
    mCaretTimer = 0;
  }

  if (mCaretTimer < lowCaretTime || mCaretTimer > highCaretTime)
  {
    mCaretTimer += 0.1;
  }
  else
  {
    mCaretTimer += 1;
  }

  float time = std::abs(0.5*CARET_TIME - mCaretTimer);
  return time / (0.5*CARET_TIME);
}

void UTextbox::renderSelf (URect actualRect)
{
  UView::renderSelf(actualRect);

  actualRect.x += 5.0 / VP_W;
  DrawingUtil::Instance().DrawText(actualRect, value, actualRect.w * VP_H, UColor(1), kTextAlignmentLeft);

  if (isFocussed())
  {
    URect textBounds = DrawingUtil::Instance().GetTextBounds(value, actualRect.w * VP_H);
    URect r = actualRect;
    r.x += textBounds.z * (1.0/VP_W) * (VP_H/VP_W);
    r.y += (r.w - textBounds.w * (1.0/VP_H)) / 2.0;
    r.z = CARET_WIDTH/VP_W;
    r.w = textBounds.w * (1.0/VP_H);
    DrawingUtil::Instance().DrawRectangle(r, UColor(1,1,1,getCaretAlpha()), NULL);
  }
}

void UTextbox::render(URect parentRect)
{
  UView::render(parentRect);
}


void UTextbox::onKeyPress(int key, int scancode, int action, int mode)
{

  if (!isFocussed())
  {
    return;
  }

  if (key == GLFW_KEY_BACKSPACE && (action == GLFW_PRESS || action == GLFW_REPEAT))
  {
    mPos --;
    if (mPos < 0)
    {
      mPos = 0;
    }
    value = value.substr(0,mPos);
  }
  else if (action == GLFW_PRESS)
  {
    if (key >= GLFW_KEY_SPACE && key <= GLFW_KEY_Z)
    {
      char c = ' ' + (key - GLFW_KEY_SPACE);
      if (key >= GLFW_KEY_A)
      {
        c += 'a' - 'A';
        if (mode & GLFW_MOD_SHIFT)
        {
          c -= 'a' - 'A';
        }
      }
      value += c;
      mPos = value.size();
    }
  }
  mCaretTimer = CARET_TIME;

  if (mTextChanged)
  {
    mTextChanged(this);
  }
}

UTextbox* UTextbox::setTextChangedHandler(FUN1(void, UTextbox*) textChanged)
{
  mTextChanged = textChanged;
  return this;
}

bool UTextbox::isFocussable()
{
  return true;
}