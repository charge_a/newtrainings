#pragma once
#include <UView.h>

class UTextbox : public UView
{
  int mPos;
  float mCaretTimer;

  UColor mTextColor;
  FUN1(void, UTextbox*) mTextChanged;

  float getCaretAlpha();
public:
  UTextbox(std::string name, URect frame, int frameFlag);

  UTextbox* setTextChangedHandler(FUN1(void, UTextbox*) textChanged);

  virtual void render(URect parentRect);
  virtual void renderSelf (URect actualRect);
  virtual void onKeyPress(int key, int scancode, int action, int mode);
  virtual bool isFocussable();

  std::string value;

};