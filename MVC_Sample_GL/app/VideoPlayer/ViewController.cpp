#include "ViewController.h"
#include "../../ui_elements/UScrollView.h"
using namespace std;
namespace VP {
  ViewController::ViewController()
  {
    mView = new UView("Main", URect(0,0,1,1), 0);
  }

  ViewController::~ViewController()
  {
    delete mView;
  }

  void ViewController::initialize()
  {
    mView
    ->setBackgroundColor(UColor(.3,.3,.3,1))
    ->setBackgroundImage("app/VideoPlayer/model.png")
    ;
    mView->addSubview(createTopBar());
    mView->addSubview(createMidView());
    mView->addSubview(createBottomBar());
  }

  UView* ViewController::createTopBar()
  {
    UView* topbar = new UView("Topbar", URect(0,0,1,30), H_ABS);
    topbar->setBackgroundColor(UColor(0,0,0,1));

    UButton* button1 = new UButton("b1", URect(0,0,30,1), X_ABS|W_ABS);
    UButton* button2 = new UButton("b2", URect(30,0,30,1), X_ABS|W_ABS);
    UButton* button3 = new UButton("b3", URect(-30,0,30,1), X_ABS|W_ABS);

    button1->setBackgroundColor(UColor(1,0,0,1));
    button2->setBackgroundColor(UColor(1,1,0,1));
    button3->setBackgroundColor(UColor(1,0,1,1));

    topbar->addSubview(button1);
    topbar->addSubview(button2);
    topbar->addSubview(button3);

    return topbar;
  }

  UView* ViewController::createMidView()
  {
    UView* midView = (new UView("Mid", URect(0, 30, 1, -30), Y_ABS|H_ABS))
                      ->setBackgroundColor(UColor(0, 0, 0.3, 1))
                      ;

    midView->addSubview(( new UView("midTab", URect(0, 0, 1, 20), H_ABS))
                        ->setBackgroundColor(UColor(0,0.3,0.6,1))
                       );
    midView->addSubview((new UView("midLeft", URect(0, 20, 200, 0), Y_ABS|W_ABS))
                        ->setBackgroundColor(UColor(0,1,0.4,1))
                      );
    midView->addSubview(createPlaylistView());

    return midView;
  }

  UView* ViewController::createBottomBar()
  {

    return (new UView("Bottom", URect(0, -30, 1, 30), Y_ABS|H_ABS))
    ->setBackgroundColor(UColor(0.3, 0,0,.1))
    ->addSubview(
      (new UView("playBar", URect(.5, 0, 90, 1), W_ABS|X_CENTRE))
      ->addSubview(
        (new UButton("bottomBack", URect(0, 0, 1.0/3, 1), 0))
        ->setBackgroundColor(UColor(0.2,0.3,0.6,1))
        ->setClickHandler([this](double x, double y) {
          UView* cv = dynamic_cast<UScrollView*>(mPlaylistView)->getContentView();
          int n = cv->getSubviews().size();
          string s = std::to_string(n);
          cv->addSubview((new UView("wewe", URect(0, 100*n, 1, 100), Y_ABS|H_ABS))
            ->addSubview((new UButton("wewe", URect(0, 0, 1, 50), Y_ABS|H_ABS))
              ->setButtonText(string("Clicked - ") + s)
              ->setFontColor(UColor(0,0,0,1))
              ->setFontSize(24)
              )
            ->addSubview((new UButton("wewe", URect(0, 50, 1, 49), Y_ABS|H_ABS))
              ->setButtonText(std::to_string(x) + "," + std::to_string(y))
              ->setFontColor(UColor(1,0,0,1))
              ->setFontSize(12)
              )
            ->addSubview((new UView("wewe", URect(0, 99, 1, 1), Y_ABS|H_ABS))
              ->setBackgroundColor(UColor(0,0,0,1))
              )
            ->setBackgroundColor(UColor(1,1,0,1))
            );
        })
      )
      ->addSubview(
        (new UButton("bottomPlay", URect(1.0/3, 0, 1.0/3, 1), 0))
        ->setBackgroundColor(UColor(0.5,0.2,0.6,1))
      )
      ->addSubview(
        (new UButton("bottomForward", URect(2.0/3, 0, 1.0/3, 1), 0))
        ->setBackgroundColor(UColor(0.1,0.9,0.2,1))
      )
    );
  }


  UView* ViewController::createPlaylistView()
  {
    mPlaylistView = new UScrollView("midRight", URect(200, 20, 0, 0), X_ABS|Y_ABS|W_ABS);
    return mPlaylistView;
  }


  void ViewController::updateViewModel()
  {

  }



} /* VP */
