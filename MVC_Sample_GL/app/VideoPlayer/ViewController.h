#pragma once
#include "../../base/Datatypes.h"
#include "../../base/MathUtil.h"
#include "../../base/graphics/DrawingUtil.h"
#include "../../ui_elements/UButton.h"

namespace VP {
  class ViewController {
  private:

    UView* createTopBar();
    UView* createMidView();
    UView* createBottomBar();

    UView* createPlaylistView();


    UView* mPlaylistView;


  public:
    ViewController ();
    virtual ~ViewController ();

    void initialize();
    void updateViewModel();

    UView* mView;
  };
} /* VP */
