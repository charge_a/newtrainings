#pragma once

// Math
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// GL
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <stdlib.h>

// functions
#include <functional>

// Macros
#define NORM(x) (glm::normalize(x))
#define RAND(a, b) (float)(a + (b-a)*((float)rand()/(RAND_MAX)))
#define DIST(v1, v2) glm::distance(v1, v2)
#define MAKE_REP(basetype, suffix) \
        typedef basetype##2 suffix##2; \
        typedef basetype##3 suffix##3; \
        typedef basetype##4 suffix##4

using namespace std::placeholders;
#define FUN0(ret) std::function<ret()>
#define FUN1(ret, p) std::function<ret(p)>
#define FUN2(ret, p, q) std::function<ret(p, q)>
#define CALLBACK0(f) std::bind(f, this)
#define CALLBACK1(f) std::bind(f, this, _1)
#define CALLBACK2(f) std::bind(f, this, _1, _2)

#define UNI3(x) x, x, x
#define UNIFORM_MAT(loc, m) glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(m))

#define ISKEY(key, value) (strcmp(key, value) == 0)

#define PRN_RECT(str, rect) printf("%s : %f, %f, %f, %f\n", str, rect.x, rect.y, rect.z, rect.w);;

MAKE_REP(glm::vec, Vec);
MAKE_REP(Vec, UPoint);
typedef Vec2 USize;
typedef Vec4 URect;
typedef Vec4 UColor;
typedef glm::mat4 UMat;

// Constant
const UPoint2 UPoint2Zero(0,0);
const UPoint3 UPoint3Zero(0,0,0);
const UMat MatID;

// Structs
// struct Bounds

inline void printRect(std::string dbgText, URect r)
{
  printf("dbg[%s]: %.2f, %.2f, %.2f, %.2f\n", dbgText.c_str(), r.x, r.y, r.z, r.w);
}
