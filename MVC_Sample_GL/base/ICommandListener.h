#pragma once
#include "Datatypes.h"

class UView;

class ICommandListener
{
public:
  virtual bool onCommandReceived(std::string message, UView* sender) = 0;
};
