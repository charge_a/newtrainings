#pragma once
#include "Datatypes.h"

class IViewEventsResponder {
public:
  virtual void onClick(URect inRect, UPoint2 atPoint) = 0;
  virtual void onDrag(URect inRect, UPoint2 atPoint, bool justStarted) = 0;
  virtual void onHover(URect inRect, UPoint2 atPoint) = 0;
  virtual void onWindowResize(USize screenSize) = 0;
  virtual void onMouseScroll(URect inRect, UPoint2 atPoint, UPoint2 offset) = 0;
  virtual void onKeyPress(int key, int scancode, int action, int mode) = 0;
};
