#pragma once
#include <string>
#include "UView.h"

class IViewFactory
{
public:
  virtual UView* getClassForTag(const char* tag) = 0;
};