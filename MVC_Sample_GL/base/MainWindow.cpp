#include "MainWindow.h"

MainWindow::MainWindow() {}

void MainWindow::initWithSize(USize size, IViewEventsResponder* view) {
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
  glfwWindowHint(GLFW_SAMPLES, 4);
  mWindow = glfwCreateWindow(size.x, size.y, "LearnOpenGL", NULL, NULL);
  if (!mWindow) {
    std::cerr << "Failed to create GLFW window" << std::endl;
    glfwTerminate();
  }
  glfwMakeContextCurrent(mWindow);

  glfwSetWindowUserPointer(mWindow, this);
  glfwSetKeyCallback(mWindow, MainWindow::handleGLFW_keyboard);
  glfwSetWindowSizeCallback(mWindow, MainWindow::handleGLFW_windowResize);
  glfwSetCursorPosCallback(mWindow, MainWindow::handleGLFW_mouseMove);
  glfwSetMouseButtonCallback(mWindow, MainWindow::handleGLFW_mouseClick);
  glfwSetScrollCallback(mWindow, MainWindow::handleGLFW_scroll);  

  glewExperimental = GL_TRUE;
  glewInit();

  mView = view;
  mWidth = size.x;
  mHeight = size.y;
}

void MainWindow::startRenderLoop(std::function<void ()> renderFunction) {
  while(!glfwWindowShouldClose(mWindow))
  {
      glfwPollEvents();
      renderFunction();
      glfwSwapBuffers(mWindow);
  }
  glfwTerminate();
}

// Static to Instance redirection
void MainWindow::handleGLFW_keyboard(GLFWwindow *window, int key, int scancode, int action, int mode) {
  MainWindow* instance = static_cast<MainWindow*>(glfwGetWindowUserPointer(window));
  instance->handle_keyboard(key, scancode, action, mode);
}
void MainWindow::handleGLFW_windowResize(GLFWwindow *window, int w, int h) {
  MainWindow* instance = static_cast<MainWindow*>(glfwGetWindowUserPointer(window));
  instance->handle_windowResize(w, h);
}
void MainWindow::handleGLFW_mouseMove(GLFWwindow *window, double xpos, double ypos) {
  MainWindow* instance = static_cast<MainWindow*>(glfwGetWindowUserPointer(window));
  instance->handle_mouseMove(xpos, ypos);
}
void MainWindow::handleGLFW_mouseClick(GLFWwindow *window, int button, int action, int mods) {
  MainWindow* instance = static_cast<MainWindow*>(glfwGetWindowUserPointer(window));
  instance->handle_mouseClick(button, action, mods);
}
void MainWindow::handleGLFW_scroll(GLFWwindow* window, double xoffset, double yoffset) {
  MainWindow* instance = static_cast<MainWindow*>(glfwGetWindowUserPointer(window));
  instance->handle_scroll(xoffset, yoffset);
}
//

void MainWindow::handle_keyboard(int key, int scancode, int action, int mode) {
  if(key == GLFW_KEY_Q && action == GLFW_PRESS)
    glfwSetWindowShouldClose(mWindow, GL_TRUE);

  mView->onKeyPress(key, scancode, action, mode);

}
void MainWindow::handle_windowResize(int w, int h) {
  glViewport(0, 0, w, h);
  mWidth = w;
  mHeight = h;
  mView->onWindowResize(USize(w, h));
}
void MainWindow::handle_mouseMove(double xpos, double ypos) {
  double x = xpos / mWidth;
  double y = ypos / mHeight;

  if (mDragBegin)
  {
    mView->onDrag(URect(0,0,1,1), UPoint2(x, y), !mDragBegin);
  }
  mView->onHover(URect(0,0,1,1), UPoint2(x, y));
}
void MainWindow::handle_mouseClick(int button, int action, int mods) {
  double xpos, ypos;
  glfwGetCursorPos(mWindow, &xpos, &ypos);
  double x = xpos / mWidth;
  double y = ypos / mHeight;
  if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
    std::cout << "" << '\n';
    mView->onClick(URect(0,0,1,1), UPoint2(x, y));
  }

  if (action == GLFW_PRESS) {
    mView->onDrag(URect(0,0,1,1), UPoint2(x, y), !mDragBegin);
    mDragBegin = true;
  }
  if (action == GLFW_RELEASE) mDragBegin = false;
}
void MainWindow::handle_scroll(double xoffset, double yoffset) {
  double xpos, ypos;
  glfwGetCursorPos(mWindow, &xpos, &ypos);
  double x = xpos / mWidth;
  double y = ypos / mHeight;
  mView->onMouseScroll(URect(0,0,1,1), UPoint2(x, y), UPoint2(xoffset, yoffset));
}


float MainWindow::getScreenWidth() {
  // int w, h;
  // glfwGetWindowSize(mWindow, &w, &h);
  // return w; //glutGet(GLUT_SCREEN_WIDTH);
  return mWidth;

}
float MainWindow::getScreenHeight() {
  // int w, h;
  // glfwGetWindowSize(mWindow, &w, &h);
  // return h; //glutGet(GLUT_SCREEN_WIDTH);
  return mHeight;
}

float MainWindow::getViewportWidth () {
  GLint xywh[4];
  glGetIntegerv(GL_VIEWPORT, xywh);
  return xywh[2];
}
float MainWindow::getViewportHeight() {
  GLint xywh[4];
  glGetIntegerv(GL_VIEWPORT, xywh);
  return xywh[3];
}
void MainWindow::resize(USize newScreenSize) {
  glfwSetWindowSize(mWindow, newScreenSize.x, newScreenSize.y);
}
