#pragma once
#include "Datatypes.h"
#include "IViewEventsResponder.h"

#define SCREEN_W (MainWindow::Instance().getScreenWidth())
#define SCREEN_H (MainWindow::Instance().getScreenHeight())
#define VP_W (MainWindow::Instance().getViewportWidth())
#define VP_H (MainWindow::Instance().getViewportHeight())
#define PX_W(x) ((x)/VP_W)
#define PX_H(y) ((y)/VP_H)

class MainWindow {
private:
  MainWindow ();

  GLFWwindow* mWindow;
  IViewEventsResponder* mView;
  float mWidth, mHeight;
  bool mDragBegin;

  static void handleGLFW_keyboard(GLFWwindow* window, int key, int scancode, int action, int mode);
  static void handleGLFW_windowResize(GLFWwindow *window, int w, int h);
  static void handleGLFW_mouseMove(GLFWwindow* window, double xpos, double ypos);
  static void handleGLFW_mouseClick(GLFWwindow* window, int button, int action, int mods);
  static void handleGLFW_scroll(GLFWwindow* window, double xoffset, double yoffset);

  void handle_keyboard(int key, int scancode, int action, int mode);
  void handle_windowResize(int w, int h);
  void handle_mouseMove(double xpos, double ypos);
  void handle_mouseClick(int button, int action, int mods);
  void handle_scroll(double xoffset, double yoffset);
public:
  static MainWindow& Instance()
  {
    static MainWindow * sInstance = NULL;
    if (sInstance == NULL) sInstance = new MainWindow();
    return *sInstance;
  }
  virtual ~MainWindow ()
  {
  }

  void initWithSize(USize size, IViewEventsResponder* view);
  void startRenderLoop(std::function<void()> renderFunction);
  float getScreenWidth();
  float getScreenHeight();
  float getViewportWidth();
  float getViewportHeight();
  void resize(USize newScreenSize);
};
