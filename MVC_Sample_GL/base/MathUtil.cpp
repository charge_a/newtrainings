#include "MathUtil.h"
#include "MainWindow.h"

Vec3 ScaleAndTranslate(Vec2 from, Vec2 to, Vec3 scale)
{
  Vec4 frameTopLeftGLCoord (to.x, to.y, 0, 1);
  Vec4 squareTopLeftGLCoord (from.x, from.y, 0, 1);
  UMat scaleMat = glm::scale(MatID, scale);
  Vec4 squareNewPos = scaleMat * squareTopLeftGLCoord;
  Vec4 translation = frameTopLeftGLCoord - squareNewPos;
  return Vec3(translation);
}

Vec2 ScreenToGLCoordinates(Vec2 screen)
{
  return Vec2(2*screen.x - 1, -(2*screen.y - 1));
}

URect PixelToNormal(URect rect)
{
  return URect(
    PX_W(rect.x),
    PX_H(rect.y),
    PX_W(rect.z),
    PX_H(rect.w)
  );
}

URect NormalToPixel(URect rect)
{
  return URect(
    VP_W * (rect.x),
    VP_H * (rect.y),
    VP_W * (rect.z),
    VP_H * (rect.w)
  );
}

bool RectContainsPoint(URect rect, UPoint2 point)
{
  return (point.x >= rect.x) && (point.x <= rect.x + rect.z)
    &&  (point.y >= rect.y) && (point.y <= rect.y + rect.w);
}

URect RectInsideRect(URect parent, URect child)
{
  return URect(
    parent.x + (child.x * parent.z),
    parent.y + (child.y * parent.w),
    parent.z * child.z,
    parent.w * child.w
  );
}
