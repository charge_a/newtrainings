#pragma once
#include "Datatypes.h"

Vec3 ScaleAndTranslate(Vec2 from, Vec2 to, Vec3 scale);

Vec2 ScreenToGLCoordinates(Vec2 screen);

URect PixelToNormal(URect rect);
URect NormalToPixel(URect rect);

bool RectContainsPoint(URect rect, UPoint2 point);

URect RectInsideRect(URect parent, URect child);
