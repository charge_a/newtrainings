#pragma once
#include "Datatypes.h"

class Model {
private:
  int mValue;

public:
  void updateModel(int value);
  FUN1(void, int) mValueChanged;
};
