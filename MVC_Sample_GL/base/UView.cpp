#include "UView.h"
#include "graphics/DrawingUtil.h"
#include "MathUtil.h"
#include "MainWindow.h"
#include <stdlib.h>

UView* UView::gFocussedView;

UView::UView(std::string name, URect frame, int frameFlag)
: mName(name), 
  mFrame(frame), 
  mFrameFlag(frameFlag), 
  mIsHovered(false),
  mIsDirty(false),
  mCommandListener(NULL),
  mClickMessage(""),
  mHoverMessage(""),
  mParentUView(NULL)
  {}

UView::~UView()
{
  for (auto style : mViewStyle)
    if (style.second.backgroundTexture)
      delete style.second.backgroundTexture;
  for(UView* sv : mSubUViews)
    if (sv)
      delete sv;
}

URect UView::computeRenderFrame(URect inRect)
{
  URect frame = mFrame;
  if (mFrameFlag & X_ABS) frame.x = (frame.x / VP_W) / (inRect.z);
  if (mFrameFlag & Y_ABS) frame.y = (frame.y / VP_H) / (inRect.w);
  if (mFrameFlag & W_ABS) frame.z = (frame.z / VP_W) / (inRect.z);
  if (mFrameFlag & H_ABS) frame.w = (frame.w / VP_H) / (inRect.w);

  // -ive left/top will be interpreted as right/bottom aligned.
  if (frame.x < 0) frame.x = 1 + frame.x;
  if (frame.y < 0) frame.y = 1 + frame.y;
  // -ive width/height will be interpreted as a gap from right/bottom.
  // 0 width/height will be interpreted as full extent from x/y.
  // width=0 is nonsense otherwise, so we use it for wrapping around.
  if (frame.z <= 0) frame.z = 1 - frame.x + frame.z;
  if (frame.w <= 0) frame.w = 1 - frame.y + frame.w;

  if (mFrameFlag & X_CENTRE) frame.x -= frame.z / 2.0;
  if (mFrameFlag & Y_CENTRE) frame.y -= frame.w / 2.0;

  // clip
  frame.x = std::max(frame.x, 0.0f);
  frame.y = std::max(frame.y, 0.0f);
  frame.z = std::max(frame.z, 0.0f);
  frame.w = std::max(frame.w, 0.0f);

  return frame;
}

void UView::renderSelf(URect actualRect) {
  mFrameRendered = actualRect;
  if (gFocussedView == this)
  {
    UColor c  = URect(1,1,0,1);
    DrawingUtil::Instance().DrawRectangle(URect(actualRect.x, actualRect.y - 5.0/VP_H, actualRect.z, 5.0/VP_H), c, NULL);
    DrawingUtil::Instance().DrawRectangle(URect(actualRect.x, actualRect.y + actualRect.w, actualRect.z, 5.0/VP_H), c, NULL);
    DrawingUtil::Instance().DrawRectangle(URect(actualRect.x - 5.0/VP_W, actualRect.y, 5.0/VP_W, actualRect.w), c, NULL);
    DrawingUtil::Instance().DrawRectangle(URect(actualRect.x + actualRect.z, actualRect.y, 5.0/VP_W, actualRect.w), c, NULL);
  }
  ViewState state = getState();
  ViewStyle style = mViewStyle[kViewStateNormal];
  if (mViewStyle.find(state) != mViewStyle.end())
  {
    style = mViewStyle[state];
  }
  DrawingUtil::Instance().DrawRectangle(actualRect, style.backgroundColor, style.backgroundTexture);
}

void UView::render(URect parentRect) {
  computeVisible(parentRect);
  if (mIsVisible)
  {
    URect actualRect = RectInsideRect(parentRect, computeRenderFrame(parentRect));
    renderSelf(actualRect);
    for (UView* sv : mSubUViews)
    {
      if (sv)
        sv->render(actualRect);
    }
  }
  mIsDirty = false;
}

UView* UView::setBackgroundColor(UColor bgColor, ViewState viewState) {
  mViewStyle[viewState].backgroundColor = bgColor;
  return this;
}
UColor UView::getBackgroundColor(ViewState viewState) { return mViewStyle[viewState].backgroundColor; }

UView* UView::setBackgroundImage(std::string image, ViewState viewState) {
  if (mViewStyle[viewState].backgroundTexture)
    delete mViewStyle[viewState].backgroundTexture;
  mViewStyle[viewState].backgroundTexture = new UTexture(image, 4);
  return this;
}

UView* UView::addSubview(UView *view) {
  mSubUViews.push_back(view);
  view->mParentUView = this;
  if (view->getName() != "") {
    mSubUViewMap[view->getName()] = view;
  }
  mIsDirty = true;
  return this;
}

UView* UView::removeSubview(UView* subview) {
  for (auto it = mSubUViews.begin(); it != mSubUViews.end(); it++)
  {
    if (*it == subview) {
      mSubUViews.erase(it);
      subview->mParentUView = NULL;
      if (subview->getName() != "") {
        mSubUViewMap.erase(subview->getName());
      }
      mIsDirty = true;
      break;
    }
  }
  return this;
}

std::vector<UView*>& UView::getSubviews() {
  return mSubUViews;
}

UView* UView::removeAllSubviews() {
  for(UView* sv : mSubUViews)
    if (sv)
      delete sv;
  mSubUViews.clear();
  mSubUViewMap.clear();
  return this;
}

UView* UView::getSubviewByName(std::string name)
{
  if (name == mName)
  {
    return this;
  }
  for (UView* sv : mSubUViews)
    if (sv)
    {
      UView* res = sv->getSubviewByName(name);
      if (res) return res;
    }
  return NULL;
}

UView* UView::setFrame(URect frame, int frameFlag) {
  mFrame = frame;
  mFrameFlag = frameFlag;
}

URect UView::getFrame()
{
  return mFrame;
}
int UView::getFrameFlag()
{
  return mFrameFlag;
}
URect UView::getRenderedFrame()
{
  return mFrameRendered;
}

bool UView::isVisible() {
  return mIsVisible;
}

void UView::computeVisible(URect parentRect)
{
  mIsVisible = true;
  URect actualRect = RectInsideRect(parentRect, computeRenderFrame(parentRect));
  if ((actualRect.x > 1.0) ||
      (actualRect.y > 1.0) ||
      (actualRect.x + actualRect.z < 0.0) ||
      (actualRect.y + actualRect.w < 0.0))
  {
    mIsVisible = false;
  }
}

bool UView::isFocussable()
{
  return false;
}

bool UView::isFocussed()
{
  return (gFocussedView == this);
}

UView* UView::getParentView()
{
  return mParentUView;
}

ViewState UView::getState()
{
  if (gFocussedView == this)
  {
    return kViewStateFocussed;
  }
  if (mIsHovered)
  {
    return kViewStateHover;
  }
  return kViewStateNormal;
}

std::string UView::getName() { return mName; }

UView* UView::setClickHandler(FUN0(void) clickHandler)
{
  mClickHandler = clickHandler;
  return this;
}

void UView::onClick(URect inRect, UPoint2 atPoint) {
  URect actualRect = RectInsideRect(inRect, computeRenderFrame(inRect));

  if (RectContainsPoint(actualRect, atPoint))
  {
    if (mParentUView == NULL)
    {
      gFocussedView = NULL;
    }
    if (isFocussable())
    {
      gFocussedView = this;
    }
    for (UView* sv: mSubUViews)
    {
      if (sv)
        sv->onClick(actualRect, atPoint);
    }
    std::cout << mName << " clicked: " << atPoint.x << "," << atPoint.y << '\n';
    if (mClickHandler)
    {
      mClickHandler();
    }
    if (mCommandListener && mClickMessage != "")
    {
      mCommandListener->onCommandReceived(mClickMessage, this);
    }
  }
}

UView* UView::setDragHandler(FUN2(void, double, double) dragHandler)
{
  mDragHandler = dragHandler;
  return this;
}

void UView::onDrag(URect inRect, UPoint2 atPoint, bool justStarted) {
  URect actualRect = RectInsideRect(inRect, computeRenderFrame(inRect));

  if (!justStarted || RectContainsPoint(actualRect, atPoint))
  {
    for (UView* sv: mSubUViews)
    {
      if (sv)
        sv->onDrag(actualRect, atPoint, justStarted);
    }
    // std::cout << mName << " dragged: " << atPoint.x << "," << atPoint.y << '\n';
    if (mDragHandler)
    {
      mDragHandler(atPoint.x, atPoint.y);
    }
  }

}


void UView::onHover(URect inRect, UPoint2 atPoint)
{
  URect actualRect = RectInsideRect(inRect, computeRenderFrame(inRect));

  if (RectContainsPoint(actualRect, atPoint))
  {
    for (UView* sv: mSubUViews)
    {
      if (sv)
        sv->onHover(actualRect, atPoint);
    }

    if (mHoverHandler)
    {
      mHoverHandler();
    }
    if (mCommandListener && mHoverMessage != "")
    {
      mCommandListener->onCommandReceived(mHoverMessage, this);
    }
    mIsHovered = true;
  }
  else {
    mIsHovered = false;
  }
}

UView* UView::setHoverHandler(FUN0(void) hoverHandler)
{
  mHoverHandler = hoverHandler;
  return this;
}

UView* UView::setCommandListener (ICommandListener* listener)
{
  mCommandListener = listener;
  for (UView* sv : mSubUViews)
    if (sv)
      sv->setCommandListener(listener);
  return this;
}

void UView::onWindowResize(USize screenSize)
{
  mIsDirty = true;
  for (UView* sv: mSubUViews)
  {
    if (sv)
      sv->onWindowResize(screenSize);
  }
}

void UView::onMouseScroll(URect inRect, UPoint2 atPoint, UPoint2 offset) {
  URect actualRect = RectInsideRect(inRect, computeRenderFrame(inRect));

  if (RectContainsPoint(actualRect, atPoint))
  {
    for (UView* sv: mSubUViews)
    {
      if (sv)
        sv->onMouseScroll(actualRect, atPoint, offset);
    }

    if (mScrollHandler)
    {
      mScrollHandler();
    }
  }

}

UView* UView::setScrollHandler(FUN0(void) scrollHandler) {
  mScrollHandler = scrollHandler;
  return this;
}

void UView::onKeyPress(int key, int scancode, int action, int mode) {
  for (UView* sv : mSubUViews)
    sv->onKeyPress(key, scancode, action, mode);
}


// XML methods
UView* UView::processAttribute(const char* key, const char* value)
{
  if (strcmp(key, "x") == 0)
  {
    float v;
    char dim[2];
    int p = sscanf(value, "%f%s", &v, dim);
    if (p == 0) printf("Error parsing attribute %s=%s\n", key, value);
    else if (p == 1) {
      mFrame.x = v;
    }
    else if (p == 2) {
      mFrame.x = v;
      mFrameFlag |= X_ABS;
    }
  }
  if (strcmp(key, "y") == 0)
  {
    float v;
    char dim[2];
    int p = sscanf(value, "%f%s", &v, dim);
    if (p == 0) printf("Error parsing attribute %s=%s\n", key, value);
    else if (p == 1) {
      mFrame.y = v;
    }
    else if (p == 2) {
      mFrame.y = v;
      mFrameFlag |= Y_ABS;
    }
  }
  if (strcmp(key, "w") == 0)
  {
    float v;
    char dim[2];
    int p = sscanf(value, "%f%s", &v, dim);
    if (p == 0) printf("Error parsing attribute %s=%s\n", key, value);
    else if (p == 1) {
      mFrame.z = v;
    }
    else if (p == 2) {
      mFrame.z = v;
      mFrameFlag |= W_ABS;
    }
  }
  if (strcmp(key, "h") == 0)
  {
    float v;
    char dim[2];
    int p = sscanf(value, "%f%s", &v, dim);
    if (p == 0) printf("Error parsing attribute %s=%s\n", key, value);
    else if (p == 1) {
      mFrame.w = v;
    }
    else if (p == 2) {
      mFrame.w = v;
      mFrameFlag |= H_ABS;
    }
  }
  if (ISKEY(key, "color"))
  {
    float r, g, b, a;
    int p = sscanf(value, "%f,%f,%f,%f", &r, &g, &b, &a);
    if (p < 4) printf("Error parsing attribute %s=%s\n", key, value);
    else {
      setBackgroundColor(UColor(r, g, b, a), kViewStateNormal);
    }
  }
  if (ISKEY(key, "image"))
  {
    setBackgroundImage(value, kViewStateNormal);
  }
  if (ISKEY(key, "display"))
  {
    if (ISKEY(value, "aspect-fit"))
    {
      mViewStyle[kViewStateNormal].backgroundTexture->displayOption = kUTextureAspectFit;
    }
    else
    {
      mViewStyle[kViewStateNormal].backgroundTexture->displayOption = kUTextureScaleToFill;
    }
  }
  if (!strcmp(key, "name"))
  {
    mName = value;
  }
  if (ISKEY(key, "on-click"))
  {
    mClickMessage = value;
  }
  if (ISKEY(key, "on-hover"))
  {
    mHoverMessage = value;
  }

  return this;
}
UView* UView::processValue(const char* value)
{
  return this;
}


// ViewStyle
ViewStyle::ViewStyle()
: backgroundColor(URect(0)), backgroundTexture(NULL)
{}