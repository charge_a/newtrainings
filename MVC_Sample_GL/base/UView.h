#pragma once
#include "IViewEventsResponder.h"
#include <vector>
#include <map>
#include "graphics/UTexture.h"
#include "ICommandListener.h"

enum ViewState
{
  kViewStateNormal,
  kViewStateFocussed,
  kViewStateHover,
  kViewStateSelected
};

class ViewStyle
{
public:
  ViewStyle();
  UColor backgroundColor;
  UTexture *backgroundTexture;
};

class UView : public IViewEventsResponder {
protected:
  std::string mName;
  URect mFrame;
  URect mFrameRendered;
  std::vector<UView*> mSubUViews;
  UView* mParentUView;
  int mFrameFlag;
  bool mIsHovered;
  bool mIsDirty;
  bool mIsVisible;
  std::map<std::string, UView*> mSubUViewMap;
  std::map<ViewState, ViewStyle> mViewStyle;

  FUN0(void) mClickHandler;
  FUN2(void, double, double) mDragHandler;
  FUN0(void) mHoverHandler;
  FUN0(void) mScrollHandler;

  ICommandListener* mCommandListener;
  std::string mClickMessage;
  std::string mHoverMessage;

public:
  URect computeRenderFrame(URect inRect);

public:
  UView (std::string name, URect frame, int frameFlag);
  virtual ~UView ();

  virtual void render(URect parentRect);
  virtual void renderSelf (URect actualRect);

  virtual UView* addSubview(UView* view);
  UView* setBackgroundColor(UColor bgColor, ViewState viewState = kViewStateNormal);
  UView* setBackgroundImage(std::string image, ViewState viewState = kViewStateNormal);
  UColor getBackgroundColor(ViewState viewState = kViewStateNormal);
  std::string getName();

  UView* removeSubview(UView* subview);
  std::vector<UView*>& getSubviews();
  UView* removeAllSubviews();
  UView* getSubviewByName(std::string name);

  UView* setFrame(URect frame, int frameFlag);
  URect getFrame();
  int getFrameFlag();
  URect getRenderedFrame();

  bool isVisible();
  void computeVisible(URect parentRect);

  virtual bool isFocussable();
  bool isFocussed();

  UView* getParentView();
  ViewState getState();

  virtual void onClick(URect inRect, UPoint2 atPoint);
  UView* setClickHandler(FUN0(void) clickHandler);

  virtual void onDrag(URect inRect, UPoint2 atPoint, bool justStarted);
  UView* setDragHandler(FUN2(void, double, double) dragHandler);

  virtual void onHover(URect inRect, UPoint2 atPoint);
  UView* setHoverHandler(FUN0(void) hoverHandler);

  UView* setCommandListener(ICommandListener* listener);

  virtual void onWindowResize(USize screenSize);

  virtual void onMouseScroll(URect inRect, UPoint2 atPoint, UPoint2 offset);
  UView* setScrollHandler(FUN0(void) scrollHandler);

  virtual void onKeyPress(int key, int scancode, int action, int mode);

public:
  virtual UView* processAttribute(const char* key, const char* value);
  virtual UView* processValue(const char* value);

public:
  static UView* gFocussedView;
};

const int X_ABS = 1 << 0;
const int Y_ABS = 1 << 1;
const int W_ABS = 1 << 2;
const int H_ABS = 1 << 3;
const int X_CENTRE = 1 << 4;
const int Y_CENTRE = 1 << 5;
