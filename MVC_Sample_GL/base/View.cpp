#include "View.h"
void View::onClick(double x, double y) {
  std::cout << "View clicked at: " << x << "," << y << '\n';
  mClickHandler(x, y);
}
