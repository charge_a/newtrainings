#pragma once
#include "IViewEventsResponder.h"


// https://stackoverflow.com/questions/5859455/how-to-implement-a-generic-button-class-that-executes-different-functions-on-cli
class View : public IViewEventsResponder {
private:

public:
  virtual void onClick(double x, double y);
  FUN2(void, double, double) mClickHandler;
};
