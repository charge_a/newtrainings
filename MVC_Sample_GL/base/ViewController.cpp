#include "ViewController.h"
#include "../ui_elements/UButton.h"

using namespace std::placeholders;

ViewController::ViewController()
{
  mView = new UView("Top", URect(0.1, 0.1, 0.8, 0.8), false);
  mModel = new Model();
}

ViewController::~ViewController()
{
  delete mView;
}

void ViewController::initialize() {
  mView->setBackgroundColor(UColor(1,0,0,1));
  UButton* v = new UButton("Inner", URect(10, 100, 300, 300), true);
  v->setBackgroundColor(UColor(0,1,0,1));
  v->setButtonText("Mayhem");
  v->setFontColor(UColor(0,0,0,1));
  v->setFontSize(16);
  v->mClickHandler = [this, v] (double x, double y) {
    mView->setBackgroundColor(v->getBackgroundColor());
  };
  mView->addSubview(v);

  v = new UButton("Inner2", URect(0.5, 0.1, 0.3, 0.3), false);
  v->setBackgroundColor(UColor(0,0,1,1));
  v->setButtonText("Made");
  v->setFontColor(UColor(0,0,0,1));
  v->setFontSize(16);
  v->mClickHandler = [this, v] (double x, double y) {
    mView->setBackgroundColor(v->getBackgroundColor());
  };
  mView->addSubview(v);

  mView->mClickHandler = CALLBACK2(&ViewController::onBtnClick);
  // Above can also be rewritten as lambda below.
  // mView->mClickHandler = [this](double x, double y) {
  //   onBtnClick(x, y);
  // };
  mModel->mValueChanged = [this](int v) {
    std::cout << "Model value changed: " << v << '\n';
  };
}

void ViewController::onBtnClick(double x, double y) {
  std::cout << "ViewController clicked at: " << x << "," << y << '\n';
  mModel->updateModel((int)(x*100));
}
