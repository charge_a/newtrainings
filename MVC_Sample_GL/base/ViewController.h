#pragma once
#include "../ui_elements/UView.h"
#include "Model.h"
class ViewController {
private:


public:
  ViewController();
  virtual ~ViewController();
  UView* mView;
  Model* mModel;

  void initialize();
  void onBtnClick(double x, double y);
};
