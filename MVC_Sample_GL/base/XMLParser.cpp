#include "XMLParser.h"
#include <iostream>

UView* XMLParser::parse(string text, IViewFactory* viewFactory, bool firstPass)
{
  file<> xmlFile(text.c_str());
  return parseText(xmlFile.data(), viewFactory, firstPass);
}

UView* XMLParser::parseText(string text, IViewFactory* viewFactory, bool firstPass)
{
  xml_document<> doc;
  doc.parse<0>((char*)text.c_str());

  UView* root;
  if (firstPass)
  {
    root = viewFactory->getClassForTag("view");
    // configure default background
    //
    for (auto child = doc.first_node(); child; child = child->next_sibling())
    {
      printf("adding %s\n", child->name());
      root->addSubview(buildDOM(child, viewFactory));
    }
  }
  else
  {
    root = buildDOM(doc.first_node(), viewFactory);
  }
  return root;
}

UView* XMLParser::buildDOM(xml_node<>* node, IViewFactory* viewFactory)
{
  UView* root;
  if (strcmp(node->name(), "include") == 0)
  {
    root = parse(node->value(), viewFactory, false);
  }
  else
  {
    root = viewFactory->getClassForTag(node->name());
    for (auto attr = node->first_attribute(); attr; attr = attr->next_attribute())
    {
      root->processAttribute(attr->name(), attr->value());
    }

    root->processValue(node->value());

    for (auto child = node->first_node(); child; child = child->next_sibling())
    {
      root->addSubview(buildDOM(child, viewFactory));
    }
  }

  return root;
}
