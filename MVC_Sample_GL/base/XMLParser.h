#pragma once
#include <map>
#include <vector>
#include <string>
#include "IViewFactory.h"
#include <rapidxml-1.13/rapidxml.hpp>
#include <rapidxml-1.13/rapidxml_utils.hpp>
#include <rapidxml-1.13/rapidxml_print.hpp>

using namespace rapidxml;

using namespace std;

class XMLParser
{
  static UView* buildDOM(xml_node<>* node, IViewFactory* viewFactory);
public:
  static UView* parse(string text, IViewFactory* viewFactory, bool firstPass=true);
  static UView* parseText(string text, IViewFactory* viewFactory, bool firstPass=true);
  
};