#include "DrawingUtil.h"

DrawingUtil::DrawingUtil()
{
  mRectRenderer = new URectRenderer();
  mTextRenderer = new UTextRenderer();
}

DrawingUtil::~DrawingUtil()
{
  delete mRectRenderer;
  delete mTextRenderer;
}

void DrawingUtil::DrawRectangle(URect frame, UColor color, UTexture *bgTex) {
  mRectRenderer->render(frame, color, bgTex);
}

void DrawingUtil::DrawLineH(Vec2 left, float length, float thick, UColor color, UTexture *bgTex) {
  // TODO: Make a separate GL renderer if performance becomes issue due to it.
  mRectRenderer->render(URect(left.x, left.y, length, thick), color, bgTex);
}
void DrawingUtil::DrawLineV(Vec2 left, float length, float thick, UColor color, UTexture *bgTex) {
  mRectRenderer->render(URect(left.x, left.y, thick, length), color, bgTex);
}

void DrawingUtil::DrawText(URect frame, std::string text, float fontSize, UColor color, TextAlignment alignment)
{
  mTextRenderer->render(frame, text, FontFactoryUbuntuL::getFont(), fontSize, color, alignment);
}

URect DrawingUtil::GetTextBounds(std::string text, float fontSize)
{
  FontFactoryUbuntuL::getFont().loadTexturesForFontSize(fontSize);
  float w = FontFactoryUbuntuL::getFont().computeTextLength(text, fontSize);
  float h = FontFactoryUbuntuL::getFont().getBoundingBox('X', fontSize).w;
  return URect(0,0,w,h);
}