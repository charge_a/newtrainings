#pragma once
#include "URectRenderer.h"
#include "UTextRenderer.h"

class DrawingUtil {
private:
  DrawingUtil ();

  URectRenderer* mRectRenderer;
  UTextRenderer* mTextRenderer;


public:
  virtual ~DrawingUtil ();
  static DrawingUtil& Instance()
  {
    static DrawingUtil* sInstance = NULL;
    if (sInstance == NULL) sInstance = new DrawingUtil();
    return *sInstance;
  }

  void DrawRectangle(URect frame, UColor color, UTexture *bgTex);
  void DrawLineH(Vec2 left, float length, float thick, UColor color, UTexture *bgTex);
  void DrawLineV(Vec2 left, float length, float thick, UColor color, UTexture *bgTex);
  void DrawText(URect frame, std::string text, float fontSize, UColor color, TextAlignment alignment);

  URect GetTextBounds(std::string text, float fontSize);
};
