#include "Font.h"
#include <iostream>

Font::Font() {}

Font::Font(char const * fontSrc)
{
  mFontSrc = fontSrc;
}

Font::Font(const Font & font)
{
  mCharMapCache = font.mCharMapCache;
  mFontSrc = font.mFontSrc;
}

void Font::loadTexturesForFontSize(int fontSize)
{
  if (mCharMapCache.find(fontSize) == mCharMapCache.end())
  {
    FT_Library ft;
    if (FT_Init_FreeType(&ft)) {
      std::cerr << "Error: Freetype: Couldn't init freetype" << '\n';
    }

    FT_Face face;
    if (FT_New_Face(ft, mFontSrc.c_str()
                    , 0, &face)) {
      std::cerr << "Error: Freetype: Failed to load font" << '\n';
    }

    FT_Set_Pixel_Sizes(face, 0, fontSize);
    // FT_Set_Char_Size(face, 50 * 64, 0, 100, 0);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    for (GLubyte c = 0; c < 128; c++)
    {
      if (FT_Load_Char(face, c, FT_LOAD_RENDER)) {
        std::cerr << "Error: Freetype: failed to load glyph " << c << '\n';
        continue;
      }

      GLuint texture;
      glGenTextures(1, &texture);
      glBindTexture(GL_TEXTURE_2D, texture);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, face->glyph->bitmap.width, face->glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

      mCharMapCache[fontSize][c] = {
        texture,
        glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
        glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
        face->glyph->advance.x
      };
    }

    FT_Done_Face(face);
    FT_Done_FreeType(ft);

  }
}

URect Font::getBoundingBox(GLchar c, int fontSize)
{
  CharMap &m = mCharMapCache[fontSize];
  int x = m[c].Bearing.x;
  int y = m[c].Bearing.y - m[c].Size.y;
  int w = m[c].Size.x;
  int h = m[c].Size.y;
  return URect(x, y, w, h);
}

int Font::getAdvance(GLchar c, int fontSize)
{
  Character &ch = mCharMapCache[fontSize][c];
  return ch.Advance >> 6;

}
GLuint Font::getTexture(GLchar c, int fontSize)
{
  Character &ch = mCharMapCache[fontSize][c];
  return ch.TextureID;
}
GLfloat Font::computeTextLength(std::string text, int fontSize)
{
  float width = 0;
  for (std::string::const_iterator c = text.begin(); c != text.end(); c++)
  {
    GLfloat w = getAdvance(*c, fontSize);// * scale;
    width += w;
  }
  return width;
}
