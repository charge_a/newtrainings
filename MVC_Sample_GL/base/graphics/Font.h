#pragma once
#include <ft2build.h>
#include FT_FREETYPE_H
#include <map>
#include "../Datatypes.h"

struct Character {
  GLuint  TextureID;
  glm::ivec2 Size;
  glm::ivec2 Bearing;
  GLuint  Advance;
};

typedef std::map<GLchar, Character> CharMap;

class Font {
private:
  std::map<int, CharMap> mCharMapCache;
  std::string mFontSrc;

public:
  Font ();
  Font (char const * fontSrc);
  Font (const Font & font);
  URect getBoundingBox(GLchar c, int fontSize);
  int getAdvance(GLchar c, int fontSize);
  GLuint getTexture(GLchar c, int fontSize);
  GLfloat computeTextLength(std::string text, int fontSize);
  void loadTexturesForFontSize(int fontSize);
};

#define FONT_FACTORY(fontName, fontSrc) \
class FontFactory##fontName {                 \
public:                                       \
  static Font & getFont() {                   \
    static Font * sFont = NULL;               \
    if (sFont == NULL)                        \
      sFont = new Font(fontSrc);              \
    return *sFont;                            \
  }                                           \
};

FONT_FACTORY(Mono, "/usr/share/fonts/truetype/freefont/FreeMono.ttf");
FONT_FACTORY(Sans, "/usr/share/fonts/truetype/freefont/FreeSans.ttf");
FONT_FACTORY(Purisa, "/usr/share/fonts/truetype/tlwg/Purisa.ttf");
FONT_FACTORY(UbuntuL, "/usr/share/fonts/truetype/ubuntu-font-family/Ubuntu-L.ttf");
FONT_FACTORY(NotoSansCJKThin, "/usr/share/fonts/opentype/noto/NotoSansCJK-Thin.ttc");
