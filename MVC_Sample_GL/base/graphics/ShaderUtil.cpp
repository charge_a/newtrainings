#include "ShaderUtil.h"

#include <string>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

string readFile(const char* filePath)
{
  string content;
  ifstream fs(filePath, ios::in);
  if (fs.is_open())
  {
    string line = "";
    while (!fs.eof()) {
      getline(fs, line);
      content.append(line + "\n");
    }
    fs.close();
    return content;
  }

  cerr << "Error reading file " << filePath << endl;
  return "";
}

GLuint compileShader(GLenum type, const char* filePath)
{
  GLuint shader = glCreateShader(type);
  string code  = readFile(filePath);
  const char* shaderCode = code.c_str();

  glShaderSource(shader, 1, &shaderCode, NULL);
  glCompileShader(shader);

  GLint result = GL_FALSE;
  int logLength;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
  if (logLength > 1)
  {
    vector<char> errorMessage(logLength);
    glGetShaderInfoLog(shader, logLength, NULL, &errorMessage[0]);
    cerr << "Shader Compilation Error in " << filePath << ": \n" << &errorMessage[0] << endl;
    return 0;
  }
  cout << "Shader compiled successfully: " << filePath << endl;
  return shader;
}

bool checkProgramStatus(GLuint program)
{
  GLint result = GL_FALSE;
  int logLength;
  glGetProgramiv(program, GL_LINK_STATUS, &result);
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
  if (logLength > 1) {
    vector<char> errorMessage (logLength);
    glGetProgramInfoLog(program, logLength, NULL, &errorMessage[0]);
    cerr << "Program Linking error: " << &errorMessage[0] << endl;
    return false;
  }
  else {
    cout << "Program Linked successfully: " << program << endl;
    return true;
  }
}

GLuint LoadProgram(const char* vertexShader, const char* fragmentShader)
{
  GLuint vs = compileShader(GL_VERTEX_SHADER, vertexShader);
  GLuint fs = compileShader(GL_FRAGMENT_SHADER, fragmentShader);
  if (vs && fs) {
    GLuint program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);

    if (!checkProgramStatus(program)) {
      program = 0;
    }

    glDeleteShader(vs);
    glDeleteShader(fs);

    return program;
  }
  return 0;
}

GLuint LoadProgram(const char* vertexShader, const char* geometryShader, const char* fragmentShader)
{
  GLuint vs = compileShader(GL_VERTEX_SHADER, vertexShader);
  GLuint gs = compileShader(GL_GEOMETRY_SHADER, geometryShader);
  GLuint fs = compileShader(GL_FRAGMENT_SHADER, fragmentShader);
  if (vs && gs && fs) {
    GLuint program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, gs);
    glAttachShader(program, fs);
    glLinkProgram(program);

    if (!checkProgramStatus(program)) {
      program = 0;
    }

    glDeleteShader(vs);
    glDeleteShader(gs);
    glDeleteShader(fs);

    return program;
  }
  return 0;

}

GLint  MaxVertexAttribs()
{
  // Maximum 4-component attributes like vec4
  GLint n;
  glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &n);
  cout << "MaxVertexAttribs: " << n << endl;
  return n;
}
