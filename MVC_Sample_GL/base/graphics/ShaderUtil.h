#pragma once
#include "../Datatypes.h"

GLuint LoadProgram(const char* vertexShader, const char* fragmentShader);
GLuint LoadProgram(const char* vertexShader, const char* geometryShader, const char* fragmentShader);
GLint  MaxVertexAttribs();
