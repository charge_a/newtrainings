#include "URectRenderer.h"
#include "../MathUtil.h"

URectRenderer::URectRenderer()
{
  mShaderProgram = LoadProgram("base/graphics/shader.vs", "base/graphics/shader.fs");
  glGenVertexArrays(1, &mVAO);
  glBindVertexArray(mVAO);
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
      const GLfloat triangles [] = {
        -1.0, -1.0, 0.0, 1.0, 0.0,
        -1.0, +1.0, 0.0, 1.0, 1.0,
        +1.0, +1.0, 0.0, 0.0, 1.0,

        +1.0, +1.0, 0.0, 0.0, 1.0,
        +1.0, -1.0, 0.0, 0.0, 0.0,
        -1.0, -1.0, 0.0, 1.0, 0.0,
      };
      glBufferData(GL_ARRAY_BUFFER, sizeof(triangles), triangles, GL_STATIC_DRAW);

      GLint aPos = 0;
      glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (GLvoid*)(0));
      glEnableVertexAttribArray(aPos);
      GLint aTex = 1;
      glVertexAttribPointer(aTex, 2, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (GLvoid*)(3*sizeof(GLfloat)));
      glEnableVertexAttribArray(aTex);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);
}

URectRenderer::~URectRenderer() {}

void URectRenderer::render(URect frame, UColor color, UTexture *background)
{
  glUseProgram(mShaderProgram);

  if (background)
  {
    background->activate();
    glUniform1i(glGetUniformLocation(mShaderProgram, "bgImageExists"), true);
    float sx = 1.0;
    float sy = 1.0;
    if (background->displayOption == kUTextureAspectFit)
    {
      URect framePx = NormalToPixel(frame);
      float imageAspectRatio = background->width() / (float)background->height();
      float frameAspectRatio = framePx.z/framePx.w;
      if (imageAspectRatio > frameAspectRatio)
      {
        sy = imageAspectRatio/frameAspectRatio;
      }
      else
      {
        sx = frameAspectRatio/imageAspectRatio;
      }
    }
    glUniform2f(glGetUniformLocation(mShaderProgram, "texScale"), sx, sy);
  }
  else
  {
    glUniform1i(glGetUniformLocation(mShaderProgram, "bgImageExists"), false);
  }

  Vec2 tlGL = ScreenToGLCoordinates(Vec2(frame.x, frame.y));
  UMat m;
  m = glm::translate(m, ScaleAndTranslate(Vec2(-1, 1), tlGL, Vec3(frame.z, frame.w, 1.0)));
  m = glm::scale(m, Vec3(frame.z, frame.w, 1.0));
  UNIFORM_MAT(glGetUniformLocation(mShaderProgram, "m"), m);
  glUniform4f(glGetUniformLocation(mShaderProgram, "bgColor"), color.x, color.y, color.z, color.w);

  glBindVertexArray(mVAO);
    glDrawArrays(GL_TRIANGLES, 0, 6);
  glBindVertexArray(0);

  if (background)
  {
    glBindTexture(GL_TEXTURE_2D, 0);
  }
}
