#pragma once
#include "ShaderUtil.h"
#include "UTexture.h"

class URectRenderer {
private:
  GLint mShaderProgram;
  GLuint mVAO;

public:
  URectRenderer ();
  virtual ~URectRenderer ();

  void render(URect frame, UColor color, UTexture *background);
};
