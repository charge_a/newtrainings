#include "UTextRenderer.h"
#include "../MainWindow.h"
#include "../MathUtil.h"

UTextRenderer::UTextRenderer()
{
  mShaderProgram = LoadProgram("base/graphics/shader_text.vs", "base/graphics/shader_text.fs");
  glGenVertexArrays(1, &mVAO);
  glBindVertexArray(mVAO);
    glGenBuffers(1, &mVBO);
    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
      glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*6*4, NULL, GL_DYNAMIC_DRAW);
      glEnableVertexAttribArray(0);
      glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4*sizeof(GLfloat), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

}

UTextRenderer::~UTextRenderer() {}

void UTextRenderer::render(URect frame, std::string text, Font &font, float fontScale, UColor fontColor, TextAlignment alignment)
{
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glUseProgram(mShaderProgram);
  glActiveTexture(GL_TEXTURE0);
  glBindVertexArray(mVAO);
    glUniform3f(glGetUniformLocation(mShaderProgram, "textColor"), fontColor.x, fontColor.y, fontColor.z);

    Vec2 tlGL = ScreenToGLCoordinates(Vec2(frame.x, frame.y));
    UMat m;
    m = glm::translate(m, ScaleAndTranslate(Vec2(-1, 1), tlGL, Vec3(frame.z, frame.w, 1.0)));
    // m = glm::scale(m, Vec3(frame.z, frame.w, 1.0));
    m = glm::scale(m, Vec3(VP_H/VP_W, 1.0, 1.0));
    UNIFORM_MAT(glGetUniformLocation(mShaderProgram, "m"), m);

    font.loadTexturesForFontSize(fontScale);
    float scale = 1.0/VP_H;//PX_H(fontScale / 20.0);
    float len = font.computeTextLength(text, fontScale) * scale;
    float x = 0.0;
    float y = -(font.getBoundingBox('X', fontScale).w * scale)/2;
    if (alignment == kTextAlignmentLeft) 
    {
      x = -frame.z*(VP_W/VP_H);
    }
    else if (alignment == kTextAlignmentCentre)
    {
      x = -len/2;
    }
    else if (alignment == kTextAlignmentRight)
    {
      x = frame.z*(VP_W/VP_H) - len;
    }
    for (std::string::const_iterator c = text.begin(); c != text.end(); c++)
    {
      URect boundingBox = font.getBoundingBox(*c, fontScale) * scale;
      GLfloat xpos = x + boundingBox.x;
      GLfloat ypos = y + boundingBox.y;

      GLfloat w = boundingBox.z;
      GLfloat h = boundingBox.w;
      // Update VBO for each character
      GLfloat vertices[6][4] = {
          { xpos,     ypos + h,   0.0, 0.0 },
          { xpos,     ypos,       0.0, 1.0 },
          { xpos + w, ypos,       1.0, 1.0 },

          { xpos,     ypos + h,   0.0, 0.0 },
          { xpos + w, ypos,       1.0, 1.0 },
          { xpos + w, ypos + h,   1.0, 0.0 }
      };
      // Render glyph texture over quad
      glBindTexture(GL_TEXTURE_2D, font.getTexture(*c, fontScale));
      // Update content of VBO memory
      glBindBuffer(GL_ARRAY_BUFFER, mVBO);
      glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
      glBindBuffer(GL_ARRAY_BUFFER, 0);

      glDrawArrays(GL_TRIANGLES, 0, 6);

      x += font.getAdvance(*c, fontScale) * scale;
    }

  glBindVertexArray(0);
}
