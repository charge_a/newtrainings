#pragma once
#include "ShaderUtil.h"
#include "Font.h"

enum TextAlignment
{
  kTextAlignmentLeft,
  kTextAlignmentCentre,
  kTextAlignmentRight
};

class UTextRenderer {
private:
  GLint mShaderProgram;
  GLuint mVAO;
  GLuint mVBO;

public:
  UTextRenderer ();
  virtual ~UTextRenderer ();

  void render(URect frame, std::string text, Font& font, float fontScale, UColor fontColor, TextAlignment alignment);
};
