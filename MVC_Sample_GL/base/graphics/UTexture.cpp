#include "UTexture.h"
#include <map>
#include <SOIL.h>

std::map<std::string, GLuint> gTextureCache;

UTexture::UTexture(std::string imagePath, int channels)
{
  if (gTextureCache.find(imagePath) == gTextureCache.end()) {
    GLuint id;
    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);
    // texture loading
    unsigned char* pixels;
    int w, h;
    if (channels == 3) {
      pixels = SOIL_load_image(imagePath.c_str(), &w, &h, 0, SOIL_LOAD_RGB);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, pixels);
    }
    else {
      pixels = SOIL_load_image(imagePath.c_str(), &w, &h, 0, SOIL_LOAD_RGBA);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    }
    SOIL_free_image_data(pixels);
    glGenerateMipmap(GL_TEXTURE_2D);
    // texture setings
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // repeat beyound s=(0,1)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // repeat beyound t=(0,1)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); // down sampling scheme
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // up sampling scheme

    glBindTexture(GL_TEXTURE_2D, 0);

    gTextureCache[imagePath] = id;
  }
  mTexture = gTextureCache[imagePath];
  displayOption = kUTextureScaleToFill;
}

UTexture::UTexture (GLuint texture) { mTexture = texture; }

UTexture::~UTexture() {}

void UTexture::activate()
{
  activateAt(GL_TEXTURE0);
}
void UTexture::activateAt(GLenum unit)
{
  glActiveTexture(unit);
  glBindTexture(GL_TEXTURE_2D, mTexture);
}
void UTexture::activateAt(GLenum unit, GLint samplerLoc)
{
  activateAt(unit);
  glUniform1i(samplerLoc, unit - GL_TEXTURE0);
}
int UTexture::width()
{
  activate();
  int w;
  int miplevel = 0;
  glGetTexLevelParameteriv(GL_TEXTURE_2D, miplevel, GL_TEXTURE_WIDTH, &w);
  return w;
}
int UTexture::height()
{
  activate();
  int h;
  int miplevel = 0;
  glGetTexLevelParameteriv(GL_TEXTURE_2D, miplevel, GL_TEXTURE_HEIGHT, &h);
  return h;
}
