#pragma once
#include "../Datatypes.h"

enum UTextureDisplay
{
  kUTextureScaleToFill,
  kUTextureAspectFit
};

class UTexture {
private:
  GLuint mTexture;

public:
  UTexture (std::string imagePath, int channels);
  UTexture (GLuint texture);
  virtual ~UTexture ();

  void activate();
  void activateAt(GLenum unit);
  void activateAt(GLenum unit, GLint samplerLoc);

  int width();
  int height();

  UTextureDisplay displayOption;
};
