#version 330 core

out vec4 color;

uniform vec4 bgColor;

uniform sampler2D bgImage;
uniform bool bgImageExists;
in vec2 vTex;

void main()
{
  vec4 colTex = texture(bgImage, vTex);
  vec4 colSolid = vec4(bgColor);
  if (bgImageExists && vTex.x >= 0 && vTex.x <= 1 && vTex.y >= 0 && vTex.y <= 1)
  {
    color = colTex * (colTex.a) + colSolid * (1.0 - colTex.a);
  }
  else
  {
    color = colSolid;
  }
}
