#version 330 core
layout(location=0) in vec3 pos;
layout(location=1) in vec2 tex;

uniform mat4 m;
uniform vec2 texScale;

out vec2 vTex;

void main()
{
  gl_Position = m*vec4(pos, 1.0);
  vTex = vec2(tex.x, 1-tex.y) * texScale;
  vTex += (vec2(1.0) - texScale) / 2.0;
}
