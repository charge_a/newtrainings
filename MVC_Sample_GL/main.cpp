#include "base/MainWindow.h"
// #include "base/ViewController.h"
#include "app/VideoPlayer/ViewController.h"
using namespace VP;

#include "base/graphics/DrawingUtil.h"
#include "base/MathUtil.h"

ViewController v;

void renderLoop () {
  glClear(GL_COLOR_BUFFER_BIT);
  v.mView->render(URect(0,0,1,1));
  v.updateViewModel();
}

int main(int argc, char const *argv[]) {
  MainWindow::Instance().initWithSize(Vec2(600,300), v.mView);
  v.initialize();
  glEnable(GL_MULTISAMPLE);
  glClearColor(1.0, 1.0, 1, 1.0);
  MainWindow::Instance().startRenderLoop(renderLoop);
  return 0;
}
