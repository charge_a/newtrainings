#include "UButton.h"
#include "../base/graphics/DrawingUtil.h"

UButton::UButton (std::string name, URect frame, int frameFlag)
: UView(name, frame, frameFlag), mButtonText(""), mTextAlignment(kTextAlignmentLeft) {}

UButton* UButton::setButtonText(std::string text)
{
  mButtonText = text;
  return this;
}
UButton* UButton::setFontSize(float size)
{
  mFontSize = size;
  return this;
}
UButton* UButton::setFontColor(UColor color)
{
  mFontColor = color;
  return this;
}
std::string UButton::getButtonText() { return mButtonText; }
float UButton::getFontSize() { return mFontSize; }
UColor UButton::getFontColor() { return mFontColor; }

UButton* UButton::setTextAlignment(TextAlignment alignment)
{
  mTextAlignment = alignment;
  return this;
}
TextAlignment UButton::getTextAlignment() { return mTextAlignment; }


void UButton::renderSelf(URect actualRect) {
  UView::renderSelf(actualRect);

  if (mButtonText != "") {
    DrawingUtil::Instance().DrawText(actualRect, mButtonText, mFontSize, mFontColor, mTextAlignment);
  }
}

UView* UButton::processAttribute(const char* key, const char* value)
{
  UView::processAttribute(key, value);

  if (ISKEY(key, "title"))
  {
    setButtonText(value);
  }
  if (ISKEY(key, "font-size"))
  {
    float size;
    sscanf(value, "%f", &size);
    setFontSize(size);
  }
  if (ISKEY(key, "font-color"))
  {
    float r, g, b, a;
    sscanf(value, "%f,%f,%f,%f", &r, &b, &g, &a);
    setFontColor(UColor(r, g, b, a));
  }
  if (ISKEY(key, "text-align"))
  {
    std::string align(value);
    if (align == "left") setTextAlignment(kTextAlignmentLeft);
    if (align == "center") setTextAlignment(kTextAlignmentCentre);
    if (align == "right") setTextAlignment(kTextAlignmentRight);
  }
}