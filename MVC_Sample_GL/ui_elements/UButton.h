#pragma once
#include <UView.h>
#include "../base/graphics/UTextRenderer.h"

class UButton : public UView {
private:
  std::string mButtonText;
  float mFontSize;
  UColor mFontColor;
  TextAlignment mTextAlignment;

public:
  UButton (std::string name, URect frame, int frameFlag);

  UButton* setButtonText(std::string text);
  UButton* setFontSize(float size);
  UButton* setFontColor(UColor color);
  UButton* setTextAlignment(TextAlignment alignment);
  TextAlignment getTextAlignment();

  std::string getButtonText();
  float getFontSize();
  UColor getFontColor();

  virtual void renderSelf (URect actualRect);

  virtual UView* processAttribute(const char* key, const char* value);

};
