#include "UFPSViewer.h"
#include <DrawingUtil.h>

UFPSViewer::UFPSViewer(std::string name, URect frame, int frameFlag)
: UView("__FPSViewer__", URect(-100, 2, 100-2, 50), X_ABS|Y_ABS|H_ABS|W_ABS)
{
  mLastTime = high_resolution_clock::now();
  setBackgroundColor(UColor(0,0,0,0.6));
}

void UFPSViewer::renderSelf (URect actualRect)
{
  high_resolution_clock::time_point t = high_resolution_clock::now();
  auto duration = duration_cast<milliseconds> (t - mLastTime).count();
  mLastTime = t;

  // printf("%f\n", duration);

  UView::renderSelf(actualRect);
  DrawingUtil::Instance().DrawText(actualRect, "FPS: " + std::to_string(int(1000.0/duration)), 40, UColor(1), kTextAlignmentCentre);
}
