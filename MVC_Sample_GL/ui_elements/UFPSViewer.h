#pragma once
#include <UView.h>
#include <chrono>

using namespace std;
using namespace std::chrono;

class UFPSViewer : public UView
{
  high_resolution_clock::time_point mLastTime;
public:
  UFPSViewer(std::string name, URect frame, int frameFlag);
  virtual void renderSelf (URect actualRect);
  
};