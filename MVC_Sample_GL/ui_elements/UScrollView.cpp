#include "UScrollView.h"
#include "../base/MainWindow.h"
#include "../base/MathUtil.h"

namespace UScrollViewCustomComponents {
  /*
  UI Element representing Scroll Bar.
   */
  class Track : public UView {
  private:

    double mPlacementY;
    double mValueY;
    double mScrollAmountY;
    URect mParentBounds;
    URect mActualFrame;
    FUN2(void, double, double) mScrollOffsetChanged;

    const double SCROLL_OFFSET = 0.001;

  public:
    Track (std::string name, URect frame, int frameFlag)
    : UView(name, frame, frameFlag)
    {
      setDragHandler(CALLBACK2(&Track::dragHandler));
      mPlacementY = 0;
      mValueY = 0;
      mScrollAmountY = 0;
    }
    virtual ~Track () {}

    void dragHandler(double x, double y)
    {
      mPlacementY = y;
    }

    void updateOffset(double xoffset, double yoffset)
    {
      if (mPlacementY < mParentBounds.y + mActualFrame.w/2) {
        mPlacementY = mParentBounds.y + mActualFrame.w/2;
      }
      else if (mPlacementY >= mParentBounds.y + mParentBounds.w - mActualFrame.w/2) {
        mPlacementY = mParentBounds.y + mParentBounds.w - mActualFrame.w/2;
      }
      mPlacementY += mScrollAmountY * yoffset;
    }

    void renderSelf(URect actualRect)
    {
      if (mPlacementY >= 0) {
        double newY = mPlacementY - (actualRect.w / 2);
        if (newY < mParentBounds.y)
        {
          newY = mParentBounds.y;
        }
        else if (newY + actualRect.w >= mParentBounds.y + mParentBounds.w)
        {
          newY = mParentBounds.y + mParentBounds.w - actualRect.w;
        }
        actualRect.y = newY;
      }
      mActualFrame = actualRect;
      double valueY = (mActualFrame.y - mParentBounds.y) / (mParentBounds.w - mActualFrame.w);
      if (fabs(mParentBounds.w - mActualFrame.w) == 0.0)
      {
        valueY = 0.0;
      }

      if (valueY != mValueY) {
        mValueY = valueY;
        mScrollOffsetChanged(0, mValueY);
      }

      if (mFrame.w < 1.0) {
        UView::renderSelf(actualRect);
      }
    }


    void setScrollBounds (URect scrollBounds)
    {
      mParentBounds = scrollBounds;
    }

    void adjustToExtent (double eY, double eYPx)
    {
      mFrame.w = 1.0 / std::max(eY, 1.0);
      mFrame.w = std::max(mFrame.w, 0.1f);
      mScrollAmountY = 10 / eYPx; 
    }

    void setScrollListener (FUN2(void, double, double) scrollListener)
    {
      mScrollOffsetChanged = scrollListener;
    }

  };

  /*
    A UI Element to show only a window of its child scene rendered.
   */
  class ContentView : public UView {
  private:
    double mOffsetY;
    double mContentExtentY;
    double mContentExtentYPx;

  public:
    ContentView (std::string name, URect frame, int frameFlag)
    : UView(name, frame, frameFlag)
    {
      mOffsetY = 0;
      mContentExtentY = 1.0;
      mContentExtentYPx = 0.0;
    }
    virtual ~ContentView () {}

    void render(URect parentRect)
    {
      if (mIsDirty)
      {
        computeExtent(parentRect);
      }

      GLint xywh[4];
      glGetIntegerv(GL_VIEWPORT, xywh);

      URect actualRect = RectInsideRect(parentRect, computeRenderFrame(parentRect));
      glViewport(xywh[0] + actualRect.x * VP_W, xywh[1] + (1-actualRect.y-actualRect.w) * VP_H,
                 actualRect.z * VP_W, actualRect.w * VP_H);

        for (UView* sv: mSubUViews)
          if (sv)
            sv->render(URect(0,-mOffsetY,1,1));

      glViewport(xywh[0], xywh[1], xywh[2], xywh[3]);

      mIsDirty = false;

    }

    void changeOffset (double x, double y)
    {
      mOffsetY = y * (mContentExtentY - 1.0);
    }

    void computeExtent(URect parentRect)
    {
      mContentExtentY = 0;

      GLint xywh[4];
      glGetIntegerv(GL_VIEWPORT, xywh);

      URect actualRect = RectInsideRect(parentRect, computeRenderFrame(parentRect));
      URect newRect = URect(0,0,1,1);
      glViewport(xywh[0] + actualRect.x * VP_W, xywh[1] + (1-actualRect.y-actualRect.w) * VP_H,
                 actualRect.z * VP_W, actualRect.w * VP_H);

        for (UView* sv: mSubUViews)
          if (sv) {
            URect r = RectInsideRect(newRect, sv->computeRenderFrame(newRect));
            mContentExtentY = std::max(mContentExtentY, (double)r.y + r.w);
          }

      mContentExtentYPx = mContentExtentY * VP_H;

      glViewport(xywh[0], xywh[1], xywh[2], xywh[3]);
    }

    double getExtentY() { return mContentExtentY; }
    double getExtentYPx() { return mContentExtentYPx; }
    double getOffsetY() { return mOffsetY; }

    void onHover(URect inRect, UPoint2 atPoint)
    {
      URect actualRect = RectInsideRect(inRect, computeRenderFrame(inRect));
      if (RectContainsPoint(actualRect, atPoint))
      {
        atPoint.x = (atPoint.x - actualRect.x) / actualRect.z;
        atPoint.y = (atPoint.y - actualRect.y) / actualRect.w;

        GLint xywh[4];
        glGetIntegerv(GL_VIEWPORT, xywh);
        glViewport(xywh[0] + actualRect.x * VP_W, xywh[1] + (1-actualRect.y-actualRect.w) * VP_H,
                   actualRect.z * VP_W, actualRect.w * VP_H);

        for (UView* sv: mSubUViews)
          if (sv)
            sv->onHover(URect(0,-mOffsetY,1,1), atPoint);

        glViewport(xywh[0], xywh[1], xywh[2], xywh[3]);
      }
    }
  };
} /* UScrollViewCustomComponents */

UScrollView::UScrollView (std::string name, URect frame, int frameFlag)
: UView(name, frame, frameFlag)
{
  mContentView = new UScrollViewCustomComponents::ContentView("scrollable_content", URect(0,0,-10,1), W_ABS);
  UView::addSubview(mContentView);

  mVerticalScroll = new UScrollViewCustomComponents::Track("vert_scroll", URect(-10,0, 0, 0.1), X_ABS);
  mVerticalScroll->setBackgroundColor(UColor(1,0,0,1));
  UView::addSubview(mVerticalScroll);

  ((UScrollViewCustomComponents::Track*)mVerticalScroll)->setScrollListener(CALLBACK2(&UScrollView::onVerticalScroll));

}

void UScrollView::renderSelf(URect actualRect)
{
  ((UScrollViewCustomComponents::Track*)mVerticalScroll)->setScrollBounds(actualRect);
  ((UScrollViewCustomComponents::Track*)mVerticalScroll)->adjustToExtent(((UScrollViewCustomComponents::ContentView*)mContentView)->getExtentY(), ((UScrollViewCustomComponents::ContentView*)mContentView)->getExtentYPx());
  UView::renderSelf(actualRect);
}

void UScrollView::onVerticalScroll(double x, double y)
{
  ((UScrollViewCustomComponents::ContentView*)mContentView)->changeOffset(x, y);
}

UView* UScrollView::addSubview(UView* view)
{
  mContentView->addSubview(view);
  return this;
}

UView* UScrollView::getContentView()
{
  return mContentView;
}

void UScrollView::onMouseScroll(URect inRect, UPoint2 atPoint, UPoint2 offset) 
{
  URect actualRect = RectInsideRect(inRect, computeRenderFrame(inRect));

  if (RectContainsPoint(actualRect, atPoint))
  {
    ((UScrollViewCustomComponents::Track*)mVerticalScroll)->updateOffset(offset.x, offset.y);

  }
  UView::onMouseScroll(inRect, atPoint, offset);

}

float UScrollView::getContentOffsetY()
{
  return ((UScrollViewCustomComponents::ContentView*)mContentView)->getOffsetY();
}

float UScrollView::getContentExtentY()
{
  return ((UScrollViewCustomComponents::ContentView*)mContentView)->getExtentY();
}
