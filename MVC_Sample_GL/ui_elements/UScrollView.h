#pragma once
#include <UView.h>

class UScrollView : public UView {
protected:

  UView* mVerticalScroll;
  UView* mContentView;

  void onVerticalScroll(double x, double y);

public:
  UScrollView (std::string name, URect frame, int frameFlag);

  virtual void renderSelf (URect actualRect);
  // UScrollView* setTrackColor(UColor color);

  virtual UView* addSubview(UView* view);

  UView* getContentView();

  virtual void onMouseScroll(URect inRect, UPoint2 atPoint, UPoint2 offset);

  float getContentOffsetY();
  float getContentExtentY();
};
