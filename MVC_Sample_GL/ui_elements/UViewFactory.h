#pragma once
#include <IViewFactory.h>
#include <stdlib.h>
#include "UButton.h"
#include "UScrollView.h"
#include "UFPSViewer.h"

class UViewFactory : public IViewFactory
{
  UViewFactory() {}
public:
  static UViewFactory* Instance() {
    static UViewFactory sInstance;
    return &sInstance;
  }
  UView* getClassForTag(const char* tag)
  {
    if (!strcmp(tag, "view"))
    {
      return new UView("aaewqe", URect(0,0,1,1), 0);
    }
    if (!strcmp(tag, "button"))
    {
      return new UButton("aaewqe", URect(0,0,1,1), 0);
    }
    if (!strcmp(tag, "scrollview"))
    {
      return new UScrollView("aaewqe", URect(0,0,1,1), 0);
    }
    if (ISKEY(tag, "fps"))
    {
      return new UFPSViewer("aaweqwe", URect(0,0,1,1), 0);
    }
    printf("Error: undefined tag: %s\n", tag);
    return NULL;
  }
  
};