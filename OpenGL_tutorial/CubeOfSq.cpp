#include "basecode/BaseGLRender.h"

CubeOfSq::CubeOfSq()
{
}

void CubeOfSq::render() {

  renderBefore();

  glm::mat4 id;
  id = glm::rotate(id, glm::radians(angle), glm::vec3(1, 1, 0));
  id = glm::scale(id, glm::vec3(0.5, 0.5, 0.5));
  double t = angle / 100.0;

  if ((int)floor(t) % 2 == 0)
  {
    t = t - (int)floor(t);
  }
  else
  {
    t = 1 - (t - (int)floor(t));
  }
  t *= 1.2;

  model = glm::translate(id, glm::vec3(0,0,-.5+t));
  SquareMVP::render();

  model = glm::translate(id, glm::vec3(0,0,.5-t));
  SquareMVP::render();

  model = glm::translate(id, glm::vec3(-0.5+t,0,0));
  model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0,1,0));
  SquareMVP::render();

  model = glm::translate(id, glm::vec3(0.5-t,0,0));
  model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0,1,0));
  SquareMVP::render();

  model = glm::translate(id, glm::vec3(0,0.5-t,0));
  model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1,0,0));
  SquareMVP::render();

  model = glm::translate(id, glm::vec3(0,-0.5+t,0));
  model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1,0,0));
  SquareMVP::render();

  angle += .5;

  renderAfter();
}
