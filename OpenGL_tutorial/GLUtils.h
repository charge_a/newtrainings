#ifndef GLUTILS_H
#define GLUTILS_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#define BUFFER_OFFSET(n) ((GLvoid*)(n))

GLuint LoadProgram(const char* vertexShader, const char* fragmentShader);
GLuint LoadProgram(const char* vertexShader, const char* geometryShader, const char* fragmentShader);
GLint  MaxVertexAttribs();
int getScreenWidth();
int getScreenHeight();

struct GLGlobalInstances {
private:
  GLFWwindow* glMainWindow;
  GLGlobalInstances() {}

public:
  static GLGlobalInstances& instance() {
    static GLGlobalInstances _instance;
    return _instance;
  }

  void setWindow(GLFWwindow* window) {
    glMainWindow = window;
  }
  GLFWwindow* getWindow() {
    return glMainWindow;
  }

};

#endif
