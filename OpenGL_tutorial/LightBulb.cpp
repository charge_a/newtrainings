#include "basecode/BaseGLRender.h"
#include "basecode/VertexData.h"
#include "basecode/Camera.h"

LightBulb::LightBulb() {
  int v_size;
  GLfloat* v_data = VDProvider::getCubePositionData(&v_size);
  addVertexAttributes(0, v_data, v_size, 3);
  addShaders("shaders/lightBulb_vs.glsl", "shaders/lightBulb_fs.glsl");

  pos = glm::vec3(0.0, 0.5, -0.5);
  angle = 0.0;

  CAM::instance()->setCamaraAt(glm::vec3(0.0, 0.0, 10.0));
}

void LightBulb::render() {

  angle += 0.25;
  if (angle > 360) angle -= 360;
  float t = glm::radians(angle);
  glm::vec3 lightSourcePos(sin(t), sin(t*2), cos(t));
  pos = lightSourcePos * 0.4f;

  float s = 0.05;
  m = glm::translate(m, pos);
  m = glm::scale(m, glm::vec3(s, s, s));

  BaseGLRender::render(GL_TRIANGLES, 36);
}

Pivot::Pivot()
{
  int v_size;
  GLfloat* v_data = VDProvider::getCubePositionData(&v_size);
  addVertexAttributes(0, v_data, v_size, 3);
  addShaders("shaders/basic_vs.glsl", "shaders/basic_fs.glsl");
}

void Pivot::render() {
  double s = 0.01;
  m = glm::scale(m, glm::vec3(s, s, s));
  m = glm::translate(m, glm::vec3(0, 0, 0));

  glUniform3f(getUniformLocation("solidColor"), 1.0, 0.0, 0.0);
  BaseGLRender::render(GL_TRIANGLES, 36);
}
