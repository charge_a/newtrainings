#include "basecode/BaseGLRender.h"
#include "basecode/Camera.h"
#include "basecode/VertexData.h"

LightingTarget::LightingTarget()
{
  int p_size, n_size;
  GLfloat* vertices = VDProvider::getCubePositionData(&p_size);
  GLfloat* normals = VDProvider::getCubeNormalData(&n_size);

  addVertexAttributes(0, vertices, p_size, 3);
  addVertexAttributes(1, normals, n_size, 3);
  addShaders("shaders/lighttargetMVP.vsh", "shaders/lighttargetMVP.fsh");

  angle = 0.0;
}

void LightingTarget::render() {
  float s = 0.3;
  m = glm::scale(m, glm::vec3(s, s, s));

  // m = glm::rotate(m, camX, glm::vec3(1.0f, 1.0f, 0.0f));
  glUniform3f(getUniformLocation("objColor"), 1.0f, 0.5f, 0.31f);
  glUniform3f(getUniformLocation("lightColor"), 1.0f, 1.0f, 1.0f);

  // double t = glfwGetTime();
  // glm::vec3 lightSourcePos(1.2f, 1.0f, 2.0f);
  glUniform3f(getUniformLocation("lightSource"), lightPos.x, lightPos.y, lightPos.z);


  BaseGLRender::render(GL_TRIANGLES, 36);

}
