#include "basecode/BaseGLRender.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

RotateSq::RotateSq()
: BaseGLRender::BaseGLRender()
{
  const GLfloat pos[] =
  {
    -0.5, -0.5, 0.0,
     0.5, -0.5, 0.0,
     0.5,  0.5, 0.0,
    -0.5,  0.5, 0.0
  };
  const GLuint indices[] =
  {
    0, 1, 2,
    0, 2, 3
  };
  addVertexAttributes(0, pos, sizeof(pos), 3);
  addVertexIndices(indices, sizeof(indices));
  addShaders("shaders/simple0.vsh", "shaders/simple0.fsh");

  angle  = 0.0f;
}

void RotateSq::render()
{
  renderBefore();
  glm::mat4 trans;
  trans = glm::translate(trans, glm::vec3(1.0f, 1.0f, 0.0f));
  trans = glm::rotate(trans, glm::radians(angle), glm::vec3(0.0f, 0.0f, 1.0f));
  GLuint loc = getUniformLocation("transform");
  glUniformMatrix4fv(                     // uniform is mat4 float
    loc,      // uniform var in shaders
    1, GL_FALSE, glm::value_ptr(trans)    // 1 matrix, col-major not swapped, matrix raw pointer
  );
  BaseGLRender::render(GL_TRIANGLES, 6);
  renderAfter();

  angle += 1.0f;
}
