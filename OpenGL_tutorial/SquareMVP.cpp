#include "basecode/BaseGLRender.h"
#include "GLUtils.h"
#include <iostream>

SquareMVP::SquareMVP()
{
  const GLfloat pos[] =
  {
    0.5,  0.5,  0.0,
    0.5, -0.5,  0.0,
   -0.5, -0.5,  0.0,
   -0.5,  0.5,  0.0
  };
  const GLfloat texCoord[] =
  {
    1.0, 1.0,
    1.0, 0.0,
    0.0, 0.0,
    0.0, 1.0
  };
  const GLuint indices[] =
  {
    0, 1, 2,
    0, 2, 3
  };
  addVertexAttributes(0, pos, sizeof(pos), 3);
  addVertexAttributes(1, texCoord, sizeof(texCoord), 2);
  addVertexIndices(indices, sizeof(indices));
  addTextureUnit(GL_TEXTURE0, "texture/face.jpg");
  addShaders("shaders/simpleMVP.vsh", "shaders/simpleMVP.fsh");

  angle = 0;
}

void SquareMVP::render() {
  renderBefore();

  // projection = glm::perspective(45.0f, getScreenWidth() / (float) getScreenHeight(), 0.1f, 10.0f);
  setUniformMatrix("m", model);
  setUniformMatrix("v", view);
  setUniformMatrix("p", projection);

  renderTextureUnit(GL_TEXTURE0, "imgTexture1");
  BaseGLRender::render(GL_TRIANGLES, 6);
  renderAfter();

  angle += .2;
}
