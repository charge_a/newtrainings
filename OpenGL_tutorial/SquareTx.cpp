#include "basecode/BaseGLRender.h"


SquareTx::SquareTx()
: BaseGLRender::BaseGLRender()
{
  const GLfloat vpos[] =
  {
     0.5,  0.5,  0.0,
     0.5, -0.5,  0.0,
    -0.5, -0.5,  0.0,
    -0.5,  0.5,  0.0
  };
  const GLfloat vcol[] =
  {
    1.0, 0.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 0.0, 1.0,
    1.0, 1.0, 0.0
  };
  const GLfloat vtex[] =
  {
    1.0, 1.0,
    1.0, 0.0,
    0.0, 0.0,
    0.0, 1.0
  };
  const GLuint indices[] =
  {
    0, 1, 2,
    0, 2, 3
  };

  addVertexAttributes(0, vpos, sizeof(vpos), 3);
  addVertexAttributes(1, vcol, sizeof(vcol), 3);
  addVertexAttributes(2, vtex, sizeof(vtex), 2);
  addVertexIndices(indices, sizeof(indices));
  addTextureUnit(GL_TEXTURE0, "texture/timeline.jpg");
  addTextureUnit(GL_TEXTURE1, "texture/face.jpg");
  addShaders("shaders/textureShader.vsh", "shaders/textureShader2.fsh");
}

void SquareTx::render()
{
  renderBefore();
  // glDrawArrays(GL_TRIANGLES, 0, count);

  renderTextureUnit(GL_TEXTURE0, "imgTexture1");
  renderTextureUnit(GL_TEXTURE1, "imgTexture2");

  BaseGLRender::render(GL_TRIANGLES, 6);

  renderAfter();
}
