#include "Triangle.h"
#include "GLUtils.h"
#include <iostream>
#include <assert.h>

TriangleSH::TriangleSH()
{
  const GLfloat vertices[] =
  {
    // position        // color
     0.5,  0.0,  0.0,  1.0,  0.0,  0.0,
    -0.5,  0.0,  0.0,  0.0,  1.0,  0.0,
     0.0,  1.0,  0.0,  0.0,  0.0,  1.0
  };
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // attrib 0 - position
    // attrib | dimension | type | normalised | stride | offset
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), BUFFER_OFFSET(0));
    glEnableVertexAttribArray(0);
    // attrib 1 - color
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), BUFFER_OFFSET(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);
  glBindVertexArray(0);
  pu = LoadProgram("shaders/simple.vsh", "shaders/simple.fsh");
  // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

void
TriangleSH::render()
{
  glUseProgram(pu);
  glBindVertexArray(vao);

  // GLint appColorLoc = glGetUniformLocation(pu, "appColor");
  // assert(appColorLoc != -1);
  // glUniform4f(appColorLoc, 0.0f, 0.5f, 0.0f, 1.0f); // pass green color to shaders.
  glDrawArrays(GL_TRIANGLES, 0, 3);

  glBindVertexArray(0);
  glUseProgram(0);
}

TriangleSH::~TriangleSH()
{

}
