#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <GL/glew.h>

class TriangleSH {
private:
  GLuint pu;
  GLuint vao;

public:
  TriangleSH ();
  virtual ~TriangleSH ();
  void render();
};
#endif
