#include "TriangleTexture.h"
#include "GLUtils.h"
#include "SOIL.h"
#include <iostream>

TriangleTx::TriangleTx()
{
  const GLfloat vertices[] =
  {
    // position        // color          // texture
     0.5,  0.0,  0.0,  1.0,  0.0,  1.0,  0.0,  1.0,
    -0.5,  0.0,  0.0,  0.0,  1.0,  0.0,  0.0,  0.0,
     0.0,  1.0,  0.0,  0.0,  0.0,  1.0,  1.0,  0.0
  };
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // attrib 0 - position
    // attrib | dimension | type | normalised | stride | offset
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), BUFFER_OFFSET(0));
    glEnableVertexAttribArray(0);
    // attrib 1 - color
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), BUFFER_OFFSET(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);
    // attrib 2 - texture
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), BUFFER_OFFSET(6 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);
    // texture
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
      // texture setings
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT); // repeat beyound s=(0,1)
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT); // repeat beyound t=(0,1)
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); // down sampling scheme
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // up sampling scheme
      // texture loading
      int w, h;
      unsigned char* pixels = SOIL_load_image("texture/timeline.jpg", &w, &h, 0, SOIL_LOAD_RGB);
      std::cout << "Image dimensions: " << w << "," << h << '\n';
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, pixels);
      glGenerateMipmap(GL_TEXTURE_2D);
      SOIL_free_image_data(pixels);
    // glBindTexture(GL_TEXTURE_2D, 0);
  glBindVertexArray(0);
  pu = LoadProgram("shaders/textureShader.vsh", "shaders/textureShader.fsh");
  // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

TriangleTx::~TriangleTx()
{

}

void
TriangleTx::render()
{
  glUseProgram(pu);
  glBindVertexArray(vao);

  glDrawArrays(GL_TRIANGLES, 0, 3);

  glBindVertexArray(0);
  glUseProgram(0);
}
