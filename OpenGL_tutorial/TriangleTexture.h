#ifndef TRIANGLE_TEXTURE_H
#define TRIANGLE_TEXTURE_H

#include <GL/glew.h>
class TriangleTx {
private:
  GLuint pu, vao;

public:
  TriangleTx ();
  virtual ~TriangleTx ();
  void    render();
};

#endif
