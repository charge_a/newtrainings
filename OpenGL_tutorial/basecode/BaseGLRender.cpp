#include "BaseGLRender.h"
#include "Camera.h"
#include <SOIL.h>
#include <iostream>

BaseGLRender::BaseGLRender()
{
  glGenVertexArrays(1, &vao);
  // texture setings
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT); // repeat beyound s=(0,1)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT); // repeat beyound t=(0,1)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); // down sampling scheme
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // up sampling scheme
  isElements  = false;
}

void BaseGLRender::addVertexAttributes(int attr, const GLfloat data[], int data_size, const int attr_dim) {
  glGenBuffers(1, &vbo); std::cout << "VBO created: " << vbo << '\n';
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, data_size, data, GL_STATIC_DRAW);
  addVertexAttributesVBO(attr, attr_dim, vbo);
}

void BaseGLRender::addVertexAttributesVBO(int attr, int attr_dim, GLuint p_vbo) {
  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, p_vbo);
  glVertexAttribPointer(attr, attr_dim, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
  glEnableVertexAttribArray(attr);
  glBindVertexArray(0);
}

void BaseGLRender::addVertexIndices(const GLuint indices[], int data_size) {
  glBindVertexArray(vao);
  glGenBuffers(1, &ebo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, data_size, indices, GL_STATIC_DRAW);
  glBindVertexArray(0);
  isElements = true;
}

void BaseGLRender::addVertexIndicesEBO(GLuint ebo) {
  glBindVertexArray(vao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBindVertexArray(0);
  isElements = true;
}

GLuint generateTexture(const char* filename) {
  GLuint text;
  glGenTextures(1, &text);
  glBindTexture(GL_TEXTURE_2D, text);

  // texture loading
  int w, h;
  unsigned char* pixels = SOIL_load_image(filename, &w, &h, 0, SOIL_LOAD_RGB);
  assert(pixels);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, pixels);
  std::cout << "Texture generated: " << filename << "(" << w << "x" << h <<")" << '\n';
  glGenerateMipmap(GL_TEXTURE_2D);
  SOIL_free_image_data(pixels);
  return text;
}

void BaseGLRender::addTextureUnit(GLenum text_unit, const char *imagePath) {
  glBindVertexArray(vao);
  glActiveTexture(text_unit);
  GLuint t = generateTexture(imagePath);
  glBindTexture(GL_TEXTURE_2D, t);
  texUnitTexId [text_unit] = t;
  glBindVertexArray(0);
}

void BaseGLRender::addShaders(const char *vertexShader, const char *fragmentShader) {
  pu = LoadProgram(vertexShader, fragmentShader);
}

void BaseGLRender::renderTextureUnit(GLenum text_unit, const char *uniformVar) {
  glActiveTexture(text_unit);
  glBindTexture(GL_TEXTURE_2D, texUnitTexId [text_unit]);
  glUniform1i(glGetUniformLocation(pu, uniformVar), text_unit - GL_TEXTURE0);
}

void BaseGLRender::renderBefore() {
  glUseProgram(pu);
  glBindVertexArray(vao);
}

void BaseGLRender::renderAfter() {
  glUseProgram(0);
  glBindVertexArray(0);
}

void BaseGLRender::render(GLenum primitive_type, int vertex_count) {
  p = glm::perspective(45.0f, (float)getScreenWidth() / (float)getScreenHeight(), 0.1f, 100.0f);
  v = CAM::instance()->getViewMatrix();
  setUniformMatrix("m", m);
  setUniformMatrix("v", v);
  setUniformMatrix("p", p);
  if (isElements) {
    glDrawElements(primitive_type, vertex_count, GL_UNSIGNED_INT, BUFFER_OFFSET(0));
  }
  else {
    glDrawArrays(primitive_type, 0, vertex_count);
  }
}

void BaseGLRender::render1() {
  renderBefore();
  m = v= p = id;
  render();
  renderAfter();
}

GLuint BaseGLRender::getUniformLocation(const char *uniformVar) {
  return glGetUniformLocation(pu, uniformVar);
}

void BaseGLRender::setUniformMatrix(const char *uniformVar, glm::mat4& matrix) {
  GLint loc = glGetUniformLocation(pu, uniformVar);
  if (loc == -1) {
    // std::cerr << "Undefined reference uniform variable name: " << uniformVar << '\n';
    return;
  }
  glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(matrix));
}

GLuint BaseGLRender::getVBO() {
  return vbo;
}
