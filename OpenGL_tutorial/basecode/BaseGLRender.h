#ifndef BADE_GL_RENDER
#define BADE_GL_RENDER

#include <GL/glew.h>
#include <map>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "../GLUtils.h"

class BaseGLRender {
public:
  GLuint pu, vao, vbo, ebo;
  std::map<GLuint, GLuint> texUnitTexId;
  bool isElements;
  glm::mat4 m, v, p, id;

public:
  BaseGLRender ();

  virtual void  render() = 0;

  void    addVertexAttributes (int attr, const GLfloat data[], int data_size, const int attr_dim);
  void    addVertexAttributesVBO (int attr, int attr_dim, GLuint p_vbo);
  void    addVertexIndices (const GLuint indices[], int data_size);
  void    addVertexIndicesEBO (GLuint ebo);
  void    addTextureUnit (GLenum text_unit, const char* imagePath);
  void    addShaders (const char* vertexShader, const char* fragmentShader);

  void    renderTextureUnit (GLenum text_unit, const char* uniformVar);
  void    renderBefore();
  void    render (GLenum primitive_type, int vertex_count);
  void    renderAfter();
  void    render1();

  GLuint  getUniformLocation (const char* uniformVar);
  void    setUniformMatrix(const char* uniformVar, glm::mat4& matrix);
  GLuint  getVBO();
};

class SquareTx : public BaseGLRender{
private:

public:
  SquareTx ();
  void render();
};
class RotateSq : public BaseGLRender{
private:
  float angle;

public:
  RotateSq ();
  void render();
};
class SquareMVP : public BaseGLRender{
private:
  float angle;

public:
  glm::mat4 model, view, projection;

  SquareMVP ();
  virtual void render();

};
class Cube : public BaseGLRender {
private:
  float angle;

public:
  Cube ();
  void render();
};

class CubeOfSq : public SquareMVP {
private:
  float angle;

public:
  CubeOfSq ();
  void render();
};

class LightingTarget : public BaseGLRender {
protected:
  GLuint lightVAO;

public:
  float angle;
  glm::vec3 lightPos;
  LightingTarget ();
  void render();
};

class LightBulb : public BaseGLRender {
private:

public:
  float angle;
  glm::vec3 pos;
  LightBulb ();
  void render();
};

class Pivot : public BaseGLRender {
private:


public:
  Pivot ();
  void render();
};
#endif
