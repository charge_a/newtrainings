#include "Camera.h"
#include <GLFW/glfw3.h>
#include <stdio.h>

#define PRINT_POS(msg, c) printf("%s: %.6f, %.6f, %.6f\n", msg, c[0], c[1], c[2]);

CAM* CAM::s_instance = 0;

CAM::CAM() {
  cameraPos = glm::vec3(0.0f, 0.0f, 1.0f);
  cameraTarget = glm::vec3(0.0f, 0.0f, 0.0f);
  cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
}

CAM* CAM::instance() {
  if (!s_instance) s_instance = new CAM();
  return s_instance;
}
void CAM::setCamaraAt(glm::vec3 pos) {
  cameraPos = (pos);
  // cameraPos = pos;
  PRINT_POS("Camera at", cameraPos);
}

void CAM::setCamaraLookAt(glm::vec3 target) {
  cameraTarget = (target);
}

void CAM::setCamaraUpAxis(glm::vec3 yAxis) {
  cameraUp = glm::normalize(yAxis);
}

glm::vec3 CAM::getCameraAt() { return cameraPos; }
glm::vec3 CAM::getCameraTarget() { return cameraTarget; }
glm::vec3 CAM::getCameraUpAxis() { return cameraUp; }

glm::mat4 CAM::getViewMatrix() {
  return glm::lookAt(cameraPos, cameraTarget, cameraUp);
}

void  CAM::moveUp(float by) {
  setCamaraAt(getCameraAt() + glm::vec3(0, by, 0));
}
void  CAM::moveDown(float by) {
  setCamaraAt(getCameraAt() - glm::vec3(0, by, 0));
}
void  CAM::moveLeft(float by) {
  setCamaraAt(getCameraAt() - glm::vec3(by, 0, 0));
}
void  CAM::moveRight(float by) {
  setCamaraAt(getCameraAt() + glm::vec3(by, 0, 0));
}
void  CAM::moveInto(float by) {
  setCamaraAt(getCameraAt() - glm::vec3(0, 0, by));
}
void  CAM::moveBack(float by) {
  setCamaraAt(getCameraAt() + glm::vec3(0, 0, by));
}

void CAM::handleKeyboard(int key) {
  float speed = 0.15f;
  switch (key) {
    case GLFW_KEY_W:
    {
      moveInto(speed);
    }
    break;
    case GLFW_KEY_UP:
    {
      moveUp(speed);
    }
    break;
    case GLFW_KEY_S:
    {
      moveBack(speed);
    }
    break;
    case GLFW_KEY_DOWN:
    {
      moveDown(speed);
    }
    break;
    case GLFW_KEY_A:
    case GLFW_KEY_LEFT:
    {
      moveLeft(speed);
    }
    break;
    case GLFW_KEY_D:
    case GLFW_KEY_RIGHT:
    {
      moveRight(speed);
    }
    break;
    default:
    {
    }
  }
}
