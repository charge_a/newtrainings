#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class CAM {
private:
  glm::vec3 cameraPos;
  glm::vec3 cameraTarget;
  glm::vec3 cameraRight;
  glm::vec3 cameraUp;
  static CAM* s_instance;
  CAM();

public:
  void  setCamaraAt (glm::vec3 pos);
  void  setCamaraLookAt (glm::vec3 target);
  void  setCamaraUpAxis (glm::vec3 yAxis);

  void  moveUp(float by);
  void  moveDown(float by);
  void  moveLeft(float by);
  void  moveRight(float by);
  void  moveInto(float by);
  void  moveBack(float by);

  glm::vec3 getCameraAt();
  glm::vec3 getCameraTarget();
  glm::vec3 getCameraUpAxis();

  glm::mat4 getViewMatrix();

  void handleKeyboard(int key);

  static CAM* instance();
};
