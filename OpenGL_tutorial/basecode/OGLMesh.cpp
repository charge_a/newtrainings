#include "OGLMesh.h"
#include <SOIL.h>
#include <map>
#include <iostream>

std::map<std::string, GLuint> loadedTextures;
GLuint OGLTexture::LoadTextureFromFile(const char *filename) {
  if (loadedTextures.find(std::string(filename)) != loadedTextures.end()) {
    return loadedTextures[std::string(filename)];
  }
  GLuint id;
  glGenTextures(1, &id);
  glBindTexture(GL_TEXTURE_2D, id);
  // texture loading
  int w, h;
  unsigned char* pixels = SOIL_load_image(filename, &w, &h, 0, SOIL_LOAD_RGB);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, pixels);
  glGenerateMipmap(GL_TEXTURE_2D);
  SOIL_free_image_data(pixels);
  // texture setings
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // repeat beyound s=(0,1)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // repeat beyound t=(0,1)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); // down sampling scheme
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // up sampling scheme

  glBindTexture(GL_TEXTURE_2D, 0);
  loadedTextures[std::string(filename)] = id;
  return id;
}

OGLMesh::OGLMesh (std::vector<OGLVertex> vertices, std::vector<GLuint> indices, std::vector<OGLTexture> textures)
{
  m_vertices = vertices;
  m_indices  = indices;
  m_textures = textures;
  setup();
}

void OGLMesh::setup() {
  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);

  glBindVertexArray(vao);
  {
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, m_vertices.size()*sizeof(OGLVertex), &m_vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size()*sizeof(GLuint), &m_indices[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(OGLVertex), (GLvoid*)(0));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(OGLVertex), (GLvoid*)offsetof(OGLVertex, normal));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(OGLVertex), (GLvoid*)offsetof(OGLVertex, texCoords));
  }
  glBindVertexArray(0);
}

void OGLMesh::Draw(OGLShader &shader) {
  std::string name_diffuse_map("material.texture_diffuse");
  std::string name_specular_map("material.texture_specular");
  GLuint nDiffuseMaps = 1;
  GLuint nSpecularMaps = 1;

  for (size_t i = 0; i < m_textures.size(); i++) {
    glActiveTexture(GL_TEXTURE0 + i);
    std::string uniformName;
    if (m_textures[i].type == OGLTextureTypeDiffuse) {
      uniformName = name_diffuse_map + std::to_string(nDiffuseMaps);
      nDiffuseMaps++;
    }
    else {
      uniformName = name_specular_map + std::to_string(nSpecularMaps);
      nSpecularMaps++;
    }
    GLint loc = glGetUniformLocation(shader.pu, uniformName.c_str());
    if (loc == -1) {
      // std::cerr << "Error getting uniform location: " << uniformName << '\n';
    }
    glUniform1i(loc, i);
    glBindTexture(GL_TEXTURE_2D, m_textures[i].texId);
  }

  // Draw
  glBindVertexArray(vao);
  glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);

  // Detach textures
  for (size_t i = 0; i < m_textures.size(); i++) {
    glActiveTexture(GL_TEXTURE0 + i);
    glBindTexture(GL_TEXTURE0 + i, 0);
  }
}
