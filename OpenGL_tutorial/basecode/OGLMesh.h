#pragma once
#include <GL/glew.h>
#include <vector>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "OGLShader.h"

struct OGLVertex {
  glm::vec3 position;
  glm::vec3 normal;
  glm::vec2 texCoords;
};

enum OGLTextureType {
  OGLTextureTypeDiffuse,
  OGLTextureTypeSpecular
};

struct OGLTexture {
  GLuint texId;
  OGLTextureType type;
  GLuint LoadTextureFromFile(const char* filename);
  OGLTexture() {}
  OGLTexture(const char* filename) {
    texId = LoadTextureFromFile(filename);
  }
  OGLTexture(const char* filename, OGLTextureType type2) {
    texId = LoadTextureFromFile(filename);
    type = type2;
  }
};

class OGLMesh {
private:
  GLuint vao, vbo, ebo;
  void setup();

public:
  OGLMesh (std::vector<OGLVertex> vertices, std::vector<GLuint> indices, std::vector<OGLTexture> textures);
  void Draw(OGLShader &shader);

  std::vector<OGLVertex> m_vertices;
  std::vector<GLuint> m_indices;
  std::vector<OGLTexture> m_textures;
};
