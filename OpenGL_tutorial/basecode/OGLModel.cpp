#include "OGLModel.h"
#include <iostream>
#include <stdio.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

OGLModel::OGLModel (const char* modelPath)
{
  loadModel(modelPath);
}

void OGLModel::Draw(OGLShader &shader) {
  for (size_t i = 0; i < m_meshes.size(); i++) {
    m_meshes[i].Draw(shader);
  }
}

void OGLModel::loadModel(std::string modelPath) {
  Assimp::Importer importer;
  const aiScene *scene = importer.ReadFile(modelPath, aiProcess_Triangulate | aiProcess_FlipUVs);

  if (!scene || (scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE) || !scene->mRootNode) {
    std::cerr << "ERROR::ASSIMP::" << importer.GetErrorString() << '\n';
    return;
  }

  directory = modelPath.substr(0, modelPath.find_last_of('/'));
  processNode(scene->mRootNode, scene);
}

void OGLModel::processNode(aiNode *node, const aiScene *scene) {
  printf("(%s)$ meshes=%d, children=%d\n", node->mName.C_Str(), node->mNumMeshes, node->mNumChildren);
  for (size_t i = 0; i < node->mNumMeshes; i++) {
    aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
    m_meshes.push_back(processMesh(mesh, scene));
  }
  for (size_t i = 0; i < node->mNumChildren; i++) {
    processNode(node->mChildren[i], scene);
  }
}

OGLMesh OGLModel::processMesh(aiMesh *mesh, const aiScene *scene)
{
  std::vector<OGLVertex> vertices;
  std::vector<GLuint> indices;
  std::vector<OGLTexture> textures;

  printf("\tmesh - #vertices: %d, #faces: %d\n", mesh->mNumVertices, mesh->mNumFaces);
  // 1
  for (size_t i = 0; i < mesh->mNumVertices; i++) {
    OGLVertex vertex;
    // Convert aiMesh's vertex to custom vertex
    vertex.position = glm::vec3(mesh->mVertices[i].x,
                                mesh->mVertices[i].y,
                                mesh->mVertices[i].z);
    vertex.normal   = glm::vec3(mesh->mNormals[i].x,
                                mesh->mNormals[i].y,
                                mesh->mNormals[i].z);
    if (mesh->mTextureCoords[0]) {
      vertex.texCoords = glm::vec2(mesh->mTextureCoords[0][i].x,
                                   mesh->mTextureCoords[0][i].y);
    }
    else {
      vertex.texCoords = glm::vec2(0, 0);
    }
    vertices.push_back(vertex);
  }

  // 2
  for (size_t i = 0; i < mesh->mNumFaces; i++) {
    aiFace face = mesh->mFaces[i];
    for (size_t j = 0; j < face.mNumIndices; j++) {
      indices.push_back(face.mIndices[j]); // due to aiProcess_Triangulate, faces are all triangles
    }
  }

  // 3
  if (mesh->mMaterialIndex >= 0) {
    aiMaterial *mat = scene->mMaterials[mesh->mMaterialIndex];
    std::vector<OGLTexture> diffuseMaps = loadMaterialTextures(mat, aiTextureType_DIFFUSE, OGLTextureTypeDiffuse);
    std::vector<OGLTexture> specularMaps = loadMaterialTextures(mat, aiTextureType_SPECULAR, OGLTextureTypeSpecular);
    textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
    textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
  }

  return OGLMesh(vertices, indices, textures);
}

std::vector<OGLTexture> OGLModel::loadMaterialTextures(aiMaterial *mat, aiTextureType type, OGLTextureType type2)
{
  std::vector<OGLTexture> textures;
  for (size_t i = 0; i < mat->GetTextureCount(type); i++) {
    aiString str;
    mat->GetTexture(type, i, &str);
    OGLTexture t;
    t.texId = t.LoadTextureFromFile((directory + "/" + std::string(str.C_Str())).c_str());
    t.type = type2;
    textures.push_back(t);
  }
  return textures;
}
