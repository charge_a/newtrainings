#pragma once
#include <assimp/mesh.h>
#include <assimp/scene.h>
#include <assimp/material.h>

#include "OGLMesh.h"

class OGLModel {
private:
  std::vector<OGLMesh> m_meshes;
  std::string directory;

  void                    loadModel(std::string modelPath);
  void                    processNode(aiNode *node, const aiScene *scene);
  OGLMesh                 processMesh(aiMesh *mesh, const aiScene *scene);
  std::vector<OGLTexture> loadMaterialTextures(aiMaterial *mat, aiTextureType type, OGLTextureType type2);

public:
  OGLModel (const char* modelPath);
  void Draw (OGLShader &shader);
};
