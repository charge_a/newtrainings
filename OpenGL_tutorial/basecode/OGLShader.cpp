#include "OGLShader.h"
#include "../GLUtils.h"

OGLShader::OGLShader(const GLchar* vertexShader, const GLchar* fragmentShader)
{
  pu = LoadProgram(vertexShader, fragmentShader);
}
OGLShader::OGLShader(const GLchar* vertexShader, const GLchar* geometryShader, const GLchar* fragmentShader)
{
  pu = LoadProgram(vertexShader, geometryShader, fragmentShader);
}
OGLShader::OGLShader (const OGLShader &shader)
{
  pu = shader.pu;
}

void OGLShader::Use() {
  glUseProgram(pu);
}
