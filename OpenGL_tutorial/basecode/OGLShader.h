#pragma once
#include <GL/glew.h>
class OGLShader {
private:


public:
  OGLShader (const GLchar* vertexShader, const GLchar* fragmentShader);
  OGLShader (const GLchar* vertexShader, const GLchar* geometryShader, const GLchar* fragmentShader);
  OGLShader (const OGLShader &shader);
  GLuint pu;
  void Use();
};
