#ifndef VERTEX_DATA_H
#define VERTEX_DATA_H
#include <GL/glew.h>
#include <iostream>

namespace VDProvider {
  GLfloat* getCubePositionData(int *size);
  GLfloat* getCubeNormalData(int *size);
  GLfloat* getCubeTextureData(int *size);  
} /* VDProvider */

#endif
