#version 300 es

struct Material
{
  sampler2D diffuse;
  sampler2D specular;
  float     shininess;
};
struct Light
{
  vec3 ambient;
  vec3 diffuse;
  vec3 specular;
  vec3 position;
};

in highp vec3 Normal;
in highp vec3 FragPosWorld;
in highp vec2 TexCoord;
out highp vec4 color;

uniform highp vec3 objColor;
uniform highp vec3 cameraPos;

uniform Material material;
uniform Light light;

void main()
{
  // Ambient light
  highp vec3 ambient = vec3(texture(material.diffuse, TexCoord)) * light.ambient;

  // Diffuse light
  highp vec3 norm = normalize(Normal);
  highp vec3 lightDirection = normalize(light.position - FragPosWorld);
  highp float diff = max(0.0f, dot(norm, lightDirection)); // if angle is > 90, dot is -ve.
  highp vec3 diffuse = (diff * vec3(texture(material.diffuse, TexCoord))) * light.diffuse;

  // Specular light
  highp vec3 viewDirection = normalize(cameraPos - FragPosWorld);
  highp vec3 reflectDir = reflect(-lightDirection, norm);
  highp float diff2 = max(0.0f, dot(viewDirection, reflectDir));
  highp float spec = pow(diff2, material.shininess);
  highp vec3 specular = vec3(texture(material.specular, TexCoord)) * spec * light.specular;

  // color = vec4(objColor * (ambient + diffuse + specular), 1.0);
  color = vec4((ambient + diffuse + specular), 1.0);
  // color = vec4(norm, 1);
}
