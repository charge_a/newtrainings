#version 300 es

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;

uniform mat4 m, v, p;

out vec3 Normal;
out vec3 FragPosWorld;
out vec2 TexCoord;

void main()
{
    gl_Position = p * v * m * vec4(position, 1.0);
    Normal = mat3(transpose(inverse(m))) * normal;
    FragPosWorld = vec3(m * vec4(position, 1.0));
    TexCoord = texCoords;
}
