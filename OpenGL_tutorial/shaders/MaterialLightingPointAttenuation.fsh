#version 300 es

struct Material
{
  sampler2D diffuse;
  sampler2D specular;
  float     shininess;
};
struct Light
{
  vec3 ambient;
  vec3 diffuse;
  vec3 specular;
  vec3 position;

  float constant;
  float linear;
  float quadratic;

  vec3 spotLightPosition;
  vec3 spotLightTowards;
  float spotLightCutoffAngle;
  float spotLightCutoffOuterAngle;
};

in highp vec3 Normal;
in highp vec3 FragPosWorld;
in highp vec2 TexCoord;
out highp vec4 color;

uniform highp vec3 objColor;
uniform highp vec3 cameraPos;

uniform Material material;
uniform Light light;

void main()
{
  // Ambient light
  highp vec3 ambient = vec3(texture(material.diffuse, TexCoord)) * light.ambient;

  // Diffuse light
  highp vec3 norm = normalize(Normal);
  highp vec3 lightDirection = normalize(light.position - FragPosWorld);
  highp float diff = max(0.0f, dot(norm, lightDirection)); // if angle is > 90, dot is -ve.
  highp vec3 diffuse = (diff * vec3(texture(material.diffuse, TexCoord))) * light.diffuse;

  // Specular light
  highp vec3 viewDirection = normalize(cameraPos - FragPosWorld);
  highp vec3 reflectDir = reflect(-lightDirection, norm);
  highp float diff2 = max(0.0f, dot(viewDirection, reflectDir));
  highp float spec = pow(diff2, material.shininess);
  highp vec3 specular = vec3(texture(material.specular, TexCoord)) * spec * light.specular;

  // attenuation is how light's intensity reduces from source with distance.
  highp float fragDist = length(light.position - FragPosWorld);
  highp float attenuation = 1.0f / (light.constant + light.linear*fragDist + light.quadratic * fragDist * fragDist);

  // Spot light calculation
  highp float theta = dot(normalize(light.spotLightPosition - FragPosWorld), normalize(light.spotLightPosition - light.spotLightTowards));
  highp float thetaCutoff = light.spotLightCutoffAngle;

  highp float epsilon = light.spotLightCutoffAngle - light.spotLightCutoffOuterAngle;
  highp float intensity = (theta - light.spotLightCutoffOuterAngle) / epsilon;
  intensity = clamp(intensity, 0.0f, 1.0f);

  // ambient *= attenuation * intensity; // leave areas in dark visible with ambient light
  diffuse *= attenuation * intensity;
  specular *= attenuation * intensity;
  color = vec4((ambient + diffuse + specular) * attenuation, 1.0);

}
