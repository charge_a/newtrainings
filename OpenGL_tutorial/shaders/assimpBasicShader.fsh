#version 300 es
in highp vec2 TexCoord;

out highp vec4 color;

struct Material
{
  sampler2D texture_diffuse1;
  sampler2D texture_specular1;
};

uniform Material material;

void main()
{
  color = vec4(texture (material.texture_diffuse1, TexCoord));
}
