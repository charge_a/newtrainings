#version 300 es
in highp vec3 Normal;
in highp vec3 FragPosWorld;
in highp vec2 TexCoord;

out highp vec4 color;

struct Material
{
  sampler2D texture_diffuse1;
  sampler2D texture_specular1;
  float shininess;
};

struct Light
{
  vec3 ambient, diffuse, specular;
  vec3 position;
  float constant;
  float linear;
  float quadratic;
};

uniform Material material;
uniform highp vec3 cameraPos;

#define NUM_POINT_LIGHTS 1
uniform Light pointLights[NUM_POINT_LIGHTS];

highp vec3 computePointLight (Light light, highp vec3 normal, highp vec3 viewDirection, highp vec3 FragPosWorld)
{
  // Ambient light
  highp vec3 ambient = vec3(texture(material.texture_diffuse1, TexCoord)) * light.ambient;
  // Diffuse light
  highp vec3 lightDirection = normalize(light.position - FragPosWorld);
  highp float diff = max(0.0f, dot(normal, lightDirection)); // if angle is > 90, dot is -ve.
  highp vec3 diffuse = (diff * vec3(texture(material.texture_diffuse1, TexCoord))) * light.diffuse;
  // Specular light
  highp vec3 reflectDir = reflect(-lightDirection, normal);
  highp float diff2 = max(0.0f, dot(viewDirection, reflectDir));
  highp float spec = pow(diff2, material.shininess);
  highp vec3 specular = vec3(texture(material.texture_specular1, TexCoord)) * spec * light.specular;
  // attenuation is how light's intensity reduces from source with distance.
  highp float fragDist = length(light.position - FragPosWorld);
  highp float attenuation = 1.0f / (light.constant + light.linear*fragDist + light.quadratic * fragDist * fragDist);

  // ambient *= attenuation;
  // diffuse *= attenuation;
  // specular *= attenuation;
  return ambient + diffuse + specular;
}

void main()
{
  highp vec3 norm = normalize(Normal);
  highp vec3 viewDirection = normalize(cameraPos - FragPosWorld);

  highp vec3 result = vec3(0,0,0);
  for(int i = 0; i < NUM_POINT_LIGHTS; i++)
  {
    result += computePointLight(pointLights[i], norm, viewDirection, FragPosWorld);
  }

  // color = vec4(texture (material.texture_diffuse1, TexCoord)) * vec4(result, 1.0);
  // color = vec4(texture (material.texture_diffuse1, TexCoord));
  color = vec4(result, 1.0);
}
