#version 300 es

in highp vec4 vcolor;
out highp vec4 color;

void main()
{
    color = vcolor;
}
