#version 300 es

layout (location = 0) in vec3 position;
uniform vec3 solidColor;
uniform mat4 m,v,p;
out vec4 vcolor;

void main()
{
    gl_Position = p*v*m*vec4(position.x, position.y, position.z, 1.0);
    vcolor = vec4(solidColor, 1.0);
}
