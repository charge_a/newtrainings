#version 300 es
in highp vec2 TexCoord;

out highp vec4 color;

uniform sampler2D imgTexture;

void main()
{
  color = vec4(texture (imgTexture, TexCoord));
}
