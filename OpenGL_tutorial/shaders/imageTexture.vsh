#version 300 es
layout (location=0) in vec3 position;
layout (location=1) in vec2 texCoord;

out vec2 TexCoord;

uniform mat4 m, v, p;

void main()
{
  gl_Position = p*v*m*vec4(position.x, -position.y, position.z, 1.0f);
  TexCoord = texCoord;
}
