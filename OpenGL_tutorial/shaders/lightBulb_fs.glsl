#version 300 es

out highp vec4 color;
uniform highp vec3 objColor;

void main()
{
    color = vec4(objColor, 1);
}
