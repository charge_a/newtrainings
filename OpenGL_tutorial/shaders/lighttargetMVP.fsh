#version 300 es

in highp vec3 Normal;
in highp vec3 FragPosWorld;
out highp vec4 color;

uniform highp vec3 objColor, lightColor;
uniform highp vec3 lightSource;
uniform highp vec3 cameraPos;

void main()
{
  // Ambient light
  highp float ambientStrength = 0.1f;
  highp vec3 ambient = ambientStrength * lightColor;

  // Diffuse light
  highp vec3 norm = normalize(Normal);
  highp vec3 lightDirection = normalize(lightSource - FragPosWorld);
  highp float diff = max(0.0f, dot(norm, lightDirection)); // if angle is > 90, dot is -ve.
  highp vec3 diffuse = diff * lightColor;

  // Specular light
  highp float specularStrength = 0.5f;
  highp vec3 viewDirection = normalize(cameraPos - FragPosWorld);
  highp vec3 reflectDir = reflect(-lightDirection, norm);
  highp float diff2 = max(0.0f, dot(viewDirection, reflectDir));
  highp float shininess = 256.0f;
  highp float spec = pow(diff2, shininess);
  highp vec3 specular = specularStrength * spec * lightColor;

  color = vec4(objColor * (ambient + diffuse + specular), 1.0);
}
