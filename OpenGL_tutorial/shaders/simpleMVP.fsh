#version 300 es

in highp vec4 vcolor;
in highp vec2 TexCoord;

out highp vec4 color;
uniform sampler2D imgTexture1;

void main()
{
  color = texture(imgTexture1, TexCoord);
}
