#version 300 es

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texture;
out vec4 vcolor;
out vec2 TexCoord;

uniform mat4 m, v, p;

void main()
{
    gl_Position = p * v * m * vec4(position.x, -position.y, position.z, 1.0);
    vcolor = vec4(0.5f, 0.0f, 0.0f, 1.0f);
    //vcolor = vec4(color, 1.0f);
    TexCoord = texture;
}
