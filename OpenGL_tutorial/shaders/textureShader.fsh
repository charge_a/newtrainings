#version 300 es
in highp vec3 vcolor;
in highp vec2 TexCoord;

out highp vec4 color;

uniform sampler2D imgTexture;

void main()
{
  color = texture (imgTexture, TexCoord) * vec4(vcolor, 1.0f);
}
