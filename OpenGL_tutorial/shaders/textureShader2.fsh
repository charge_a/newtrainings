#version 300 es
in highp vec3 vcolor;
in highp vec2 TexCoord;

out highp vec4 color;

uniform sampler2D imgTexture1;
uniform sampler2D imgTexture2;

void main()
{
//  color = vec4(vcolor, 1.0);
  color = mix( texture (imgTexture1, TexCoord), texture (imgTexture2, TexCoord), 0.5);
}
