#include "Triangle.h"
#include "TriangleTexture.h"
#include "basecode/BaseGLRender.h"
#include "basecode/Camera.h"
#include <GLFW/glfw3.h>
#include <assert.h>
#include "GLUtils.h"
#include <iostream>
#include "v2/BaseObject.h"
#include "v2/wooden/WoodenPlank.h"
#include "v3/UGLImpl.h"

#include "v3/Text/GUI/Main.h"

BaseObject *cube, *bulb;
// BaseScene *scene;
UGL::UGLBaseRenderer *scene;
UGL::UGLBaseRenderer *axis;
BaseObject *image;
UI::UIRenderer *ui;
void init()
{
  glClearColor(0.1, 0.1, 0.1, 1.0);
  glViewport(0, 0, getScreenWidth(), getScreenHeight());
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_STENCIL_TEST);
  glEnable(GL_MULTISAMPLE);

  // t = new TriangleSH();
  // t = new SquareTx();
  // t = new SquareMVP();
  // t = new Cube();
  // t = new CubeOfSq();
  // t = new LightingTarget();
  // bulb = new LightBulb();
  // pivot = new Pivot();
  // cube = new CubeA();
  // bulb = new BulbA();
  //
  // cube->setupMain();
  // bulb->setupMain();

  // scene = new BuildingScene();
  // scene = new CubeManiaScene();
  // scene = new MultiLightScene();
  // scene = new NanosuitScene();
  // scene = new WoodenScene();
  // scene = new WoodenOutlineScene();
  // scene = new UGL::UGLSample();
  // scene = new UGL::UGLOutline();
  // scene = new UGL::UGLMirror();
  // scene = new UGL::UGLMirror2();
  // scene = new UGL::UGLBlendingScene();
  // scene = new UGL::UGLOffScreen(new UGL::UGLSkyBoxEnvironment);
  // scene = new UGL::UGLSkyBox();
  // scene = new UGL::UGLSkyBoxEnvironment();
  // scene = new UGL::UGLGeometryExample();
  // scene = new UGL::UGLInstancingExample();
  // scene = new UGL::UGLShadowScene();
  // scene = new UGL::UGLMultipassRender();
  // scene = new UGL::UGLBillBoard();
  scene = new UGL::UGLVideoScene();
  // scene = new UGL::UGLText();
  scene->setup();

  axis = new UGL::UGLAxis();
  axis->setup();

  ui = new UI::UIRenderer();
  ui->setup();

  // image = new PlaneTexture();
  // image = new WoodenPlank();
  // image->setupMain();
}

/* display function - code from:
     http://fly.cc.fer.hr/~unreal/theredbook/chapter01.html
This is the actual usage of the OpenGL library.
The following code is the same for any platform */
void renderFunction()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // bulb->renderMain();
    // ((CubeA*)cube)->lightPos = ((BulbA*)bulb)->lightPos;
    // cube->renderMain();

    axis->render();
    // scene->render();
    ui->render();
    // image->renderMain();

    UGL::UGLCounter::get().update();

    glFlush();
}
void keyboardFunc(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    // When a user presses the escape key, we set the WindowShouldClose property to true,
    // closing the application
    if(key == GLFW_KEY_Q && action == GLFW_PRESS)
    	glfwSetWindowShouldClose(window, GL_TRUE);

    if (action != GLFW_RELEASE) {
      CAM::instance()->handleKeyboard(key);
      scene->handleKeyboard(key, action);
      ui->handleKeyboard(key, action);
    }

}
//
// void keyboardFunc(unsigned char key, int x, int y) {
//   switch (key) {
//     case 'q':
//     case 27:
//     {
//       exit(0);
//     }
//     break;
//   }
// }
//
// void renderIdle() {
//   renderFunction();
// }
// void renderTimer(int value) {
//   if (value > 0) {
//     glutTimerFunc(value, renderTimer, value);
//   }
//   renderFunction();
//   glutPostRedisplay();
// }

void windowResize(GLFWwindow *window, int w, int h) {
  glViewport(0, 0, w, h);
}

void cursorFunc(GLFWwindow* window, double xpos, double ypos)
{
  double x = xpos / getScreenWidth();
  double y = 1.0 - (ypos / getScreenHeight());
  ui->handleMouseMove(x, y);
}

void mouseClickFunc(GLFWwindow* window, int button, int action, int mods)
{
  if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);
    double x = xpos / getScreenWidth();
    double y = 1.0 - (ypos / getScreenHeight());
    ui->handleMouseClick(x, y);
  }
}

/* Main method - main entry point of application
the freeglut library does the window creation work for us,
regardless of the platform. */
int main(int argc, char** argv)
{
    // glutInit(&argc, argv);
    // glutInitDisplayMode(GLUT_SINGLE | GLUT_DEPTH);
    // glutInitWindowSize(500,500);
    // glutInitWindowPosition(100,100);
    // glutCreateWindow("OpenGL - First window demo");
    //
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
    glfwWindowHint(GLFW_SAMPLES, 4);
    GLFWwindow* window = glfwCreateWindow(600, 600, "LearnOpenGL", NULL, NULL);
    if (!window)
    {
        std::cerr << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    GLGlobalInstances::instance().setWindow(window);
    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, keyboardFunc);
    glfwSetWindowSizeCallback(window, windowResize);
    glfwSetCursorPosCallback(window, cursorFunc);
    glfwSetMouseButtonCallback(window, mouseClickFunc);

    glewExperimental = GL_TRUE;
    glewInit();
    init();

    while(!glfwWindowShouldClose(window))
    {
        glfwPollEvents();
        renderFunction();
        glfwSwapBuffers(window);
    }
    // glutKeyboardFunc(keyboardFunc);
    // glutDisplayFunc(renderFunction);
    // glutIdleFunc(renderIdle);
    // glutTimerFunc(0, renderTimer, 30);
    // glutMainLoop();
    glfwTerminate();
    return 0;
}
