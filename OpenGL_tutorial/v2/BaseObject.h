#ifndef BASE_OBJECT
#define BASE_OBJECT

#include "../basecode/BaseGLRender.h"
#include "../basecode/VertexData.h"
#include "../basecode/Camera.h"
#include <vector>
#include "../basecode/OGLModel.h"

#define UNI3(x) x, x, x

// Base classes
class BaseObject : public BaseGLRender {
private:
public:
  BaseObject (){}
  virtual void setup() = 0;
  void setupMain();
  void renderMain();
};

class BaseScene {
private:
public:
  BaseScene (){};

  virtual void setup() = 0;
  virtual void render() = 0;

  virtual void handleKeyboard(int key, int action) {}
};

// Derived Objects
class CubeA : public BaseObject {
public:
  CubeA() {}
  virtual void setup();
  virtual void render();
  void setupSharedVertices();
protected:
  void setupVertices();

public:
  glm::vec3 objPos;
  glm::vec3 objRotation;
  float objRotationAngle;
  glm::vec3 objColor;
  glm::vec3 lightColor;
  glm::vec3 lightPos;
  float size;
};
class CubeB : public CubeA {
public:
  CubeB() {}
  virtual void setup();
  virtual void render();

};
class CubeC : public CubeA {
public:
  CubeC() {}
  virtual void setup();
  virtual void render();

};
class CubeD : public CubeA {
public:
  CubeD() {}
  virtual void setup();
  virtual void render();

};

class BulbA: public CubeA {
  float angle;
public:
  BulbA(){}
  virtual void setup();
  virtual void render();

};
class BulbB: public CubeA {
  float angle;
public:
  BulbB(){}
  virtual void setup();
  virtual void render();

};
class CubeE : public CubeA {
public:
  CubeE() {}
  virtual void setup();
  virtual void render();
  virtual void renderParamsLightSource(int i, BulbB* bulb);

};

class PlaneTexture : public BaseObject {
private:


public:
  PlaneTexture () {};
  virtual void setup();
  virtual void render();
};

// Derived Scenes
class BuildingScene : public BaseScene {
  int n;
  CubeA *building;
  BulbA *bulb;
public:
  BuildingScene(){}
  virtual void setup();
  virtual void render();
};
class CubeManiaScene : public BaseScene {
  std::vector<glm::vec3> position;
  std::vector<float> rotation;
  CubeA *cube;
  BulbA *bulb;
  float angle;
public:
  CubeManiaScene(){}
  virtual void setup();
  virtual void render();
};
class MultiLightScene : public BaseScene {
  std::vector<glm::vec3> cube_pos;
  CubeE *cube;
  BulbB *bulb;
  float angle;
  int nBulb;
public:
  MultiLightScene(){}
  virtual void setup();
  virtual void render();
};

class NanosuitScene : public BaseScene {
private:
  OGLModel *model;
  OGLShader *shader;
public:
  NanosuitScene (){};
  virtual void setup();
  virtual void render();
};
#endif
