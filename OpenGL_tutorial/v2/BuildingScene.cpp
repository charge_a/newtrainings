#include "BaseObject.h"

void BuildingScene::setup() {
  // building = new CubeA();
  // building = new CubeB();
  building = new CubeC();
  building->setupMain();
  bulb = new BulbA();
  bulb->setup();
  n = 4;
  // glEnable(GL_CULL_FACE);
}

void BuildingScene::render() {
  bulb->renderMain();
  float z = 0;
  for (size_t i = 0; i < n; i++) {
    building->lightPos = bulb->objPos;
    building->size = 0.25;
    if (i%2 == 0) {
      building->objPos = glm::vec3(-0.25, 0, z);
    }
    else {
      building->objPos = glm::vec3(0.25, 0, z);
    }
    // building->objPos = glm::vec3(0, 0, 0);
    building->renderMain();
    z -= 0.35;
  }
}
