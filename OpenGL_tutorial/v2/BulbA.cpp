#include "BaseObject.h"

void BulbA::setup() {
  CubeA::setupVertices();
  addShaders("shaders/lightBulb_vs.glsl", "shaders/lightBulb_fs.glsl");

  size = 0.05;
  objColor = lightColor;
  objRotation = glm::vec3(0.0, 1.0, 0.0);
  objRotationAngle = 0;
  angle = 0;
}

void BulbA::render() {
  angle += 0.95;
  if (angle > 360) angle -= 360;
  float t = glm::radians(angle);
  glm::vec3 lightSourcePos(sin(t), cos(t), 0);
  lightPos = lightSourcePos * 1.3f;

  objPos = lightPos;
  CubeA::render();
}
