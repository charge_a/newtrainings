#include "BaseObject.h"

void BulbB::setup() {
  CubeA::setupVertices();
  addShaders("shaders/lightBulb_vs.glsl", "shaders/lightBulb_fs.glsl");

  size = 0.05;
  objColor = glm::vec3(1, 1, 1);
  objRotation = glm::vec3(0.0, 1.0, 0.0);
  objRotationAngle = 0;
  angle = 0;
}

void BulbB::render() {
  CubeA::render();
}
