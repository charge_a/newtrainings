#include "BaseObject.h"
#include <map>

std::map<int, GLuint> sharedVBO;

void CubeA::setupVertices() {
  int vbo0 = sharedVBO[0];
  int vbo1 = sharedVBO[1];
  if (vbo0 && vbo1){
    addVertexAttributesVBO(0, 3, vbo0);
    addVertexAttributesVBO(1, 3, vbo1);
  }
  else {
    int size;
    GLfloat* v_data = VDProvider::getCubePositionData(&size);
    addVertexAttributes(0, v_data, size, 3);
    sharedVBO[0] = vbo;
    GLfloat* n_data = VDProvider::getCubeNormalData(&size);
    addVertexAttributes(1, n_data, size, 3);
    sharedVBO[1] = vbo;
  }
}
void CubeA::setup()
{
  setupVertices();
  addShaders("shaders/lighttargetMVP.vsh", "shaders/lighttargetMVP.fsh");

  size = 0.5;
  objColor = glm::vec3(1.0, 0.5, 0.31);
  objRotation = glm::vec3(0.0, 1.0, 0.0);
  objRotationAngle = 0;
  objPos = glm::vec3(0,0,0);
  lightColor = glm::vec3(1.0, 1.0, 1.0);
}

void CubeA::render() {
  // float size = 0.5;
  m = glm::translate(m, objPos);
  m = glm::rotate(m, objRotationAngle, objRotation);
  m = glm::scale(m, glm::vec3(size, size, size));
  glm::vec3 cam = CAM::instance()->getCameraAt();

  glUniform3f(getUniformLocation("objColor"), objColor.x, objColor.y, objColor.z);
  glUniform3f(getUniformLocation("lightColor"), lightColor.x, lightColor.y, lightColor.z);
  glUniform3f(getUniformLocation("lightSource"), lightPos.x, lightPos.y, lightPos.z);
  glUniform3f(getUniformLocation("cameraPos"), cam.x, cam.y, cam.z);
  BaseGLRender::render(GL_TRIANGLES, 36);
}
