#include "BaseObject.h"
#include <map>

void CubeB::setup()
{
  setupVertices();
  addShaders("shaders/MaterialLighting.vsh", "shaders/MaterialLighting.fsh");

  size = 0.5;
  objColor = glm::vec3(1.0, 0.5, 0.31);
  objPos = glm::vec3(0,0,0);
  lightColor = glm::vec3(1.0, 1.0, 1.0);
}

void CubeB::render() {
  // float size = 0.5;
  m = glm::translate(m, objPos);
  m = glm::scale(m, glm::vec3(size, size, size));
  glm::vec3 cam = CAM::instance()->getCameraAt();

  glUniform3f(getUniformLocation("objColor"), objColor.x, objColor.y, objColor.z);
  glUniform3f(getUniformLocation("cameraPos"), cam.x, cam.y, cam.z);

  glUniform3f(getUniformLocation("material.ambient"),   1.0f, 0.5f, 0.31f);
  glUniform3f(getUniformLocation("material.diffuse"),   1.0f, 0.5f, 0.31f);
  glUniform3f(getUniformLocation("material.specular"),  0.5f, 0.5f, 0.5f);
  glUniform1f(getUniformLocation("material.shininess"), 32.0f);

  glUniform3f(getUniformLocation("light.ambient"),   0.2f, 0.2f, 0.2f);
  glUniform3f(getUniformLocation("light.diffuse"),   0.5f, 0.5f, 0.5f);
  glUniform3f(getUniformLocation("light.specular"),  1.0f, 1.0f, 1.0f);
  glUniform3f(getUniformLocation("light.position"),  lightPos.x, lightPos.y, lightPos.z);
  BaseGLRender::render(GL_TRIANGLES, 36);
}
