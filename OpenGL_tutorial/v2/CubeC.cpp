#include "BaseObject.h"
#include <map>

void CubeC::setup()
{
  setupVertices();
  {
    int size;
    GLfloat* t_data = VDProvider::getCubeTextureData(&size);
    addVertexAttributes(2, t_data, size, 2);
  }

  addShaders("shaders/MaterialLightingMaps.vsh", "shaders/MaterialLightingMaps.fsh");
  addTextureUnit(GL_TEXTURE0, "texture/container2.png");
  addTextureUnit(GL_TEXTURE1, "texture/container2_specular.png");

  size = 0.5;
  objColor = glm::vec3(1.0, 0.5, 0.31);
  objRotation = glm::vec3(0.0, 1.0, 0.0);
  objRotationAngle = 0;
  objPos = glm::vec3(0,0,0);
  lightColor = glm::vec3(1.0, 1.0, 1.0);
}

void CubeC::render() {
  // float size = 0.5;
  m = glm::translate(m, objPos);
  m = glm::rotate(m, objRotationAngle, objRotation);
  m = glm::scale(m, glm::vec3(size, size, size));
  glm::vec3 cam = CAM::instance()->getCameraAt();

  glUniform3f(getUniformLocation("objColor"), objColor.x, objColor.y, objColor.z);
  glUniform3f(getUniformLocation("cameraPos"), cam.x, cam.y, cam.z);

  glUniform3f(getUniformLocation("material.specular"),  0.5f, 0.5f, 0.5f);
  glUniform1f(getUniformLocation("material.shininess"), 32.0f);
  renderTextureUnit(GL_TEXTURE0, "material.diffuse");
  renderTextureUnit(GL_TEXTURE1, "material.specular");

  float ld = 0.8f;
  glUniform3f(getUniformLocation("light.ambient"),   ld, ld, ld);
  glUniform3f(getUniformLocation("light.diffuse"),   ld, ld, ld);
  glUniform3f(getUniformLocation("light.specular"),  1.0f, 1.0f, 1.0f);
  glUniform3f(getUniformLocation("light.position"),  lightPos.x, lightPos.y, lightPos.z);

  BaseGLRender::render(GL_TRIANGLES, 36);
}
