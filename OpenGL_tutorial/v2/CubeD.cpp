#include "BaseObject.h"
#include <map>

void CubeD::setup()
{
  setupVertices();
  {
    int size;
    GLfloat* t_data = VDProvider::getCubeTextureData(&size);
    addVertexAttributes(2, t_data, size, 2);
  }

  // addShaders("shaders/MaterialLightingMaps.vsh", "shaders/MaterialLightingDirectional.fsh");
  addShaders("shaders/MaterialLightingMaps.vsh", "shaders/MaterialLightingPointAttenuation.fsh");
  addTextureUnit(GL_TEXTURE0, "texture/container2.png");
  addTextureUnit(GL_TEXTURE1, "texture/container2_specular.png");

  size = 1.0;
  objColor = glm::vec3(1.0, 0.5, 0.31);
  objRotation = glm::vec3(0.0, 1.0, 0.0);
  objRotationAngle = 0;
  objPos = glm::vec3(0,0,0);
  lightColor = glm::vec3(1.0, 1.0, 1.0);
}

void CubeD::render() {
  // float size = 0.5;
  m = glm::translate(m, objPos);
  m = glm::rotate(m, objRotationAngle, objRotation);
  m = glm::scale(m, glm::vec3(size, size, size));
  glm::vec3 cam = CAM::instance()->getCameraAt();
  glm::vec3 camTarget = CAM::instance()->getCameraTarget();

  glUniform3f(getUniformLocation("objColor"), objColor.x, objColor.y, objColor.z);
  glUniform3f(getUniformLocation("cameraPos"), cam.x, cam.y, cam.z);

  glUniform3f(getUniformLocation("material.specular"),  0.5f, 0.5f, 0.5f);
  glUniform1f(getUniformLocation("material.shininess"), 32.0f);
  renderTextureUnit(GL_TEXTURE0, "material.diffuse");
  renderTextureUnit(GL_TEXTURE1, "material.specular");

  glUniform3f(getUniformLocation("light.ambient"),   0.2f, 0.2f, 0.2f);
  glUniform3f(getUniformLocation("light.diffuse"),   0.5f, 0.5f, 0.5f);
  glUniform3f(getUniformLocation("light.specular"),  1.0f, 1.0f, 1.0f);
  // glUniform3f(getUniformLocation("light.direction"), -0.2f, -1.0f, -0.3f);

  // Point source attenuation params
  glUniform3f(getUniformLocation("light.position"),  lightPos.x, lightPos.y, lightPos.z);
  glUniform1f(getUniformLocation("light.constant"),  1.0f);
  glUniform1f(getUniformLocation("light.linear"),    0.09f);
  glUniform1f(getUniformLocation("light.quadratic"), 0.032f);

  // Spot Light params
  glUniform3f(getUniformLocation("light.spotLightPosition"), cam.x, cam.y, cam.z); // spot light on cameraPos
  glUniform3f(getUniformLocation("light.spotLightTowards"), camTarget.x, camTarget.y, camTarget.z);
  glUniform1f(getUniformLocation("light.spotLightCutoffAngle"), glm::cos(glm::radians(6.5f)));
  glUniform1f(getUniformLocation("light.spotLightCutoffOuterAngle"), glm::cos(glm::radians(12.5f)));

  BaseGLRender::render(GL_TRIANGLES, 36);
}
