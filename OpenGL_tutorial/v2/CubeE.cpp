#include "BaseObject.h"
#include <string>

void CubeE::setup()
{
  setupVertices();
  {
    int size;
    GLfloat* t_data = VDProvider::getCubeTextureData(&size);
    addVertexAttributes(2, t_data, size, 2);
  }

  // addShaders("shaders/MaterialLightingMaps.vsh", "shaders/MaterialLightingDirectional.fsh");
  addShaders("shaders/MaterialLightingMaps.vsh", "shaders/MaterialLightingMultipleSources.fsh");
  addTextureUnit(GL_TEXTURE0, "texture/container2.png");
  addTextureUnit(GL_TEXTURE1, "texture/container2_specular.png");

  size = 1.0;
  objColor = glm::vec3(1.0, 0.5, 0.31);
  objRotation = glm::vec3(0.0, 1.0, 0.0);
  objRotationAngle = 0;
  objPos = glm::vec3(0,0,0);
  lightColor = glm::vec3(1.0, 1.0, 1.0);
}

void CubeE::renderParamsLightSource(int i, BulbB *bulb) {
  renderBefore();
  std::string si = std::to_string(i);
  glm::vec3 a = 0.2f * bulb->objColor;
  glm::vec3 d = 0.2f * bulb->objColor;
  glm::vec3 s = 1.0f * bulb->objColor;
  glUniform3f(getUniformLocation(("pointLights["+si+"].position").c_str()),  bulb->objPos.x, bulb->objPos.y, bulb->objPos.z);
  glUniform1f(getUniformLocation(("pointLights["+si+"].constant").c_str()),  1.0f);
  glUniform1f(getUniformLocation(("pointLights["+si+"].linear").c_str()),    0.09f);
  glUniform1f(getUniformLocation(("pointLights["+si+"].quadratic").c_str()), 0.032f);
  glUniform3f(getUniformLocation(("pointLights["+si+"].ambient").c_str()),   a.x, a.y, a.z);
  glUniform3f(getUniformLocation(("pointLights["+si+"].diffuse").c_str()),   d.x ,d.y, d.z);
  glUniform3f(getUniformLocation(("pointLights["+si+"].specular").c_str()),  s.x, s.y, s.z);
  renderAfter();
}

void CubeE::render() {
  // float size = 0.5;
  m = glm::translate(m, objPos);
  m = glm::rotate(m, objRotationAngle, objRotation);
  m = glm::scale(m, glm::vec3(size, size, size));

  glm::vec3 cam = CAM::instance()->getCameraAt();
  glm::vec3 camTarget = CAM::instance()->getCameraTarget();

  glUniform3f(getUniformLocation("cameraPos"), cam.x, cam.y, cam.z);

  glUniform3f(getUniformLocation("dirLight.ambient"),   0.2f, 0.2f, 0.2f);
  glUniform3f(getUniformLocation("dirLight.diffuse"),   0.5f, 0.5f, 0.5f);
  glUniform3f(getUniformLocation("dirLight.specular"),  1.0f, 1.0f, 1.0f);
  glUniform3f(getUniformLocation("dirLight.direction"), -0.2f, -1.0f, -0.3f);

  glUniform3f(getUniformLocation("objColor"), objColor.x, objColor.y, objColor.z);
  glUniform3f(getUniformLocation("material.specular"),  0.5f, 0.5f, 0.5f);
  glUniform1f(getUniformLocation("material.shininess"), 32.0f);
  renderTextureUnit(GL_TEXTURE0, "material.diffuse");
  renderTextureUnit(GL_TEXTURE1, "material.specular");

  BaseGLRender::render(GL_TRIANGLES, 36);
}
