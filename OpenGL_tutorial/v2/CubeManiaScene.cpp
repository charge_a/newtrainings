// CubeManiaScene - scattered cubes
#include "BaseObject.h"

void CubeManiaScene::setup()
{
  cube = new CubeD();
  cube->setupMain();
  position.push_back(glm::vec3( 0.0f,  0.0f,  0.0f));
  position.push_back(glm::vec3( 2.0f,  5.0f, -15.0f));
  position.push_back(glm::vec3(-1.5f, -2.2f, -2.5f));
  position.push_back(glm::vec3(-3.8f, -2.0f, -12.3f));
  position.push_back(glm::vec3( 2.4f, -0.4f, -3.5f));
  position.push_back(glm::vec3(-1.7f,  3.0f, -7.5f));
  position.push_back(glm::vec3( 1.3f, -2.0f, -2.5f));
  position.push_back(glm::vec3( 1.5f,  2.0f, -2.5f));
  position.push_back(glm::vec3( 1.5f,  0.2f, -1.5f));
  position.push_back(glm::vec3(-1.3f,  1.0f, -1.5f));
  for (size_t i = 0; i < position.size(); i++) {
    rotation.push_back(20.0f * i);
  }
  CAM::instance()->setCamaraAt(glm::vec3(0, 0, 3));
  angle = 0;

  bulb = new BulbA();
  bulb->setupMain();
}

void CubeManiaScene::render() {
  for (size_t i = 0; i < position.size(); i++) {
    cube->lightPos = glm::vec3(0, 0, 1); // bulb->objPos;
    cube->objPos = position[i];
    cube->objRotationAngle = glm::radians(rotation[i] + angle);
    cube->objRotation = glm::vec3(1.0f, 0.3f, 0.5f);
    cube->renderMain();
  }
  // angle ++;
  if (angle > 360) angle -= 360;
  bulb->renderMain();
}
