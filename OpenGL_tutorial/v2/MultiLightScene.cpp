// MultiLightScene - multiple light sources
#include "BaseObject.h"

void MultiLightScene::setup()
{
  cube = new CubeE();
  cube->setupMain();
  cube_pos.push_back(glm::vec3( 0.0f,  0.0f,  1.0f));
  cube_pos.push_back(glm::vec3( 1.0f,  0.0f,  1.0f));
  cube_pos.push_back(glm::vec3(-1.0f,  0.0f,  1.0f));
  cube_pos.push_back(glm::vec3( 0.0f,  0.0f,  0.0f));
  cube_pos.push_back(glm::vec3( 1.0f,  0.0f,  0.0f));
  cube_pos.push_back(glm::vec3(-1.0f,  0.0f,  0.0f));
  cube_pos.push_back(glm::vec3( 0.0f,  0.0f, -1.0f));
  cube_pos.push_back(glm::vec3( 1.0f,  0.0f, -1.0f));
  cube_pos.push_back(glm::vec3(-1.0f,  0.0f, -1.0f));

  bulb = new BulbB();
  bulb->setupMain();

  CAM::instance()->setCamaraAt(glm::vec3(0, 0, 3));
  angle = 0;
  nBulb = 4;
}

void MultiLightScene::render() {

  // Point source attenuation params
  float t = glm::radians(angle);
  for (size_t i = 0; i < nBulb; i++) {
    switch (i) {
      case 0: {
        glm::vec3 lightSourcePos(sin(t), 1, cos(t));
        bulb->objPos = lightSourcePos;
        bulb->objColor = glm::vec3(1, 0, 0);
      }
      break;
      case 1: {
        glm::vec3 lightSourcePos(sin(t), -1, cos(t));
        bulb->objPos = lightSourcePos;
        bulb->objColor = glm::vec3(0, 1, 0);
      }
      break;
      case 2: {
        glm::vec3 lightSourcePos(2, 0, sin(t));
        bulb->objPos = lightSourcePos;
        bulb->objColor = glm::vec3(1, 1, 0);
      }
      break;
      case 3: {
        glm::vec3 lightSourcePos(-2, 0, sin(t));
        bulb->objPos = lightSourcePos;
        bulb->objColor = glm::vec3(0, 1, 1);
      }
      break;
    }
    bulb->renderMain();
    cube->renderParamsLightSource(i, bulb);
  }

  for (size_t i = 0; i < cube_pos.size(); i++) {
    cube->objPos = cube_pos[i];
    // cube->objRotationAngle = glm::radians(rotation[i] + angle);
    // cube->objRotation = glm::vec3(1.0f, 0.3f, 0.5f);
    cube->renderMain();
  }
  angle ++;
  if (angle > 360) angle -= 360;
}
