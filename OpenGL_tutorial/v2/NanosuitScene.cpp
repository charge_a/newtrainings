#include "BaseObject.h"

float nanoAngle;

void NanosuitScene::setup() {
  model = new OGLModel("models/nanosuit/nanosuit.obj");
  // shader = new OGLShader("shaders/assimpBasicShader.vsh", "shaders/assimpBasicShader.fsh");
  shader = new OGLShader("shaders/assimpPointLightShader.vsh", "shaders/assimpPointLightShader.fsh");

  CAM::instance()->setCamaraAt(glm::vec3(0, 1, 5));
  CAM::instance()->setCamaraLookAt(glm::vec3(0, 2, 0));

  nanoAngle=0.0;
}

void NanosuitScene::render() {
  shader->Use();
  glm::mat4 m, v, p;
  float f = 0.3f;
  m = glm::scale(m, glm::vec3(f, f, f));
  m = glm::rotate(m, glm::radians(nanoAngle), glm::vec3(0, 1, 0));
  p = glm::perspective(45.0f, (float)getScreenWidth() / (float)getScreenHeight(), 0.1f, 100.0f);
  v = CAM::instance()->getViewMatrix();
  glUniformMatrix4fv(glGetUniformLocation(shader->pu, "m"), 1, GL_FALSE, glm::value_ptr(m));
  glUniformMatrix4fv(glGetUniformLocation(shader->pu, "v"), 1, GL_FALSE, glm::value_ptr(v));
  glUniformMatrix4fv(glGetUniformLocation(shader->pu, "p"), 1, GL_FALSE, glm::value_ptr(p));

  glUniform3f(glGetUniformLocation(shader->pu, "pointLights[0].ambient"), 0.6, 0.6, 0.6);
  glUniform3f(glGetUniformLocation(shader->pu, "pointLights[0].diffuse"), 0.6, 0.6, 0.6);
  glUniform3f(glGetUniformLocation(shader->pu, "pointLights[0].specular"), 1.0, 1.0, 1.0);
  glUniform3f(glGetUniformLocation(shader->pu, "pointLights[0].position"), 0.0, 2.0, 4.0);
  glUniform1f(glGetUniformLocation(shader->pu, "pointLights[0].constant"), 1.0);
  glUniform1f(glGetUniformLocation(shader->pu, "pointLights[0].linear"),   0.09);
  glUniform1f(glGetUniformLocation(shader->pu, "pointLights[0].quadratic"), 0.032);
  glm::vec3 cam = CAM::instance()->getCameraAt();
  glUniform3f(glGetUniformLocation(shader->pu, "cameraPos"), cam.x, cam.y, cam.z);

  glUniform1f(glGetUniformLocation(shader->pu, "material.shininess"), 32.0);

  model->Draw(*shader);

  nanoAngle ++;
  if (nanoAngle >= 360) {
    nanoAngle -= 360;
  }
}
