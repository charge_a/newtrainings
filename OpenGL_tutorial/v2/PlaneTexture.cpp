#include "BaseObject.h"

void PlaneTexture::setup()
{
  const GLfloat pos[] =
  {
    0.5,  0.5,  0.0,
    0.5, -0.5,  0.0,
   -0.5, -0.5,  0.0,
   -0.5,  0.5,  0.0
  };
  const GLfloat tex[] =
  {
    1.0, 1.0,
    1.0, 0.0,
    0.0, 0.0,
    0.0, 1.0
  };
  addVertexAttributes(0, pos, sizeof(pos), 3);
  addVertexAttributes(1, tex, sizeof(tex), 2);

  const GLuint indices[] =
  {
    0, 1, 2,
    0, 2, 3
  };
  addVertexIndices(indices, sizeof(indices));
  addShaders("shaders/imageTexture.vsh", "shaders/imageTexture.fsh");

  // Load texture
  OGLTexture t("texture/container2.png");
  texUnitTexId[GL_TEXTURE0] = t.texId;

}

void PlaneTexture::render() {
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texUnitTexId[GL_TEXTURE0]);
  glUniform1i(glGetUniformLocation(pu, "imgTexture"), 0);

  BaseGLRender::render(GL_TRIANGLES, 6);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE0, 0);
}
