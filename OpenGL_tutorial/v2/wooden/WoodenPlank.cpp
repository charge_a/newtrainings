#include "WoodenPlank.h"
#include <iostream>

void WoodenPlank::setup() {
  const GLfloat pos[] =
  {
    0.5,  0.5,  0.0,
    0.5, -0.5,  0.0,
   -0.5, -0.5,  0.0,
   -0.5,  0.5,  0.0
  };
  const GLfloat tex[] =
  {
    1.0, 1.0,
    1.0, 0.0,
    0.0, 0.0,
    0.0, 1.0
  };
  const GLuint indices[] =
  {
    0, 1, 2,
    0, 2, 3
  };
  addVertexAttributes(0, pos, sizeof(pos), 3);
  addVertexAttributes(1, tex, sizeof(tex), 2);
  addVertexIndices(indices, sizeof(indices));
  addShaders("v2/wooden/plank.vsh", "v2/wooden/plank.fsh");
  addTextureUnit(GL_TEXTURE0, "v2/wooden/plank.jpg");

}

void WoodenPlank::render() {
  renderTextureUnit(GL_TEXTURE0, "img");
  BaseGLRender::render(GL_TRIANGLES, 6);
}
