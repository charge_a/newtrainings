#pragma once
#include "../BaseObject.h"

class WoodenPlank : public BaseObject {
private:


public:
  WoodenPlank () {}
  virtual void setup();
  virtual void render();
};

class WoodenScene : public BaseScene {
private:
  WoodenPlank plank;
  BulbB *bulb;
  float angleB;
  GLboolean bling;
  float roomSize;

  void drawPlank (glm::vec3 translateBy, float rotateAngle, glm::vec3 rotateAxis);
  void drawRoom();

public:
  WoodenScene () {}
  void setup();
  void render();

  void handleKeyboard(int key, int action);
};
