#include "WoodenPlank.h"

void WoodenScene::setup() {
  plank.setupMain();
  bulb = new BulbB();
  bulb->setupMain();
  angleB = 0;
  bling = false;
  roomSize = 10.0;
  bulb->objPos = glm::vec3(0.0, -(roomSize / 2) + 0.05, 0);

  CAM::instance()->setCamaraAt(glm::vec3(0, 0, roomSize*1.5));
  CAM::instance()->setCamaraLookAt(glm::vec3(UNI3(0)));
}

void WoodenScene::render() {
  plank.renderBefore();
  glUniform3f(glGetUniformLocation(plank.pu, "pointLights[0].ambient"), UNI3(0.8));
  glUniform3f(glGetUniformLocation(plank.pu, "pointLights[0].diffuse"), UNI3(0.8));
  glUniform3f(glGetUniformLocation(plank.pu, "pointLights[0].specular"), UNI3(1.0));
  glUniform3f(glGetUniformLocation(plank.pu, "pointLights[0].position"), bulb->objPos.x, bulb->objPos.y, bulb->objPos.z);
  glUniform1f(glGetUniformLocation(plank.pu, "pointLights[0].constant"), 1.0);
  glUniform1f(glGetUniformLocation(plank.pu, "pointLights[0].linear"),   0.09);
  glUniform1f(glGetUniformLocation(plank.pu, "pointLights[0].quadratic"), 0.032);
  glm::vec3 cam = CAM::instance()->getCameraAt();
  glUniform3f(glGetUniformLocation(plank.pu, "cameraPos"), cam.x, cam.y, cam.z);

  glUniform1f(glGetUniformLocation(plank.pu, "material.shininess"), 8.0);
  glUniform3f(glGetUniformLocation(plank.pu, "material.diffuse"), UNI3(0.5));
  glUniform3f(glGetUniformLocation(plank.pu, "material.specular"), UNI3(1));

  glUniform1i(glGetUniformLocation(plank.pu, "bling"), bling);

  drawRoom();

  plank.renderAfter();

  angleB += (angleB > 360) ? -360 : .4;
  float t = glm::radians(angleB);
  float t2 = t*10;
  glm::vec3 lightSourcePos(roomSize/2*sin(t2), roomSize/2*cos(t), roomSize/2*cos(t2));
  bulb->objPos = lightSourcePos;
  bulb->renderMain();
}

void WoodenScene::drawPlank (glm::vec3 translateBy, float rotateAngle, glm::vec3 rotateAxis) {

  plank.m = plank.v = plank.p = plank.id;
  plank.m = glm::translate(plank.m, translateBy);
  plank.m = glm::rotate(plank.m, glm::radians(rotateAngle), rotateAxis);
  plank.m = glm::scale(plank.m, glm::vec3(UNI3(roomSize)));
  plank.render();

}

float popsod = 0;

void WoodenScene::drawRoom() {

  if (popsod > 360) popsod-=360; popsod++;

  float w = roomSize;

  // back wall
  drawPlank(glm::vec3(0, 0, -w/2), 0, glm::vec3(1, 0, 0));
  // right wall
  drawPlank(glm::vec3(w/2, 0, 0), -90, glm::vec3(0, 1, 0));
  // left wall
  drawPlank(glm::vec3(-w/2, 0, 0), 90, glm::vec3(0, 1, 0));
  // top wall
  drawPlank(glm::vec3(0, w/2, 0), 90, glm::vec3(1, 0, 0));
  // bottom wall
  drawPlank(glm::vec3(0, -w/2, 0), -90, glm::vec3(1, 0, 0));

}


void WoodenScene::handleKeyboard(int key, int action) {
  if (key == GLFW_KEY_B) {
    // bling = !bling;
    bling = (action == GLFW_PRESS) || (action == GLFW_REPEAT);
    // printf("bling = %d, action = %d\n", bling, action);
  }
}
