#version 300 es

in highp vec2 TexCoord;
in highp vec3 Normal;
in highp vec3 FragPosWorld;

out highp vec4 color;

struct Material
{
  vec3 diffuse, specular;
  float shininess;
};
struct Light
{
  vec3 ambient, diffuse, specular;
  vec3 position;
  float constant;
  float linear;
  float quadratic;
};

uniform sampler2D img;
uniform Material material;
uniform highp vec3 cameraPos;
#define NUM_POINT_LIGHTS 1
uniform Light pointLights[NUM_POINT_LIGHTS];

uniform bool bling;

highp vec3 computePointLight (Light light, highp vec3 normal, highp vec3 viewDirection, highp vec3 FragPosWorld)
{
  // Ambient light
  highp vec3 ambient = material.diffuse * light.ambient;
  // Diffuse light
  highp vec3 lightDirection = normalize(light.position - FragPosWorld);
  highp float diff = max(0.0f, dot(normal, lightDirection)); // if angle is > 90, dot is -ve.
  highp vec3 diffuse = (diff * material.diffuse) * light.diffuse;
  // Specular light
  highp float diff2;
  if (bling) {
    highp vec3 halfwayDir = normalize(lightDirection + viewDirection);
    diff2 = max(0.0, dot(normal, halfwayDir));
  }
  else {
    highp vec3 reflectDir = reflect(-lightDirection, normal);
    diff2 = max(0.0f, dot(viewDirection, reflectDir));
  }
  // Blinn-Phong model
  highp float spec = pow(diff2, material.shininess);
  highp vec3 specular = material.specular * spec * light.specular;
  // attenuation is how light's intensity reduces from source with distance.
  highp float fragDist = length(light.position - FragPosWorld);
  highp float attenuation = 1.0f / (light.constant + light.linear*fragDist + light.quadratic * fragDist * fragDist);

  ambient *= attenuation;
  diffuse *= attenuation;
  specular *= attenuation;
  return ambient + diffuse + specular;
}

void main()
{
  highp vec3 norm = normalize(Normal);
  highp vec3 viewDirection = normalize(cameraPos - FragPosWorld);
  highp vec3 result = vec3(0,0,0);
  for(int i = 0; i < NUM_POINT_LIGHTS; i++)
  {
    result += computePointLight(pointLights[i], norm, viewDirection, FragPosWorld);
  }

  color = vec4( texture(img, TexCoord))* vec4(result, 1.0);
  // highp float gamma = 2.2;
  // color.rgb = pow(color.rgb, vec3(1.0/gamma));
  // color = vec4(vec3(gl_FragCoord.z), 1.0f);
}
