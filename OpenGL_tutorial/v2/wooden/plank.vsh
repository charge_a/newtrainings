#version 300 es

layout(location=0) in vec3 pos;
layout(location=1) in vec2 tex;

out vec2 TexCoord;
out vec3 Normal;
out vec3 FragPosWorld;

uniform mat4 m, v, p;

void main()
{
  gl_Position = p*v*m*vec4(pos, 1);
  TexCoord = tex;
  // Normal = mat3(transpose(inverse(m))) * vec3(0,1,0);
  Normal = vec3(m*vec4(0,0,1,0));
  FragPosWorld = vec3(m * vec4(pos, 1.0));
}
