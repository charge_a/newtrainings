#include "../UGLImpl.h"

namespace UGL {
  UGLAxis::UGLAxis()
  : mShader("v3/Axis/shader.vs","v3/Axis/shader.fs")
  {

  }

  void UGLAxis::setup() {
    const GLfloat v[] = {
      -1.0, 0.0, 0.0,
      1.0, 0.0, 0.0,

      0.0, 1.0, 0.0,
      0.0, -1.0, 0.0,

      0.0, 0.0, -1.0,
      0.0, 0.0, 1.0,

      // vertical bars
      1.0, 0.1, 0.0,
      1.0, -0.1, 0.0,
      -1.0, 0.1, 0.0,
      -1.0, -0.1, 0.0,

      // horizontal bars
      -0.1, 1.0, 0.0,
      0.1, 1.0, 0.0,
      -0.1, -1.0, 0.0,
      0.1, -1.0, 0.0,
    };
    UGLVertex w(v, sizeof(v), 3);
    setupVertex(0, w);
    vertex_geometry = GL_LINES;
  }

  void UGLAxis::render() {
    glLineWidth(2);
    UGLBaseRenderer::render(mShader);
  }

} /* UGL */
