#pragma once

#include "UGLImpl.h"
#include <stdlib.h>

typedef glm::vec2 float2D;
typedef glm::vec3 float3D;
typedef glm::vec4 float4D;

#define NORM(x) (glm::normalize(x))
#define RAND(a, b) (float)(a + (b-a)*((float)rand()/(RAND_MAX)))
#define DIST(v1, v2) glm::distance(v1, v2)

const float2D FLOAT2D_ZERO(0, 0);
const float3D FLOAT3D_ZERO(0, 0, 0);

/**
 * x = from left
 * y = from bottom
 * w = width
 * h = height
 */
struct Bounds {
  float x, y, w, h;
  Bounds(float _left, float _bottom, float _w, float _h)
  :x(_left), y(_bottom), w(_w), h(_h)
  {}
  Bounds(const Bounds & b)
  :x(b.x), y(b.y), w(b.w), h(b.h)
  {}
  Bounds()
  :x(0), y(0), w(0), h(0)
  {}

  Bounds InBounds(const Bounds & b)
  {
    return Bounds(
      b.x + (x * b.w),
      b.y + (y * b.h),
      (w * b.w),
      (h * b.h)
    );
  }

  bool ContainsPoint(float px, float py)
  {
    return ((px >= x) && (py >= y) && (px < x+w) && (py < y+h));
  }

};

const Bounds BOUNDS_ZERO;
const Bounds BOUNDS_FULL(0,0,1,1);
