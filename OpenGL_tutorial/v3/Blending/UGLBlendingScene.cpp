#include "../UGLImpl.h"

namespace UGL {

  class ImagePlane : public UGLBaseRenderer {
    OGLShader shader;
    UGLTextureUnit tu;
  public:
    // ImagePlane ();
    ImagePlane (OGLShader _shader, UGLTextureUnit _tu)
    : shader(_shader), tu(_tu)
    {

    }
    ImagePlane (const ImagePlane &image)
    : ImagePlane(image.shader, image.tu)
    {

    }

    void setup() {
      setupVertex(0, VERTEX_SQUARE_POS);
      setupVertex(1, VERTEX_SQUARE_TEX);
    }

    void render() {
      tu.BindTexture(GL_TEXTURE0, shader.pu, "image");
      UGLBaseRenderer::render(shader);
    }
  };

  ImagePlane *tile;

  UGLBlendingScene::UGLBlendingScene()
  {
    tile = new ImagePlane(
      OGLShader("v3/Blending/tex.vsh", "v3/Blending/tex.fsh"),
      UGLTextureUnit("texture/window.png", UGLTextureUnit::Channels::RGBA)
    );
  }

  /**
   * Enable Blending
   *
   * glBlendFunc(sFactor, dFactor):
   *    OpenGL uses Depth test to discard background fragments.
   *    However, if the foreground object is semi-transparent, we can blend the background fragments.
   *    So, when blending is enabled, fragments at different Depth are blended instead of discard.
   *    OpenGL does this in order the fragments are rendered on screen.
   *    Therefore, objects 'must be rendered in increasing z-value", otherwise blending will not work.
   *
   *    From second rendering, the color buffer is updated using blend equation:
   *        C = srcColor * sFactor + dstColor * dFactor
   *        where srcColor = Color being drawn
   *              dstColor = current color buffer
   *              sFactor, dFactor = contribution of 2 colors in the blend.
   *    Typically, sFactor is taken from the 4th component of srcColor called ALPHA, and dFactor = 1 - sFactor.
   *    So, sFactor = GL_SRC_ALPHA and dFactor = GL_ONE_MINUS_SRC_ALPHA.
   *
   * glBlendColor:
   *    To specify another C_constant to the blend equation.
   *
   * glBlendFuncSeparate(sFactor, dFactor, sAlpha, dAlpha): different options for RGB channels and Alpha channel.
   *
   * glBlendEquation:
   *    Usually, in the blend equation, we add the contribution of 2 colors.
   *    But this function allows us to change that to -
   *      GL_FUNC_ADD: default
   *      GL_FUNC_SUBTRACT: C = src - dst
   *      GL_FUNC_REVERSE_SUBTRACT C = dst - src
   *    But, it's not usually required.
   *
   *
   * In general, following is the order of drawing with blending enabled -
   *  1. Draw all opaque objects first.
      2. Sort all the transparent objects.
      3. Draw all the transparent objects in sorted order.
   */
  void UGLBlendingScene::setup() {
    tile->setup();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  }

  void UGLBlendingScene::render() {
    int n = 50;
    for (size_t i = 0; i < n; i++) {
      float dz = 0.15 * i * (UGLCounter::get().oscillating() + 0.01);
      float theta = dz*2;
      tile->m = id;
      tile->m = glm::translate(tile->m, glm::vec3(0, 0, -1 + dz));
      tile->m = glm::rotate(tile->m, theta, glm::vec3(0, 0, 1));
      tile->render();

    }
  }
} /* UGL */
