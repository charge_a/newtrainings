#version 300 es

in highp vec2 Tex;
out highp vec4 color;

uniform sampler2D image;

void main()
{
  highp vec4 tex = texture(image, Tex);
  // if (tex.a < 0.1)
  //   discard;
  color = tex;
}
