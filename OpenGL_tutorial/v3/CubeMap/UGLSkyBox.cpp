#include "../UGLImpl.h"
namespace UGL {

  UGLBaseRenderer* mirror1;

  UGLSkyBox::UGLSkyBox()
  : shader("v3/CubeMap/cubemap.vsh", "v3/CubeMap/cubemap.fsh")
  {
    isCubeMap = true;
    mirror1 = new UGLMirror2();
  }
  void UGLSkyBox::setup() {
    cmap.faces[UGLTextureUnitCubeMap::BACK]   = "cubemaps/skybox/back.jpg";
    cmap.faces[UGLTextureUnitCubeMap::BOTTOM] = "cubemaps/skybox/bottom.jpg";
    cmap.faces[UGLTextureUnitCubeMap::FRONT]  = "cubemaps/skybox/front.jpg";
    cmap.faces[UGLTextureUnitCubeMap::LEFT]   = "cubemaps/skybox/left.jpg";
    cmap.faces[UGLTextureUnitCubeMap::RIGHT]  = "cubemaps/skybox/right.jpg";
    cmap.faces[UGLTextureUnitCubeMap::TOP]    = "cubemaps/skybox/top.jpg";
    cmap.setup();

    setupVertex(0, VERTEX_SKYBOX_POS);

    mirror1->setup();
  }
  void UGLSkyBox::render() {
    glDepthMask(GL_FALSE);
      cmap.BindCubeMap(GL_TEXTURE0, shader.pu, "skybox");
      UGLBaseRenderer::render(shader);
      cmap.UnBindCubeMap();
    glDepthMask(GL_TRUE);

    mirror1->render();
  }
} /* UGL */
