#include "../UGLImpl.h"
#include "../../v2/BaseObject.h"
namespace UGL {
  namespace SkyBoxObjects {
    class UGLNanosuit : public UGLBaseRenderer {
      OGLModel nanosuit;
      OGLShader shader;

    public:
      UGLTextureUnitCubeMap cmap;
      UGLNanosuit ()
      // : shader("v3/CubeMap/object.vsh", "v3/CubeMap/object_reflect_nanosuit.fsh")
      : shader("v3/CubeMap/object.vsh", "v3/CubeMap/object_refract.fsh")
      , nanosuit("models/nanosuit/nanosuit.obj")
      {

      }

      void setup() {

        CAM::instance()->setCamaraAt(glm::vec3(0, 2, 5));
        CAM::instance()->setCamaraLookAt(glm::vec3(0, 2, -1));

      }

      void defineAdditionalUniforms(OGLShader &s) {
        cmap.BindCubeMap(GL_TEXTURE0, s.pu, "skybox");
        glm::vec3 cam = CAM::instance()->getCameraAt();
        glUniform3f(glGetUniformLocation(s.pu, "viewPos"), cam.x, cam.y, cam.z);
      }

      void render() {
        m = glm::scale(id, glm::vec3(UNI3(.25)));
        m = glm::rotate(m, UGLCounter::get().angle(), glm::vec3(0, 1, 0));
        UGLBaseRenderer::render(shader);
      }

      void renderGeometry() {
        nanosuit.Draw(shader);
      }

    };

    class UGLSample2 : public UGLSample {
    public:
      UGLTextureUnitCubeMap cmap;
      UGLSample2 () {
        // shader = OGLShader("v3/CubeMap/object.vsh", "v3/CubeMap/object_reflect.fsh");
        shader = OGLShader("v3/CubeMap/object.vsh", "v3/CubeMap/object_refract.fsh");
      }

      void setup() {
        UGLSample::setup();
        setupVertex(1, VERTEX_CUBE_NORM);
        setupVertex(2, VERTEX_CUBE_TEX);
      }

      void defineAdditionalUniforms(OGLShader &s) {
        cmap.BindCubeMap(GL_TEXTURE2, s.pu, "skybox");
        glm::vec3 cam = CAM::instance()->getCameraAt();
        glUniform3f(glGetUniformLocation(s.pu, "viewPos"), cam.x, cam.y, cam.z);
      }

      void render() {
        m = glm::translate(id, glm::vec3(2, 2, 0));
        UGLSample::render();
      }

    };

  } /* SkyBoxObjects */

  using namespace SkyBoxObjects;

  UGLNanosuit *sbeNanoSuit;
  UGLSample2 *mir;

  UGLSkyBoxEnvironment::UGLSkyBoxEnvironment()
  : shader("v3/CubeMap/cubemap_optimised.vsh", "v3/CubeMap/cubemap.fsh")
  {
    isCubeMap = true;
    sbeNanoSuit = new UGLNanosuit();
    mir = new UGLSample2();
  }
  void UGLSkyBoxEnvironment::setup() {
    cmap.faces[UGLTextureUnitCubeMap::BACK]   = "cubemaps/skybox/back.jpg";
    cmap.faces[UGLTextureUnitCubeMap::BOTTOM] = "cubemaps/skybox/bottom.jpg";
    cmap.faces[UGLTextureUnitCubeMap::FRONT]  = "cubemaps/skybox/front.jpg";
    cmap.faces[UGLTextureUnitCubeMap::LEFT]   = "cubemaps/skybox/left.jpg";
    cmap.faces[UGLTextureUnitCubeMap::RIGHT]  = "cubemaps/skybox/right.jpg";
    cmap.faces[UGLTextureUnitCubeMap::TOP]    = "cubemaps/skybox/top.jpg";
    cmap.setup();

    setupVertex(0, VERTEX_SKYBOX_POS);

    sbeNanoSuit->setup();
    sbeNanoSuit->cmap = cmap;

    mir->setup();
    mir->cmap = cmap;
  }
  void UGLSkyBoxEnvironment::defineAdditionalUniforms(OGLShader &shader) {
    cmap.BindCubeMap(GL_TEXTURE0, shader.pu, "skybox");
  }
  void UGLSkyBoxEnvironment::render() {
    // draw scene objects first
    sbeNanoSuit -> render();
    mir->render();

    // now draw cubemap
    // because now, DepthBuffer is filled with depth values of objects
    // and in vertex shader, we use gl_Position = pos.xyww, so that depth of each fragment is = 1 (far behind, max depth)
    // GL_LEQUAL: because when skybox is drawn, the background will have DB = 1, so if we did fragDepth < DB,
    // it will fail for all frags. So, we need fragDepth <= DB.
    glDepthFunc(GL_LEQUAL);
    UGLBaseRenderer::render(shader);
    glDepthFunc(GL_LESS);
  }
} /* UGL */
