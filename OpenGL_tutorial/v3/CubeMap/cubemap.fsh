#version 330 core
in highp vec3 TexCoords;

out highp vec4 color;

uniform samplerCube skybox;

void main()
{
  color = texture(skybox, TexCoords);
}
