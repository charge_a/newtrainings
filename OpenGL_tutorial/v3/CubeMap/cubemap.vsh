#version 330 core
layout (location=0) in vec3 pos;

out vec3 TexCoords;

uniform mat4 m, v, p;

void main()
{
  gl_Position = p*v*vec4(pos, 1.0);
  TexCoords = pos;
}
