#version 300 es
in highp vec3 Normal;
in highp vec3 FragPosWorld;
in highp vec2 TexCoord;

out highp vec4 color;

uniform samplerCube skybox;
uniform highp vec3 viewPos;



void main()
{
  highp vec3 I = normalize(FragPosWorld - viewPos);
  highp vec3 R = reflect(I, normalize(Normal));
  highp vec4 tColor = texture(skybox, R);

  color = tColor;
  // color = vec4(viewPos, 1);
}
