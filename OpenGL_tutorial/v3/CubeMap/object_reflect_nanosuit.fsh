#version 300 es
in highp vec3 Normal;
in highp vec3 FragPosWorld;
in highp vec2 TexCoord;

out highp vec4 color;

struct Material
{
  sampler2D texture_diffuse1;
  sampler2D texture_specular1;
};
uniform Material material;
uniform samplerCube skybox;
uniform highp vec3 viewPos;



void main()
{
  highp vec3 I = normalize(FragPosWorld - viewPos);
  highp vec3 R = reflect(I, normalize(Normal));
  highp vec4 tColor = texture(skybox, R);

  highp vec4 diffuse = texture(material.texture_diffuse1, TexCoord);
  highp vec4 spec = texture(material.texture_specular1, TexCoord);

  color = tColor;
  color += diffuse;
  // color = vec4(viewPos, 1);
}
