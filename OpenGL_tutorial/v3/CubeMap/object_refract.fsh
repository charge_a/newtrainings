#version 300 es
in highp vec3 Normal;
in highp vec3 FragPosWorld;
in highp vec2 TexCoord;

out highp vec4 color;

uniform samplerCube skybox;
uniform highp vec3 viewPos;

struct Material
{
  sampler2D texture_diffuse1;
  sampler2D texture_specular1;
  float shininess;
};
uniform Material material;


void main()
{
  highp float ratio = 1.0 / 1.52;
  highp vec3 I = normalize(FragPosWorld - viewPos);
  highp vec3 R = refract(I, normalize(Normal), ratio);
  color = texture(skybox, R);
}
