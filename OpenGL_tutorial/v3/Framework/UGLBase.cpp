#include "UGLBase.h"
#include <iostream>
#include <SOIL.h>

namespace UGL {

  // Vertex
  UGLVertex::UGLVertex(const GLfloat *data, int size, int dimension)
  {
    std::cout << "vertex size: " << size << '\n';
    std::cout << "vertex dimensin: " << dimension << '\n';
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    this->size = size / sizeof(GLfloat);
    this->dimension = dimension;
    this->isInstanceArray = false;
  }

  std::map<std::string, GLuint> UGLTextureUnit::textureCache;
  // Texture
  UGLTextureUnit::UGLTextureUnit(std::string filename, Channels type)
  {
    if (textureCache.find(filename) == textureCache.end()) {
      GLuint id;
      glGenTextures(1, &id);
      glBindTexture(GL_TEXTURE_2D, id);
      // texture loading
      if (type == Channels::RGB) {
        ImageLoader loader(filename, UGL::ImageLoader::FORMAT::RGB);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, loader.w, loader.h, 0, GL_RGB, GL_UNSIGNED_BYTE, loader.pixels);
      }
      else {
        ImageLoader loader(filename, UGL::ImageLoader::FORMAT::RGBA);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, loader.w, loader.h, 0, GL_RGBA, GL_UNSIGNED_BYTE, loader.pixels);
      }
      glGenerateMipmap(GL_TEXTURE_2D);
      // texture setings
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT); // repeat beyound s=(0,1)
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT); // repeat beyound t=(0,1)
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); // down sampling scheme
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // up sampling scheme

      glBindTexture(GL_TEXTURE_2D, 0);

      textureCache[filename] = id;
    }
    texture = textureCache[filename];
    textureType = type;
  }
  UGLTextureUnit::UGLTextureUnit(const UGLTextureUnit &tu) {
    texture = tu.texture;
    textureUnit = tu.textureUnit;
    textureType = tu.textureType;
  }
  void UGLTextureUnit::BindTexture(GLenum unit, GLuint shaderPu, std::string shaderVar) {
    glActiveTexture(unit);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(glGetUniformLocation(shaderPu, shaderVar.c_str()), unit - GL_TEXTURE0);
    textureUnit = unit;
  }
  void UGLTextureUnit::UnbindTexture() {
    glActiveTexture(textureUnit);
    glBindTexture(GL_TEXTURE_2D, 0);
  }
  // End of UGLTextureUnit

  UGLBaseRenderer::UGLBaseRenderer() {
    glGenVertexArrays(1, &vao);
    vertex_geometry = GL_TRIANGLES;
    isCubeMap = false;
  }

  void UGLBaseRenderer::setup() {

  }

  void UGLBaseRenderer::setupVertex(int location, const UGL::UGLVertex &v) {
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, v.vbo);
    glVertexAttribPointer(location, v.dimension, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(location);
    if (v.isInstanceArray) {
      glVertexAttribDivisor(location, 1);
    }
    glBindVertexArray(0);

    if (!v.isInstanceArray) {
      // vertex_count is #vertices for drawing object, not its instances
      vertex_count = v.size / v.dimension;
      std::cout << "vertex_count = " << vertex_count << '\n';
    }
  }

  void UGLBaseRenderer::render() {
    /* code */
    std::cout << "This was not supposed to get called..." << '\n';
  }

  void UGLBaseRenderer::render(OGLShader &shader) {
    GLint xywh[4];
    glGetIntegerv(GL_VIEWPORT, xywh);
    p = glm::perspective(45.0f, (float)xywh[2] / (float)xywh[3], 0.1f, 100.0f);
    v = CAM::instance()->getViewMatrix();

    if (isCubeMap) {
      // a skybox needs to remain translation-proof, i.e. it should respond to
      // camera rotation, perspective, etc. but not translation, so that how ever much
      // the user (camera) moves, the scene remains unmoved, giving illusion of a large
      // surrounding environment.
      //
      // To do that, one simply needs to take off top-left 3*3 matrix of 4*4 view matrix.
      // This can be achieved by converting mat4 -> mat3 -> mat4.
      v = glm::mat4(glm::mat3(v));
    }

    glBindVertexArray(vao);
    shader.Use();
    glUniformMatrix4fv(glGetUniformLocation(shader.pu, "m"), 1, GL_FALSE, glm::value_ptr(m));
    glUniformMatrix4fv(glGetUniformLocation(shader.pu, "v"), 1, GL_FALSE, glm::value_ptr(v));
    glUniformMatrix4fv(glGetUniformLocation(shader.pu, "p"), 1, GL_FALSE, glm::value_ptr(p));

    defineAdditionalUniforms(shader);
    setupAdditionalTextures(shader);
    renderGeometry();

    clearTextureBindings();
    glBindVertexArray(0);
  }

  void UGLBaseRenderer::defineAdditionalUniforms(OGLShader &shader) {
    // Uniforms must be defined AFTER binding a shader.
    // Override this method to provide additional uniform variables for shader in use.
  }

  void UGLBaseRenderer::setupAdditionalTextures(OGLShader &shader) {
    // A texture is bound to a target and texture unit. This binding is independent of VAO.
  }

  void UGLBaseRenderer::renderGeometry() {
    glDrawArrays(vertex_geometry, 0, vertex_count);
  }

  void UGLBaseRenderer::clearTextureBindings() {
    for (int i = 0; i < 9; i++) {
      glActiveTexture(GL_TEXTURE0 + i);
      glBindTexture(GL_TEXTURE_2D, 0);
    }
  }

  void UGLBaseRenderer::handleKeyboard(int key, int action) {
    /* code */
  }

  // ImageLoader
  ImageLoader::ImageLoader(std::string filename, FORMAT format)
  {
    switch (format) {
      case FORMAT::RGB:
      {
        pixels = SOIL_load_image(filename.c_str(), &w, &h, 0, SOIL_LOAD_RGB);
      }
      break;
      case FORMAT::RGBA:
      {
        pixels = SOIL_load_image(filename.c_str(), &w, &h, 0, SOIL_LOAD_RGBA);
      }
      break;
      case FORMAT::TGA:
      {
        int channels;
        pixels = SOIL_load_image(filename.c_str(), &w, &h, &channels, SOIL_LOAD_RGB);
      }
      break;
    }
  }
  ImageLoader::~ImageLoader()
  {
    SOIL_free_image_data(pixels);
  }
  // End of ImageLoader

  // UGLTextureUnitCubeMap
  UGLTextureUnitCubeMap::UGLTextureUnitCubeMap() {}
  UGLTextureUnitCubeMap::UGLTextureUnitCubeMap(const UGLTextureUnitCubeMap &tu)
  {
    texture = tu.texture;
    textureUnit = tu.textureUnit;
  }
  GLuint UGLTextureUnitCubeMap::setup() {
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

    for (size_t i = 0; i < 6; i++) {
      ImageLoader loader(faces[i], UGL::ImageLoader::FORMAT::TGA);
      glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB,
                   loader.w, loader.h, 0, GL_RGB, GL_UNSIGNED_BYTE, loader.pixels);
    }
    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
    return texture;
  }
  void UGLTextureUnitCubeMap::BindCubeMap(GLenum unit, GLuint shaderPu, std::string shaderVar) {
    glActiveTexture(unit);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
    glUniform1i(glGetUniformLocation(shaderPu, shaderVar.c_str()), unit - GL_TEXTURE0);
    textureUnit = unit;
  }
  void UGLTextureUnitCubeMap::UnBindCubeMap() {
    glActiveTexture(textureUnit);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
  }
  // End of UGLTextureUnitCubeMap

} /* UGL */
