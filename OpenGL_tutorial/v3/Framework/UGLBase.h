#pragma once
#include "../../basecode/Camera.h"
#include "../../basecode/VertexData.h"
#include "../../basecode/OGLShader.h"
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "../../GLUtils.h"

#include <string>
#include <map>

#define UNI3(x) x, x, x
typedef glm::vec2 gvec2;
typedef glm::vec3 gvec3;
typedef glm::vec4 gvec4;

namespace UGL {

  /**
   * VertexData to share between VAOs.
   * See UGLConstant.h for pre-defined vbos.
   */
  struct UGLVertex {
    GLuint vbo;
    int size;
    int dimension;
    bool isInstanceArray;
    UGLVertex(const GLfloat *data, int size, int dimension);
  };

  struct ImageLoader {
    enum FORMAT { RGB, RGBA, TGA };
    unsigned char* pixels;
    int w, h;
    ImageLoader(std::string filename, FORMAT format);
    ~ImageLoader();
  };

  struct UGLTextureUnit {
    enum Channels { RGB, RGBA };
    static std::map<std::string, GLuint> textureCache;
    GLuint texture;
    GLenum textureUnit;
    Channels textureType;
    UGLTextureUnit(std::string filename, Channels type);
    UGLTextureUnit(const UGLTextureUnit &tu);
    void BindTexture(GLenum unit, GLuint shaderPu, std::string shaderVar);
    void UnbindTexture();
  };

  /**
   * Minimal rendering capabilities
   */
  class UGLBaseRenderer {
  public:
    GLuint vao;
    glm::mat4 m, v, p, id;
    int vertex_count;
    GLenum vertex_geometry;
    bool isCubeMap;

  public:
    UGLBaseRenderer ();
    virtual void setup();
    void setupVertex(int location, const UGLVertex &v);
    virtual void render();
    void render(OGLShader &shader);
    virtual void defineAdditionalUniforms(OGLShader &shader);
    virtual void setupAdditionalTextures(OGLShader &shader);
    virtual void renderGeometry();
    void clearTextureBindings();
    virtual void handleKeyboard(int key, int action);
  };

  /**
   * A singleton for maintaining animation counters during render.
   * note that updation needs to be done once per render call.
   */
  class UGLCounter {
  private:
    float m_angle = 0;
    float m_speed = 1;
    UGLCounter () {};

  public:
    static UGLCounter& get() {
      static UGLCounter instance;
      return instance;
    }
    void update() {
      m_angle +=m_speed;
      if (m_angle > 360) m_angle -= 360;
    }
    float angle() {
      return glm::radians(m_angle);
    }
    float oscillating() {
      return (sin(angle()) + 1) / 2.0;
    }
    void setSpeed(float speed) {
      m_speed = speed;
    }
  };


  /**
   * Wrapper class for implementing Framebuffer for offscreen rendering
   * @param  w    width of framebuffer's attachments
   * @param  h    height of framebuffer's attachments
   * @param  type GL_RGB or GL_RGBA
   */
  class UGLFrameBuffer {
  public:
    UGLFrameBuffer ();
    void   setup();
    GLuint createTextureAttachment(int w, int h, UGLTextureUnit::Channels type);
    void   attachTextureAttachment(GLuint texture);
    GLuint createRenderBufferAttachment(int w, int h);
    void   attachRenderBufferAttachment(GLuint rbo);
    bool   valid();
    bool   prepare();
    GLuint framebuffer;
    GLuint texture;
    GLuint rbo;

  };


  struct UGLTextureUnitCubeMap {
    enum FACE { RIGHT, LEFT, TOP, BOTTOM, BACK, FRONT };
    GLuint texture;
    GLuint textureUnit;
    std::string faces[6];
    UGLTextureUnitCubeMap();
    UGLTextureUnitCubeMap(const UGLTextureUnitCubeMap &tu);
    GLuint setup();
    void   BindCubeMap(GLenum unit, GLuint shaderPu, std::string shaderVar);
    void   UnBindCubeMap();
  };


} /* UGL */
