#pragma once
#include "UGLBase.h"

namespace UGL {
  const GLfloat cube_pos_data [] = {
    -0.5f, -0.5f, -0.5f,
     0.5f, -0.5f, -0.5f,
     0.5f,  0.5f, -0.5f,
     0.5f,  0.5f, -0.5f,
    -0.5f,  0.5f, -0.5f,
    -0.5f, -0.5f, -0.5f,

    -0.5f, -0.5f,  0.5f,
     0.5f, -0.5f,  0.5f,
     0.5f,  0.5f,  0.5f,
     0.5f,  0.5f,  0.5f,
    -0.5f,  0.5f,  0.5f,
    -0.5f, -0.5f,  0.5f,

    -0.5f,  0.5f,  0.5f,
    -0.5f,  0.5f, -0.5f,
    -0.5f, -0.5f, -0.5f,
    -0.5f, -0.5f, -0.5f,
    -0.5f, -0.5f,  0.5f,
    -0.5f,  0.5f,  0.5f,

     0.5f,  0.5f,  0.5f,
     0.5f,  0.5f, -0.5f,
     0.5f, -0.5f, -0.5f,
     0.5f, -0.5f, -0.5f,
     0.5f, -0.5f,  0.5f,
     0.5f,  0.5f,  0.5f,

    -0.5f, -0.5f, -0.5f,
     0.5f, -0.5f, -0.5f,
     0.5f, -0.5f,  0.5f,
     0.5f, -0.5f,  0.5f,
    -0.5f, -0.5f,  0.5f,
    -0.5f, -0.5f, -0.5f,

    -0.5f,  0.5f, -0.5f,
     0.5f,  0.5f, -0.5f,
     0.5f,  0.5f,  0.5f,
     0.5f,  0.5f,  0.5f,
    -0.5f,  0.5f,  0.5f,
    -0.5f,  0.5f, -0.5f,
  };
  const GLfloat square_pos_data [] =
  {
    0.5,  0.5,  0.0,
    0.5, -0.5,  0.0,
   -0.5, -0.5,  0.0,

   -0.5, -0.5,  0.0,
   -0.5,  0.5,  0.0,
    0.5,  0.5,  0.0,
  };
  const GLfloat square_norm_data [] =
  {
    0.0,  0.0,  -1.0,
    0.0,  0.0,  -1.0,
    0.0,  0.0,  -1.0,

    0.0,  0.0,  -1.0,
    0.0,  0.0,  -1.0,
    0.0,  0.0,  -1.0,
  };
  const GLfloat cube_tex_data[] =
  {
    0.0f, 0.0f,
    1.0f, 0.0f,
    1.0f, 1.0f,
    1.0f, 1.0f,
    0.0f, 1.0f,
    0.0f, 0.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    1.0f, 1.0f,
    1.0f, 1.0f,
    0.0f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    1.0f, 1.0f,
    0.0f, 1.0f,
    0.0f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    1.0f, 0.0f,
    1.0f, 1.0f,
    0.0f, 1.0f,
    0.0f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
    1.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 1.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
    1.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 1.0f
  };
  const GLfloat square_tex_data [] =
  {
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0,

    0.0, 1.0,
    0.0, 0.0,
    1.0, 0.0,
  };
  const GLfloat skyboxVertices[] = {
    // Positions
    -1.0f,  1.0f, -1.0f,
    -1.0f, -1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
     1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,

    -1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,

     1.0f, -1.0f, -1.0f,
     1.0f, -1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,

    -1.0f, -1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,

    -1.0f,  1.0f, -1.0f,
     1.0f,  1.0f, -1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f, -1.0f,

    -1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
     1.0f, -1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
     1.0f, -1.0f,  1.0f
   };

   const GLfloat cube_norm[] =
   {
    0.0f,  0.0f, -1.0f,
    0.0f,  0.0f, -1.0f,
    0.0f,  0.0f, -1.0f,
    0.0f,  0.0f, -1.0f,
    0.0f,  0.0f, -1.0f,
    0.0f,  0.0f, -1.0f,

    0.0f,  0.0f, 1.0f,
    0.0f,  0.0f, 1.0f,
    0.0f,  0.0f, 1.0f,
    0.0f,  0.0f, 1.0f,
    0.0f,  0.0f, 1.0f,
    0.0f,  0.0f, 1.0f,

    -1.0f,  0.0f,  0.0f,
    -1.0f,  0.0f,  0.0f,
    -1.0f,  0.0f,  0.0f,
    -1.0f,  0.0f,  0.0f,
    -1.0f,  0.0f,  0.0f,
    -1.0f,  0.0f,  0.0f,

    1.0f,  0.0f,  0.0f,
    1.0f,  0.0f,  0.0f,
    1.0f,  0.0f,  0.0f,
    1.0f,  0.0f,  0.0f,
    1.0f,  0.0f,  0.0f,
    1.0f,  0.0f,  0.0f,

    0.0f, -1.0f,  0.0f,
    0.0f, -1.0f,  0.0f,
    0.0f, -1.0f,  0.0f,
    0.0f, -1.0f,  0.0f,
    0.0f, -1.0f,  0.0f,
    0.0f, -1.0f,  0.0f,

    0.0f,  1.0f,  0.0f,
    0.0f,  1.0f,  0.0f,
    0.0f,  1.0f,  0.0f,
    0.0f,  1.0f,  0.0f,
    0.0f,  1.0f,  0.0f,
    0.0f,  1.0f,  0.0f
  };


  class UGLVBOFactory {
  private:
    UGLVBOFactory () {}

  public:
    static UGLVertex getCubePos() {
      static UGLVertex *s_vertex = NULL;
      return (s_vertex
            ? *s_vertex
            : *( s_vertex = new UGL::UGLVertex(cube_pos_data, sizeof(cube_pos_data), 3) ));
    }
    static UGLVertex getSquarePos() {
      static UGLVertex *s_vertex = NULL;
      return (s_vertex
            ? *s_vertex
            : *( s_vertex = new UGL::UGLVertex(square_pos_data, sizeof(square_pos_data), 3) ));
    }
    static UGLVertex getCubeTex() {
      static UGLVertex *s_vertex = NULL;
      return (s_vertex
            ? *s_vertex
            : *( s_vertex = new UGL::UGLVertex(cube_tex_data, sizeof(cube_tex_data), 2) ));
    }
    static UGLVertex getSquareTex() {
      static UGLVertex *s_vertex = NULL;
      return (s_vertex
            ? *s_vertex
            : *( s_vertex = new UGL::UGLVertex(square_tex_data, sizeof(square_tex_data), 2) ));
    }
    static UGLVertex getSkyboxPos() {
      static UGLVertex *s_vertex = NULL;
      return (s_vertex
            ? *s_vertex
            : *( s_vertex = new UGL::UGLVertex(skyboxVertices, sizeof(skyboxVertices), 3) ));
    }
    static UGLVertex getCubeNormal() {
      static UGLVertex *s_vertex = NULL;
      return (s_vertex
            ? *s_vertex
            : *( s_vertex = new UGL::UGLVertex(cube_norm, sizeof(cube_norm), 3) ));
    }
    static UGLVertex getSquareNormal() {
      static UGLVertex *s_vertex = NULL;
      return (s_vertex
            ? *s_vertex
            : *( s_vertex = new UGL::UGLVertex(square_norm_data, sizeof(square_norm_data), 3) ));
    }
  };

  #define VERTEX_CUBE_POS   (UGLVBOFactory::getCubePos())
  #define VERTEX_SQUARE_POS (UGLVBOFactory::getSquarePos())
  #define VERTEX_CUBE_TEX   (UGLVBOFactory::getCubeTex())
  #define VERTEX_SQUARE_TEX (UGLVBOFactory::getSquareTex())
  #define VERTEX_SKYBOX_POS (UGLVBOFactory::getSkyboxPos())
  #define VERTEX_CUBE_NORM  (UGLVBOFactory::getCubeNormal())
  #define VERTEX_SQUARE_NORM (UGLVBOFactory::getSquareNormal())
  // NOTE: We use #define instead of "const static" because VBO initialisation
  // cannot happen before GL initialisation is complete.
} /* UGL */
