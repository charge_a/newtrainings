#include "UGLBase.h"

namespace UGL {
  UGLFrameBuffer::UGLFrameBuffer()
  {
  }

  void UGLFrameBuffer::setup() {
    glGenFramebuffers(1, &framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
  }

  GLuint UGLFrameBuffer::createTextureAttachment(int w, int h, UGLTextureUnit::Channels type)
  {
    // GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
      if (type == UGLTextureUnit::Channels::RGB) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h,
                     0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
      }
      else {
        // TODO;
      }
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);
    return texture;
  }

  void UGLFrameBuffer::attachTextureAttachment(GLuint texture) {
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                           texture, 0);
  }

  GLuint UGLFrameBuffer::createRenderBufferAttachment(int w, int h)
  {
    // GLuint rbo;
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
      glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, w, h);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    return rbo;
  }

  void UGLFrameBuffer::attachRenderBufferAttachment(GLuint rbo) {
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT,
                              GL_RENDERBUFFER, rbo);
  }

  bool UGLFrameBuffer::valid() {
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
      bool res = (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    return res;
  }

  bool UGLFrameBuffer::prepare() {
    attachTextureAttachment(createTextureAttachment(getScreenWidth(), getScreenHeight(), UGLTextureUnit::Channels::RGB));
    attachRenderBufferAttachment(createRenderBufferAttachment(getScreenWidth(), getScreenHeight()));
    return valid();
  }
} /* UGL */
