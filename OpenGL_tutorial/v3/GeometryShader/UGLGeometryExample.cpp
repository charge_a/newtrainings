#include "../UGLImpl.h"
#include "../../v2/BaseObject.h"

namespace UGL {
  namespace GeoSample {
    class Point : public UGLBaseRenderer {
      OGLShader *shader;
      OGLShader *shaderBasic;
      OGLShader *shaderLine;
      OGLShader *shaderHouse;
    public:
      Point ()
      {
        vertex_geometry = GL_POINTS;
      }

      void setup() {
        const GLfloat points [] =
        {
          -0.5,  0.5,
           0.5,  0.5,
           0.5, -0.5,
          -0.5, -0.5,
        };
        const UGLVertex v (points, sizeof(points), 2);
        setupVertex(0, v);

        shaderBasic = new OGLShader("v3/GeometryShader/geo.vsh", "v3/GeometryShader/geo.gsh", "v3/GeometryShader/geo.fsh");
        shaderLine = new OGLShader("v3/GeometryShader/geo.vsh", "v3/GeometryShader/geo_line.gsh", "v3/GeometryShader/geo.fsh");
        shaderHouse = new OGLShader("v3/GeometryShader/geo.vsh", "v3/GeometryShader/geo_house.gsh", "v3/GeometryShader/geo.fsh");
        shader = shaderBasic;
      }

      void render() {
        // m = glm::translate(id, glm::vec3(UGLCounter::get().oscillating(), 0, 0));
        UGLBaseRenderer::render(*shader);
      }

      void handleKeyboard(int key, int action) {
        switch (key) {
          case GLFW_KEY_0:
          {
            shader = shaderBasic;
          }
          break;
          case GLFW_KEY_1:
          {
            shader = shaderLine;
          }
          break;
          case GLFW_KEY_2:
          {
            shader = shaderHouse;
          }
          break;
        }
      }

    };

    // Nanosuit
    class Nanosuit : public UGLBaseRenderer {
    private:
      OGLModel nanosuit;
      OGLShader *shader;
      OGLShader *shaderExplosion;
      OGLShader *shaderNormal;
      OGLShader *shaderNanosuit;

    public:
      Nanosuit ()
      : nanosuit("models/nanosuit/nanosuit.obj")
      {
        shaderExplosion = new OGLShader("v3/GeometryShader/geo_nano.vsh", "v3/GeometryShader/geo_nano_expload.gsh", "v3/GeometryShader/geo_nano.fsh");
        shaderNormal = new OGLShader("v3/GeometryShader/geo_nano.vsh", "v3/GeometryShader/geo_nano_normal.gsh", "v3/GeometryShader/geo_nano_normal.fsh");
        shaderNanosuit = new OGLShader("v3/GeometryShader/geo_nano.vsh", "v3/GeometryShader/geo_nano.fsh");
        shader = shaderExplosion;
      }

      void setup() {
        CAM::instance()->setCamaraAt(glm::vec3(0, 2, 5));
        CAM::instance()->setCamaraLookAt(glm::vec3(0, 2, -1));
        glClearColor(0.6, 0.6, 0.6, 1.0);
      }

      void defineAdditionalUniforms(OGLShader &s) {
        glUniform1f(glGetUniformLocation(s.pu, "displacement"), UGLCounter::get().oscillating());
      }

      void render() {
        m = glm::scale(id, glm::vec3(UNI3(.25)));
        UGLBaseRenderer::render(*shader);
        UGLBaseRenderer::render(*shaderNanosuit);
      }

      void renderGeometry() {
        nanosuit.Draw(*shader);
      }

      void handleKeyboard(int key, int action) {
        switch (key) {
          case GLFW_KEY_0:
          {
            shader = shaderNanosuit;
          }
          break;
          case GLFW_KEY_1:
          {
            shader = shaderExplosion;
          }
          break;
          case GLFW_KEY_2:
          {
            shader = shaderNormal;
          }
          break;
        }
      }

    };
  } /* GeoSample */

  // GeoSample::Point *obj;
  GeoSample::Nanosuit *obj;
  // UGLGeometryExample
  UGLGeometryExample::UGLGeometryExample()
  {
    // obj = new GeoSample::Point();
    obj = new GeoSample::Nanosuit();
  }

  void UGLGeometryExample::setup()
  {
    obj->setup();
  }

  void UGLGeometryExample::render()
  {
    obj->render();
  }

  void UGLGeometryExample::handleKeyboard(int key, int action) {
    obj->handleKeyboard(key, action);
  }
} /* UGL */
