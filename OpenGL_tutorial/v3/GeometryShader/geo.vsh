#version 330 core
layout (location = 0) in vec2 pos;

uniform mat4 m, v, p;

void main()
{
  gl_Position = p*v*m*vec4(pos, 0, 1);
  // gl_PointSize = 1;
}
