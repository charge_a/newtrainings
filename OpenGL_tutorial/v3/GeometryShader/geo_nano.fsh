#version 330 core
out vec4 color;
in vec2 TexCoords;

in VS_OUT {
  vec3 Normal;
  vec2 TexCoords;
} fs_in;

struct Material
{
  sampler2D texture_diffuse1;
  sampler2D texture_specular1;
};
uniform Material material;

void main()
{
  color = texture(material.texture_diffuse1, fs_in.TexCoords);
}
