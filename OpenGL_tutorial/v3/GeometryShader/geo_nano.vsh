#version 330 core
layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 tex;

uniform mat4 m, v, p;

out VS_OUT {
  vec3 Normal;
  vec2 TexCoords;
} vs_out;

void main()
{
  gl_Position = p*v*m*vec4(pos, 1);
  // gl_PointSize = 1;
  vs_out.TexCoords = tex;
  vec3 normal1 = mat3(transpose(inverse(v*m))) * normal;
  vs_out.Normal = normalize(vec3( p * vec4(normal1, 1) ));
}
