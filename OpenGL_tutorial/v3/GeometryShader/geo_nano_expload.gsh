#version 330 core
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in VS_OUT {
  vec3 Normal;
  vec2 TexCoords;
} gs_in[];

out VS_OUT {
  vec3 Normal;
  vec2 TexCoords;
} gs_out;

uniform float displacement;

vec3 GetNormal()
{
  vec3 side1 = vec3(gl_in[0].gl_Position) - vec3(gl_in[1].gl_Position);
  vec3 side2 = vec3(gl_in[2].gl_Position) - vec3(gl_in[1].gl_Position);
  return normalize(cross(side1, side2));
}

vec4 explode (vec4 pos, vec3 normal) {
  return pos + vec4(normal * displacement, 0);
}

void main()
{
  vec3 N = GetNormal();

  for(int i = 0; i < 3; i++)
  {
    gl_Position = explode(gl_in[i].gl_Position, N);
    gs_out.TexCoords = gs_in[i].TexCoords;
    gs_out.Normal = gs_in[i].Normal;
    EmitVertex();
  }

  EndPrimitive();

}
