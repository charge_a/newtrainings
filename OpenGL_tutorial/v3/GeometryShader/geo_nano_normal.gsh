#version 330 core
layout (triangles) in;
layout (line_strip, max_vertices = 2) out;

in VS_OUT {
  vec3 Normal;
  vec2 TexCoords;
} gs_in[];

uniform float displacement;

const float lineLength = 0.05f;

void main()
{
  for(int i = 0; i < 3; i++)
  {
    gl_Position = gl_in[i].gl_Position;
    EmitVertex();
    gl_Position = gl_in[i].gl_Position + vec4(gs_in[i].Normal, 0) * lineLength;
    EmitVertex();
    EndPrimitive();
  }

}
