#version 330 core
in vec3 Pos;
out vec4 color;

void main()
{
  color = vec4(0.5 + Pos.y, 0.5 + Pos.y, 0.5 + Pos.y, 1);
  float d = Pos.x*Pos.x + Pos.y*Pos.y;
  color = vec4(0.5, 0.5, 0.85, 1) * d;
  color += vec4(1, 1, 1, 0) * 0.1;
  // color = vec4(Pos, 1);
}
