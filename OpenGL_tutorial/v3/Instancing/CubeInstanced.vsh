#version 330 core
layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 loc;
layout (location = 2) in float size;

uniform mat4 m, v, p;
uniform float osc;

out vec3 Pos;

void main()
{
  vec3 pos1 = (pos * size) + loc;
  gl_Position = p*v*m*vec4(pos1, 1.0);
  Pos = pos;
}
