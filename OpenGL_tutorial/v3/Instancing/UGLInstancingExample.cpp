#include "../UGLImpl.h"
#include <vector>

namespace UGL {

  namespace InnerInstancing {
    // Cube
    class Cube : public UGLBaseRenderer {
    public:
      Cube ()
      {
        vertShader = "v3/Instancing/Cube.vsh";
        fragShader = "v3/Instancing/Cube.fsh";
      }
      void setup() {
        shader = new OGLShader(vertShader.c_str(), fragShader.c_str());
        setupVertex(0, VERTEX_CUBE_POS);
      }
      void render() {
        m = glm::rotate(id, UGLCounter::get().angle(), glm::vec3(0, 1, 0));
        UGLBaseRenderer::render(*shader);
      }
      OGLShader *shader;
      std::string vertShader;
      std::string fragShader;
    };

    // CubeInstanced
    class CubeInstanced : public Cube {
    public:
      uint instance_count;
      std::vector<gvec3> obj_vert_dir;
      std::vector<gvec3> loc;
      std::vector<float> sz;
      std::map<int, int> parent_dir;

      const int FRACTAL_LEVELS = 6;
      const float ACTUAL_COORD = 0.5;
      const float LEVEL_REDUCE = 0.35;

      CubeInstanced ()
      {
        vertShader = "v3/Instancing/CubeInstanced.vsh";
      }
      void calcVertexDirections() {
        int sgn[] = {-1, 1};
        for (int i=0; i<2; i++) {
          for (int j=0; j<2; j++) {
            gvec3 P(sgn[i], sgn[j], 1);
            int d1 = obj_vert_dir.size();
            obj_vert_dir.push_back(P);
            int d2 = obj_vert_dir.size();
            obj_vert_dir.push_back(-P);
            parent_dir[d1] = d2;
            parent_dir[d2] = d1;
          }
        }
      }
      void fractal(int level, gvec3 center, float len, int parent_corner) {
        if (level == 0)
        {
          return;
        }
        loc.push_back(center);
        sz.push_back(len);

        float fraction = LEVEL_REDUCE;
        for (int i=0; i < obj_vert_dir.size(); i++)
        {
          if (parent_corner == -1 || parent_dir[parent_corner] != i) {
            gvec3 v = obj_vert_dir[i] * (1 + fraction) * len * ACTUAL_COORD;
            fractal(level - 1, center + v, len * fraction, i);
          }

        }
      }
      void setup() {
        Cube::setup();

        obj_vert_dir.resize(0);
        calcVertexDirections();

        loc.resize(0);
        sz.resize(0);
        fractal(FRACTAL_LEVELS, gvec3(UNI3(0)), 1, -1);
        instance_count = loc.size();

        UGLVertex v((const GLfloat *)&loc[0], sizeof(gvec3) * loc.size(), 3);
        v.isInstanceArray = true;
        setupVertex(1, v);

        UGLVertex w((const GLfloat *)&sz[0], sizeof(float) * sz.size(), 1);
        w.isInstanceArray = true;
        setupVertex(2, w);

        CAM::instance()->setCamaraAt(glm::vec3(0, 2, 3));
        UGLCounter::get().setSpeed(0.2);
      }

      void renderGeometry() {
        glDrawArraysInstanced(vertex_geometry, 0, vertex_count, instance_count);
      }
      void defineAdditionalUniforms(OGLShader &s) {
        glUniform1f(glGetUniformLocation(s.pu, "osc"), 1- UGLCounter::get().oscillating());
      }
    };


    // CubeInstanced2
    class CubeInstanced2 : public Cube {
    public:
      int instance_count;
      std::vector<gvec3> loc;
      std::vector<float> sz;
      std::vector<gvec3> face_normal;
      std::map<int, int> face_diag;
      CubeInstanced2 ()
      {
        vertShader = "v3/Instancing/CubeInstanced.vsh";
        gvec3 p;
        int i = 0;
        p = gvec3(1, 0, 0);
        face_normal.push_back(p);
        face_normal.push_back(-p);
        face_diag[i] = i+1;
        face_diag[i+1] = i;
        i+=2;

        p = gvec3(0, 1, 0);
        face_normal.push_back(p);
        face_normal.push_back(-p);
        face_diag[i] = i+1;
        face_diag[i+1] = i;
        i+=2;

        p = gvec3(0, 0, 1);
        face_normal.push_back(p);
        face_normal.push_back(-p);
        face_diag[i] = i+1;
        face_diag[i+1] = i;
        i+=2;

      }
      void renderGeometry() {
        glDrawArraysInstanced(vertex_geometry, 0, vertex_count, instance_count);
      }
      void defineAdditionalUniforms(OGLShader &s) {
        glUniform1f(glGetUniformLocation(s.pu, "osc"), 1- UGLCounter::get().oscillating());
      }

      void fractal(int fromFace, int level, float len, gvec3 center) {
        if (level == 0)
        {
          return;
        }
        loc.push_back(center);
        sz.push_back(len);
        float fac = .5;
        for (int i = 0; i < face_normal.size(); i++)
        {
          if (fromFace == -1 || fromFace != face_diag[i])
          {
            gvec3 nCenter = center + face_normal[i] * ((1.0f) * len*0.5f);
            fractal(i, level - 1, len*fac, nCenter);
          }
        }
      }

      void generateInstances() {
        loc.resize(0);
        sz.resize(0);
        fractal(-1, 7, 1, gvec3(0,0,0));
      }

      void setup() {
        Cube::setup();

        generateInstances();
        instance_count = loc.size();
        std::cout << "#instances: " << instance_count << '\n';

        UGLVertex a((const GLfloat *)&loc[0], sizeof(gvec3) * loc.size(), 3);
        UGLVertex b((const GLfloat *)&sz[0], sizeof(GLfloat) * sz.size(), 1);
        a.isInstanceArray = true;
        b.isInstanceArray = true;

        setupVertex(1, a);
        setupVertex(2, b);

        CAM::instance()->setCamaraAt(glm::vec3(0, 2, 3));
        UGLCounter::get().setSpeed(0.2);
      }
    };
  } /* InnerInstancing */

  UGLBaseRenderer *obj;

  // UGLInstancingExample
  //
  UGLInstancingExample::UGLInstancingExample() {
    // obj = new InnerInstancing::CubeInstanced();
    obj = new InnerInstancing::CubeInstanced2();
  }

  void UGLInstancingExample::setup() {
    obj->setup();
  }

  void UGLInstancingExample::render() {
    obj->render();
  }

  void UGLInstancingExample::handleKeyboard(int key, int action) {
    /* code */
  }
} /* UGL */
