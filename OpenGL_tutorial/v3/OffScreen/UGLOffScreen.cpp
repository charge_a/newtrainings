#include "../UGLImpl.h"
#include <iostream>
namespace UGL {

  OGLShader *shader [7];

  UGLOffScreen::UGLOffScreen(UGLBaseRenderer *mirror)
  {
    sceneToRender = mirror;
  }

  void UGLOffScreen::setup() {
    UGLFrameBuffer framebuffer;
    framebuffer.setup();
    fboTex = framebuffer.createTextureAttachment(getScreenWidth()/2, getScreenHeight(), UGLTextureUnit::Channels::RGB);
    GLuint fboRbo = framebuffer.createRenderBufferAttachment(getScreenWidth()/2, getScreenHeight());
    framebuffer.attachTextureAttachment(fboTex);
    framebuffer.attachRenderBufferAttachment(fboRbo);
    if (framebuffer.valid()) {
      fbo = framebuffer.framebuffer;
    }
    else {
      std::cerr << "Error creating Framebuffer" << '\n';
    }
    setupVertex(0, VERTEX_SQUARE_POS);
    setupVertex(1, VERTEX_SQUARE_TEX);
    sceneToRender->setup();

    // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    shader[0] = new OGLShader("v3/OffScreen/tex.vsh", "v3/OffScreen/tex.fsh");
    shader[1] = new OGLShader("v3/OffScreen/tex.vsh", "v3/OffScreen/tex_invert.fsh");
    shader[2] = new OGLShader("v3/OffScreen/tex.vsh", "v3/OffScreen/tex_grayscale.fsh");
    shader[3] = new OGLShader("v3/OffScreen/tex.vsh", "v3/OffScreen/tex_sharpen.fsh");
    shader[4] = new OGLShader("v3/OffScreen/tex.vsh", "v3/OffScreen/tex_blur.fsh");
    shader[5] = new OGLShader("v3/OffScreen/tex.vsh", "v3/OffScreen/tex_edge_detection.fsh");
    shader[6] = new OGLShader("v3/OffScreen/tex.vsh", "v3/OffScreen/tex_gamma_correction.fsh");
    fboShader = shader[0];
  }

  void UGLOffScreen::render() {
    glViewport(0, 0, getScreenWidth()/2, getScreenHeight());

    // 1. Render scene to fbo
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glClearColor(0.1, 0.1, 0.1, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    renderOriginal();

    // 2. Bind screenFBO and render fbo texture on screen
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClearColor(0.1, 0.1, 0.1, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    glDisable(GL_DEPTH_TEST);
    glBindTexture(GL_TEXTURE_2D, fboTex);
    UGLBaseRenderer::render(*fboShader);

    glViewport(getScreenWidth()/2, 0, getScreenWidth()/2, getScreenHeight());
    renderOriginal();

  }

  void UGLOffScreen::renderOriginal() {
    glEnable(GL_DEPTH_TEST);
    sceneToRender->render();

  }

  void UGLOffScreen::handleKeyboard(int key, int action) {
    switch (key) {
      case GLFW_KEY_0:
      {
        fboShader = shader[0];
      }
      break;
      case GLFW_KEY_1:
      {
        fboShader = shader[1];
      }
      break;
      case GLFW_KEY_2:
      {
        fboShader = shader[2];
      }
      break;
      case GLFW_KEY_3:
      {
        fboShader = shader[3];
      }
      break;
      case GLFW_KEY_4:
      {
        fboShader = shader[4];
      }
      break;
      case GLFW_KEY_5:
      {
        fboShader = shader[5];
      }
      break;
      case GLFW_KEY_6:
      {
        fboShader = shader[6];
      }
      break;
    }
  }

} /* UGL */
