#version 330 core
in highp vec2 TexCoords;
out highp vec4 color;

uniform sampler2D screenTexture;

void main()
{
    color = texture(screenTexture, TexCoords);
}
