#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoords;

out vec2 TexCoords;

void main()
{
    gl_Position = vec4(position.x, -position.y, position.z, 1.0f / 2.0f);
    // 4th component w = 1/2 because position is [-0.5, 0.5], so a fbo texture
    // will get scaled down to half. Since gl_Position is actually x/w, y/w, z/w
    // we make w = 0.5. It's a hack.
    TexCoords = texCoords;
}
