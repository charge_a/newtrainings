#version 330 core
in highp vec2 TexCoords;
out highp vec4 color;

uniform sampler2D screenTexture;

void main()
{
    color = texture(screenTexture, TexCoords);
    float gamma = 2.2;
    color.rgb = pow(color.rgb, vec3(1.0 / gamma));
}
