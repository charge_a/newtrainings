#version 330 core
in highp vec2 TexCoords;
out highp vec4 color;

uniform sampler2D screenTexture;

void main()
{
    color = texture(screenTexture, TexCoords);
    // highp float avg = (color.r + color.g + color.b) / 3.0;
    highp float avg = 0.2126 * color.r + 0.7152 * color.g + 0.0722 * color.b;
    color.r = color.g = color.b = avg;
}
