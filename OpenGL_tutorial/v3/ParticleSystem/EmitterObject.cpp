#include "EmitterTemplate.h"

namespace UGL {

EmitterObject::EmitterObject() {
  mGravity[0] = 0;
  mGravity[1] = 0;
  mLife = 0;
  mTime = 0;

}

void EmitterObject::setup() {
  Emitter newEmitter = {0.0};
  float oRadius = 0.1;
  float oVelocity = 0.5;
  float oDecay = 0.25;
  float oSize = 8.0;
  float oColor = 0.25;
  for (int i=0; i<NUM_PARTICLES; i++) {
    Particle &p = newEmitter.eParticles[i];
    p.pID = glm::radians(360.0 * (((float)i) / NUM_PARTICLES));
    p.pRadiusOffset = RAND(oRadius, 1.0);
    p.pVelocityOffset = RAND(-oVelocity, oVelocity);
    p.pDecayOffset = RAND(-oDecay, oDecay);
    p.pSizeOffset = RAND(-oSize, oSize);
    p.pColorOffset[0] = RAND(-oColor, oColor);
    p.pColorOffset[1] = RAND(-oColor, oColor);
    p.pColorOffset[2] = RAND(-oColor, oColor);
  }
  newEmitter.eRadius = 0.75;
  newEmitter.eVelocity = 3.0;
  newEmitter.eDecay = 2.0;
  newEmitter.eSizeStart = 32.0;
  newEmitter.eSizeEnd = 8.0;
  newEmitter.eColorStart[0] = 1.0;
  newEmitter.eColorStart[1] = 0.5;
  newEmitter.eColorStart[2] = 0.0;
  newEmitter.eColorEnd[0] = 0.25;
  newEmitter.eColorEnd[1] = 0.0;
  newEmitter.eColorEnd[2] = 0.0;

  float growth = newEmitter.eRadius / newEmitter.eVelocity;
  mLife = growth + newEmitter.eDecay + oDecay;

  float airResistance = 10.0;
  mGravity[0] = 0.0;
  mGravity[1] = -9.81 * (1.0 / airResistance);

  mEmitter = newEmitter;

  UGLVertex v((const GLfloat *)mEmitter.eParticles, sizeof(mEmitter.eParticles), 123);
  glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, v.vbo);
    glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), BUFFER_OFFSET(offsetof(Particle, pID)));
    glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), BUFFER_OFFSET(offsetof(Particle, pRadiusOffset)));
    glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), BUFFER_OFFSET(offsetof(Particle, pVelocityOffset)));
    glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), BUFFER_OFFSET(offsetof(Particle, pDecayOffset)));
    glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), BUFFER_OFFSET(offsetof(Particle, pSizeOffset)));
    glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), BUFFER_OFFSET(offsetof(Particle, pColorOffset)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);
    glEnableVertexAttribArray(4);
    glEnableVertexAttribArray(5);
  glBindVertexArray(0);

  emittershader = new OGLShader("v3/ParticleSystem/emitter.vs","v3/ParticleSystem/emitter.fs");
}

void EmitterObject::render() {
  vertex_count = NUM_PARTICLES;
  vertex_geometry = GL_POINTS;
  UGLBaseRenderer::render(*emittershader);
}

void EmitterObject::defineAdditionalUniforms(OGLShader &shader) {
  glUniform2f (glGetUniformLocation(shader.pu, "u_Gravity")     , mGravity[0], mGravity[1]);
  glUniform1f (glGetUniformLocation(shader.pu, "u_Time")        , mTime);
  glUniform1f (glGetUniformLocation(shader.pu, "u_eRadius")     , mEmitter.eRadius);
  glUniform1f (glGetUniformLocation(shader.pu, "u_eVelocity")   , mEmitter.eVelocity);
  glUniform1f (glGetUniformLocation(shader.pu, "u_eDecay")      , mEmitter.eDecay);
  glUniform1f (glGetUniformLocation(shader.pu, "u_eSizeStart")  , mEmitter.eSizeStart);
  glUniform1f (glGetUniformLocation(shader.pu, "u_eSizeEnd")    , mEmitter.eSizeEnd);
  glUniform3f (glGetUniformLocation(shader.pu, "u_eColorStart") , mEmitter.eColorStart[0]
                                                                , mEmitter.eColorStart[1]
                                                                , mEmitter.eColorStart[2]);
  glUniform3f (glGetUniformLocation(shader.pu, "u_eColorEnd")   , mEmitter.eColorEnd[0]
                                                                , mEmitter.eColorEnd[1]
                                                                , mEmitter.eColorEnd[2]);

  static int printVar = 1;
  if (printVar) {
    printVar --;
    printf ("u_Gravity: %f,%f\n"   , mGravity[0], mGravity[1]);
    printf ("u_Time: %f\n"      , mTime);
    printf ("u_eRadius: %f\n"   , mEmitter.eRadius);
    printf ("u_eVelocity: %f\n" , mEmitter.eVelocity);
    printf ("u_eDecay:%f\n"    , mEmitter.eDecay);
    // printf ("u_eSize:%f\n"     , mEmitter.eSize);
    // printf ("u_eColor:%f,%f,%f\n"    , mEmitter.eColor[0]
    //                       , mEmitter.eColor[1]
    //                       , mEmitter.eColor[2]);
  }

}

void EmitterObject::update(float timeElapsed) {
  mTime += timeElapsed;
  if (mTime > mLife) {
    mTime = 0;
  }
}

} /* UGL */
