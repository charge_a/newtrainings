#pragma once
#include "../UGLImpl.h"

#define NUM_PARTICLES 360*5

typedef glm::vec2 float2D;
typedef glm::vec3 float3D;

#define NORM(x) (glm::normalize(x))

struct Particle {
  float3D mPosition;
  float3D mVelocity;
  float3D mColorInitial;
  float3D mColorFinal;
};

namespace UGL {
  class Emitter : public UGLBaseRenderer {
    Particle mParticles[NUM_PARTICLES];
    float2D mOrigin;
    UGLTextureUnit *texture;
    GLuint mVBO;
  public:
    Emitter ();
    void setup();
    void render(OGLShader &shader);
    void defineAdditionalUniforms(OGLShader &shader);
  };
} /* UGL */
