#include "EmitterTemplate.h"

#include <stdlib.h>
#define RAND(a, b) (a + (b-a)*((float)rand()/(RAND_MAX)))

namespace UGL {

  Emitter::Emitter() {

  }

  void Emitter::setup() {
    mOrigin = float2D(0,0);

    for (int i = 0; i < NUM_PARTICLES; i++) {
      float angle = glm::radians(float(i));
      float3D axis = float3D(cos(angle), sin(angle), 0);
      Particle &particle = mParticles [i];
      particle.mVelocity = NORM(axis) * RAND(0.1f, 0.8f);// * 0.1f;
      particle.mColorInitial = float3D(1, 0.75, 0)*3.0f;
      particle.mColorFinal = float3D(0.25, 1, 0);
      particle.mPosition = float3D(RAND(-0.4f, 0.4f),RAND(-0.4f, 0.4f),0);
    }

    glGenBuffers(1, &mVBO);
    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(mParticles), (const GLfloat *)mParticles, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(vao);
      glBindBuffer(GL_ARRAY_BUFFER, mVBO);

      glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), BUFFER_OFFSET(offsetof(Particle, mVelocity)));
      glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), BUFFER_OFFSET(offsetof(Particle, mColorInitial)));
      glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), BUFFER_OFFSET(offsetof(Particle, mColorFinal)));
      glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), BUFFER_OFFSET(offsetof(Particle, mPosition)));

      glEnableVertexAttribArray(0);
      glEnableVertexAttribArray(1);
      glEnableVertexAttribArray(2);
      glEnableVertexAttribArray(3);

    glBindVertexArray(0);

    vertex_geometry = GL_POINTS;
    vertex_count = NUM_PARTICLES;
    glEnable(GL_PROGRAM_POINT_SIZE);

    texture = new UGLTextureUnit("texture/flower.png", UGLTextureUnit::Channels::RGBA);
    // standard blending of transparent textures.
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  }

  void Emitter::render(OGLShader &shader) {
    for (int i=0; i<NUM_PARTICLES; i++) {
      Particle &p = mParticles[i];
      p.mPosition += (p.mVelocity * 0.01f);

      if (glm::distance(p.mPosition, float3D(0,0,0)) > 4.0f) {
        p.mPosition = float3D(0,0,0);
      }
    }
    glBindVertexArray(vao);
      glBindBuffer(GL_ARRAY_BUFFER, mVBO);
      glBufferData(GL_ARRAY_BUFFER, sizeof(mParticles), (const GLfloat *)mParticles, GL_DYNAMIC_DRAW);
    glBindVertexArray(0);
    // glDepthMask(GL_FALSE);
    UGLBaseRenderer::render(shader);
  }

  void Emitter::defineAdditionalUniforms(OGLShader &shader) {

    glUniform2f (glGetUniformLocation(shader.pu, "origin"), mOrigin.x, mOrigin.y);
    texture->BindTexture(GL_TEXTURE0, shader.pu, "mask");
  }
} /* UGL */
