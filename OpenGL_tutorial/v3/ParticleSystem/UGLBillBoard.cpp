#include "../UGLImpl.h"
// #include "EmitterTemplate.h"
// #include "v1/ParticleEmitter.h"
// #include "v1/Sparks/SparkParticleEmitter.h"
#include "v1/LineEmitter/LineEmitter.h"

// https://www.raywenderlich.com/37600/opengl-es-particle-system-tutorial-part-1
// https://www.raywenderlich.com/38248/opengl-es-particle-system-tutorial-part-2
//
namespace UGL {
  // Emitter *emitter;
  // SparkParticleEmitter *emitter;
  LineParticleEmitter *emitter;

  UGLBillBoard::UGLBillBoard() {
    // emitter = new SparkParticleEmitter();
    emitter = new LineParticleEmitter();
  }

  void UGLBillBoard::setup() {
    emitter->setup();
  }

  void UGLBillBoard::render() {
    emitter->render(*emitter->mShader);
  }

  void UGLBillBoard::defineAdditionalUniforms(OGLShader &shader) {
  }
} /* UGL */
