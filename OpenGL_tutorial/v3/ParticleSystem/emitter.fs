#version 330 core

out vec4 FragColor;

in vec3 vColor;
uniform sampler2D mask;

void main()
{
  vec4 tColor = texture(mask, gl_PointCoord);
  vec4 color = vec4(vColor, 1.0);

  FragColor = color * tColor;
}
