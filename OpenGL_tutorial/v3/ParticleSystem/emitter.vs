#version 330 core
layout(location=0) in vec3 aVelocity;
layout(location=1) in vec3 aColorInitial;
layout(location=2) in vec3 aColorFinal;
layout(location=3) in vec3 aPosition;

uniform mat4 m, v, p;
uniform vec2 origin;

out vec3 vColor;

void main()
{
  // float x = origin.x + time * aVelocity.x;
  // float y = origin.y + time * aVelocity.y;

  // gl_Position = p*v*m*vec4(x,y,0.0,1.0);
  gl_Position = p*v*m*vec4(aPosition,1.0);
  gl_PointSize = 2.0;
  float decay = distance(origin, aPosition.xy);
  decay = sqrt(decay);
  decay = sqrt(decay);
  vColor = mix(aColorInitial, aColorFinal, decay);
}
