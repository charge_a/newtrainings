#include "LineEmitter.h"

struct Particle
{
  float3D mPosition;
  float3D mVelocity;
  float   mLife;
  float   mAge;
};

LineParticleEmitter::LineParticleEmitter()
{
  mNumParticles = 200000;
  mParticles = new Particle[mNumParticles];
  mSpeed = 0.01;
}

void LineParticleEmitter::configureParticle(int index) {
  Particle& t = mParticles[index];
  t.mPosition = evalCurve (index / (float)(2*mNumParticles));
  t.mVelocity = float3D(RAND(-0.01, 0.01), RAND(-0.01, 0.01), RAND(-0.01, 0.01));
  t.mLife = RAND(0, 2);
  t.mAge = 0;

  t.mVelocity *= 0.05f;
}

void LineParticleEmitter::updateParticle(int index) {
  Particle& t = mParticles[index];
  t.mPosition += t.mVelocity;
  t.mAge += mSpeed;
  if (t.mAge > t.mLife) {
    configureParticle(index);
  }
}

void LineParticleEmitter::setup() {
  mShader = new OGLShader("v3/ParticleSystem/v1/LineEmitter/shader.vs",
                          "v3/ParticleSystem/v1/LineEmitter/shader.fs");
  ParticleEmitter::setup();
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE);
}

void LineParticleEmitter::defineAdditionalUniforms(OGLShader &shader) {
}

void LineParticleEmitter::setAttributes() {
  setAttribute(0, 3, offsetof(Particle, mPosition));
  setAttribute(1, 1, offsetof(Particle, mAge));
  setAttribute(2, 1, offsetof(Particle, mLife));
}

int LineParticleEmitter::getStride() {
  return sizeof(Particle);
}

float3D LineParticleEmitter::evalCurve(float t) {
  int numLoops = 4;
  float y = cos(glm::radians(numLoops*4*t*180)) * 0.25;
  float z = sin(glm::radians(numLoops*4*t*180)) * 0.25;
  float x = (t*2 - 0.5);
  return float3D(x, y, z);
}
