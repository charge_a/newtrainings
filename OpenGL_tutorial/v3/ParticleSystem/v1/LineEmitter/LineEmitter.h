#pragma once

#include "../ParticleEmitter.h"

class LineParticleEmitter : public ParticleEmitter {
public:
  LineParticleEmitter ();
  virtual void configureParticle (int index);
  virtual void updateParticle (int index);
  virtual void setup();
  virtual void defineAdditionalUniforms(OGLShader &shader);
  virtual void setAttributes();
  virtual int getStride();

  float3D evalCurve(float t);


  OGLShader *mShader;
  float3D   mOrigin;
  float     mSpeed;

};
