#version 330 core

out vec4 color;
in float vAge;
in float vLife;

void main()
{
  color = vec4(1, 0,0,1);
  float d = distance(gl_PointCoord, vec2(0.5, 0.5));
  color.a = (1 - vAge / vLife);
  color.r = (1 - vAge / vLife);
  color.g = (1 - vAge / vLife);
  color.b = (1 - (0.001*vAge) / vLife);
  if (d > 0.5*(1 - vAge / vLife)) {
    color = vec4(0);
  }
}
