#version 330 core
layout(location = 0) in vec3 pos;
layout(location = 1) in float age;
layout(location = 2) in float life;

uniform mat4 m, v, p;

out float vAge;
out float vLife;

void main()
{
  gl_Position = p*v*m*vec4(pos, 1);
  gl_PointSize = 4 * (1.0-(age/life));

  vAge = age;
  vLife = life;
}
