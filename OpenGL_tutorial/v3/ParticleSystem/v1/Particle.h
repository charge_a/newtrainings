#pragma once

#include "../../Basetypes.h"

struct Particle {
  float3D mPosition;
  float3D mVelocity;
  float3D mColor;
  float mSize;
  int mID;
};
