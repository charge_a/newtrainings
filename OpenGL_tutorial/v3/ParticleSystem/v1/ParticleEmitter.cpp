// #include "Particle.h"
#include "ParticleEmitter.h"

ParticleEmitter::ParticleEmitter() {
}

void ParticleEmitter::setupParticles() {
  for (int i = 0; i < mNumParticles; i++)
  {
    configureParticle(i);
  }

  glGenBuffers(1, &mVBO);
  glBindBuffer(GL_ARRAY_BUFFER, mVBO);
  glBufferData(GL_ARRAY_BUFFER, getStride() * mNumParticles, mParticles, GL_DYNAMIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void ParticleEmitter::setup() {

  setupParticles();
  setAttributes();

  vertex_geometry = GL_POINTS;
  vertex_count = mNumParticles;
  glEnable(GL_PROGRAM_POINT_SIZE);


}

void ParticleEmitter::setAttribute(GLuint location, GLuint dimension, GLuint offset) {
  glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
      glVertexAttribPointer(location,
                            dimension,
                            GL_FLOAT,
                            GL_FALSE,
                            getStride(),
                            BUFFER_OFFSET(offset)
                          );
      glEnableVertexAttribArray(location);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);
}

void ParticleEmitter::render(OGLShader &shader) {

  for (int i=0; i < mNumParticles; i++)
  {
    updateParticle(i);
  }

  glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    glBufferData(GL_ARRAY_BUFFER, getStride() * mNumParticles, mParticles, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  glDisable(GL_DEPTH_TEST);
    UGL::UGLBaseRenderer::render(shader);
  glEnable(GL_DEPTH_TEST);
}

void ParticleEmitter::defineAdditionalUniforms(OGLShader &shader) {
}
