#pragma once

#include "../../Basetypes.h"

struct Particle;
class ParticleEmitter : public UGL::UGLBaseRenderer {
public:
  ParticleEmitter ();
  virtual void setup();
  virtual void render(OGLShader &shader);
  virtual void defineAdditionalUniforms(OGLShader &shader);
  void setupParticles();
  void setAttribute(GLuint location, GLuint dimension, GLuint offset);
  virtual void setAttributes() = 0;
  virtual void configureParticle (int index) = 0;
  virtual void updateParticle (int index) = 0;
  virtual int getStride() = 0;

protected:
  Particle *              mParticles;
  GLuint                  mVBO;

public:
  int                     mNumParticles;
};
