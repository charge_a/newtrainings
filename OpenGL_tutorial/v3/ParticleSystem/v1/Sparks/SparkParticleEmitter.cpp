#include "SparkParticleEmitter.h"

struct Particle {
  float3D mPosition;
  float3D mVelocity;
  float3D mColor0;
  float3D mColor1;
  float   mSize;
  int     mID;
  float   mLife;
};
// 3*4*4 + 4 + 4 = 48 + 8 = 56

SparkParticleEmitter::SparkParticleEmitter() {
  mNumParticles = 10000;
  mParticles = new Particle[mNumParticles];
  mOrigin = float3D(-0.5,0,0);
  mSpeed = 0.009;
}

void SparkParticleEmitter::configureParticle(int index) {
  Particle &particle = mParticles[index];

  particle.mID = index;
  float z = (float)particle.mID/mNumParticles;
  particle.mPosition = mOrigin;
  particle.mVelocity = float3D(RAND(0.5, 1), RAND(-0.3, 0.3), 0) * mSpeed;
  particle.mColor0 = float3D(1, 1, 0);
  particle.mColor1 = float3D(1, 0, 0);
  particle.mSize = 10;
  particle.mLife = RAND(0.5, 1);
}

void SparkParticleEmitter::updateParticle(int index) {
  Particle &particle = mParticles[index];

  particle.mPosition += particle.mVelocity;
  particle.mLife -= mSpeed;

  if (particle.mLife <= 0) {
    configureParticle(index);
  }
}

void SparkParticleEmitter::setup() {
  mShader = new OGLShader("v3/ParticleSystem/v1/Sparks/emitter.vs","v3/ParticleSystem/v1/Sparks/emitter.fs");
  ParticleEmitter::setup();

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

}

void SparkParticleEmitter::setAttributes() {
  setAttribute(0, 3, offsetof(Particle, mPosition));
  setAttribute(1, 1, offsetof(Particle, mLife));
  setAttribute(2, 3, offsetof(Particle, mColor0));
  setAttribute(3, 3, offsetof(Particle, mColor1));
  setAttribute(4, 1, offsetof(Particle, mSize));
}

int SparkParticleEmitter::getStride() {
  return sizeof(Particle);
}

void SparkParticleEmitter::defineAdditionalUniforms(OGLShader &shader) {
  glUniform3f(
    glGetUniformLocation(shader.pu, "origin"),
    mOrigin.x, mOrigin.y, mOrigin.z
  );

  // float angle = UGL::UGLCounter::get().angle()*2;
  // mOrigin = float3D(cos(angle), sin(angle), 0) * 0.25f;
}
