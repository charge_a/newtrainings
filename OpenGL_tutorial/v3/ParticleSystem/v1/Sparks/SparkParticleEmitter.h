#pragma once

#include "../ParticleEmitter.h"

class SparkParticleEmitter : public ParticleEmitter {
public:
  SparkParticleEmitter ();
  virtual void configureParticle (int index);
  virtual void updateParticle (int index);
  virtual void setup();
  virtual void defineAdditionalUniforms(OGLShader &shader);
  virtual void setAttributes();
  virtual int getStride();


  OGLShader *mShader;
  float3D   mOrigin;
  float     mSpeed;

};
