#version 330 core

out vec4 FragColor;

in vec3 vColor;
in float vDecay;

void main()
{
  // radial gradiant
  float d = 0.5 - distance(gl_PointCoord, vec2(0.5, 0.5));
  d = clamp(d, 0, 1);
  vec4 color = vec4(vColor, d);

  // disk
  float coreRadius = 0.2;
  if(d > coreRadius)
  {
    color = vec4(vColor, (1.0));
  }

  // make particles disappear
  if (vDecay > 0.2) {
    color.a *= (1.0 - (vDecay ));
  }

  FragColor = color;
}
