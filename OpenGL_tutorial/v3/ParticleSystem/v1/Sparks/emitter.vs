#version 330 core
layout(location=0) in vec3 aPosition;
layout(location=1) in float aTime;
layout(location=2) in vec3 aColorInitial;
layout(location=3) in vec3 aColorFinal;
layout(location=4) in float aSize;

uniform mat4 m, v, p;

uniform vec3 origin;

out vec3 vColor;
out float vDecay;

void main()
{
  gl_Position = p*v*m*vec4(aPosition,1.0);

  vDecay = 1.0 - aTime;
  vColor = mix(aColorInitial, aColorFinal, vDecay);
  gl_PointSize = aSize * (0.0 + vDecay);
}
