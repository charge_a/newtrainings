#version 330 core

out vec4 FragColor;

in vec3 vColor;

void main()
{
  vec4 color = vec4(vColor, 1.0);

  FragColor = color;
}
