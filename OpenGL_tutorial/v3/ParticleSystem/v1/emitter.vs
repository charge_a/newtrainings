#version 330 core
layout(location=0) in vec3 aPosition;
layout(location=1) in vec3 aVelocity;
layout(location=2) in vec3 aColor;
layout(location=3) in float aSize;

uniform mat4 m, v, p;

out vec3 vColor;

void main()
{
  gl_Position = p*v*m*vec4(aPosition,1.0);
  gl_PointSize = aSize;
  float decay = distance(vec2(0,0), aPosition.xy);
  decay = sqrt(decay);
  decay = sqrt(decay);
  vColor = aColor;
}
