#include "../UGLImpl.h"

namespace UGL {

  // Multi pass Render objects
  namespace MPObjects {
    class ImageViewer : public UGLBaseRenderer {
    public:
      OGLShader *shader;
      ImageViewer() {}

      void setup() {
        setupVertex(0, VERTEX_SQUARE_POS);
        setupVertex(1, VERTEX_SQUARE_TEX);

        shader = new OGLShader("v3/RadialBlur/ImageViewer.vs","v3/RadialBlur/ImageViewer.fs");
      }

      void renderTexure(GLuint texture) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
        // m = glm::rotate(id, UGLCounter::get().angle(), gvec3(0,0,1));
        m = glm::scale(id, gvec3(UNI3(0.5)));
        UGLBaseRenderer::render(*shader);
      }
    };

    class BackBufferRenderer : public UGLBaseRenderer {
    public:
      BackBufferRenderer () {}

      void setup() {
        const GLfloat screen_quad[] =
        {
          1.0,  1.0,  0.0,
          1.0, -1.0,  0.0,
         -1.0, -1.0,  0.0,

         -1.0, -1.0,  0.0,
         -1.0,  1.0,  0.0,
          1.0,  1.0,  0.0,
        };
        UGLVertex fullScreenVBO(screen_quad, sizeof(screen_quad), 3);
        setupVertex(0, fullScreenVBO);
        setupVertex(1, VERTEX_SQUARE_TEX);
      }

      void renderTexure(GLuint texture, GLuint targetFB, OGLShader *shader) {
        glBindFramebuffer(GL_FRAMEBUFFER, targetFB);
          glClear(GL_COLOR_BUFFER_BIT);
          glDisable(GL_DEPTH_TEST);
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, texture);
            UGLBaseRenderer::render(*shader);
          glEnable(GL_DEPTH_TEST);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
      }
    };
  } /* MPObjects */








  // Scene objects
  MPObjects::ImageViewer        *viewer;
  UGLTextureUnit                *image;
  UGLFrameBuffer                fb[2]; // ping-pong fbos
  MPObjects::BackBufferRenderer *bbr;
  OGLShader                     *shaderBlur;





  // Main class
  UGLMultipassRender::UGLMultipassRender() {
    viewer = new MPObjects::ImageViewer();
    image = new UGLTextureUnit("texture/face.jpg", UGLTextureUnit::Channels::RGB);
    bbr = new MPObjects::BackBufferRenderer();
  }

  void UGLMultipassRender::setup() {
    viewer->setup();
    bbr->setup();
    int w = getScreenWidth();
    int h = getScreenHeight();
    for (int i = 0; i < 2; i++) {
      fb[i].setup();
      fb[i].attachTextureAttachment(fb[i].createTextureAttachment(w, h, UGLTextureUnit::Channels::RGB));
    }
    shaderBlur = new OGLShader("v3/RadialBlur/baseTex.vs","v3/RadialBlur/texBlur.fs");
    numPasses = 0;
  }

  void UGLMultipassRender::render() {
    // Pass 1: Render scene to fb
    glClearColor(0.1, 0.1, 0.1, 1.0);
    glBindFramebuffer(GL_FRAMEBUFFER, fb[0].framebuffer);
      glClear(GL_COLOR_BUFFER_BIT);
      glDisable(GL_DEPTH_TEST);
      viewer->renderTexure(image->texture);
      glEnable(GL_DEPTH_TEST);

    OGLShader *pingpongShader = shaderBlur;
    int currentFBO = 0;
    for (int i = 0; i < numPasses; i++) {
      // Pass 2: n-pass ping pong blur
      glUniform1i(glGetUniformLocation(pingpongShader->pu, "horizontal"), i%2 ? true : false);
      bbr->renderTexure(fb[currentFBO].texture, fb[1-currentFBO].framebuffer, pingpongShader);
      currentFBO = 1-currentFBO;
    }
    bbr->renderTexure(fb[currentFBO].texture, 0, pingpongShader);
  }

  void UGLMultipassRender::handleKeyboard(int key, int action) {
    switch (key) {
      case GLFW_KEY_KP_ADD:
      {
        numPasses+=2;
        std::cout << "numPasses = " << numPasses << '\n';
      }
      break;
      case GLFW_KEY_KP_SUBTRACT:
      {
        numPasses-=2;
        if (numPasses < 0) numPasses = 0;
        std::cout << "numPasses = " << numPasses << '\n';
      }
      break;
    }
  }
} /* UGL */
