#include "UGLShadowScene.h"
namespace Shadow {
  Plane::Plane(UGLTextureUnit texture)
  : mTexture(texture)
  {

  }

  void Plane::setup() {
    setupVertex(0, VERTEX_SQUARE_POS);
    setupVertex(1, VERTEX_SQUARE_TEX);
    setupVertex(2, VERTEX_SQUARE_NORM);
  }

  void Plane::setupAdditionalTextures(OGLShader &shader) {
    mTexture.BindTexture(GL_TEXTURE0, shader.pu, "image");
  }

  void Plane::defineAdditionalUniforms(OGLShader &shader) {
    if (mSceneDelegate) {
      mSceneDelegate->defineAdditionalUniforms(shader);
    }
  }



  Cube::Cube(UGLTextureUnit texture)
  : mTexture(texture)
  {

  }

  void Cube::setup() {
    setupVertex(0, VERTEX_CUBE_POS);
    setupVertex(1, VERTEX_CUBE_TEX);
    setupVertex(2, VERTEX_CUBE_NORM);
  }

  void Cube::setupAdditionalTextures(OGLShader &shader) {
    mTexture.BindTexture(GL_TEXTURE0, shader.pu, "image");
  }

  void Cube::defineAdditionalUniforms(OGLShader &shader) {
    if (mSceneDelegate) {
      mSceneDelegate->defineAdditionalUniforms(shader);
    }
  }
} /* Shadow */
