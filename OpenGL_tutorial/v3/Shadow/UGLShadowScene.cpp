#include "UGLShadowScene.h"
#include <vector>

namespace UGL {

  Shadow::Plane *plane;
  OGLShader *planeShader;
  Shadow::Cube *cube;

  struct CubeData {
    gvec3 mPos;
    gvec3 mAxis;
    GLfloat mRotationRad;
    CubeData(gvec3 pos, gvec3 axis, GLfloat rotation)
    : mPos(pos), mAxis(axis), mRotationRad(glm::radians(rotation))
    {}
  };
  std::vector<CubeData> cubeData;

  UGLShadowScene::UGLShadowScene() {
    plane = new Shadow::Plane(UGLTextureUnit("texture/plank.jpg", UGLTextureUnit::RGB));
    planeShader = new OGLShader("v3/Shadow/shader.vs", "v3/Shadow/shader.fs");

    cube = new Shadow::Cube(UGLTextureUnit("texture/container2.png", UGLTextureUnit::RGB));
  }

  void UGLShadowScene::setup() {
    plane->setup();
    cube->setup();
    plane->mSceneDelegate = this;
    cube->mSceneDelegate = this;


    cubeData.push_back(CubeData(gvec3(0, .6, 0), gvec3(0, 1, 0), 30));
    cubeData.push_back(CubeData(gvec3(2, 1, 2), gvec3(1, 1, 0), 40));

    CAM::instance()->moveUp(1);
  }

  void UGLShadowScene::render() {
    plane->m = glm::rotate( glm::scale(id, gvec3(UNI3(4)))
                          , glm::radians(90.0f)
                          , gvec3(1,0,0)
                          );
    plane->render(*planeShader);

    for (int i = 0; i < cubeData.size(); i++) {
      cube->m = glm::scale(id, gvec3(UNI3(1.0/4)));
      cube->m = glm::translate(cube->m, cubeData[i].mPos);
      cube->m = glm::rotate(cube->m, cubeData[i].mRotationRad + UGLCounter::get().angle(), cubeData[i].mAxis);
      cube->render(*planeShader);
    }
  }

  void UGLShadowScene::defineAdditionalUniforms(OGLShader &shader) {
    // Camera
    gvec3 cam = CAM::instance()->getCameraAt();
    glUniform3fv(glGetUniformLocation(shader.pu, "cameraPos"), 1, glm::value_ptr(cam));

    // Material
    glUniform3f(glGetUniformLocation(shader.pu, "material.ambient"), UNI3(.3));
    glUniform3f(glGetUniformLocation(shader.pu, "material.diffuse"), UNI3(1));
    glUniform3f(glGetUniformLocation(shader.pu, "material.specular"), UNI3(.1));
    glUniform1f(glGetUniformLocation(shader.pu, "material.shininess"), 32);

    // Light
    glUniform3f(glGetUniformLocation(shader.pu, "light.ambient"), UNI3(1));
    glUniform3f(glGetUniformLocation(shader.pu, "light.diffuse"), UNI3(1));
    glUniform3f(glGetUniformLocation(shader.pu, "light.specular"), UNI3(.1));
    glUniform3f(glGetUniformLocation(shader.pu, "light.direction"), -1, -1, 0);

  }
} /* UGL */
