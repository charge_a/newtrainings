#pragma once
#include "../UGLImpl.h"

namespace Shadow {

  using namespace UGL;

  class Plane : public UGLBaseRenderer {
    UGLTextureUnit mTexture;
  public:
    UGLBaseRenderer* mSceneDelegate;
    Plane (UGLTextureUnit texture);
    void setup();
    void defineAdditionalUniforms(OGLShader &shader);
    void setupAdditionalTextures(OGLShader &shader);

  };

  class Cube : public UGLBaseRenderer {
    UGLTextureUnit mTexture;
  public:
    UGLBaseRenderer* mSceneDelegate;
    Cube (UGLTextureUnit texture);
    void setup();
    void defineAdditionalUniforms(OGLShader &shader);
    void setupAdditionalTextures(OGLShader &shader);

  };

} /* Shadow */
