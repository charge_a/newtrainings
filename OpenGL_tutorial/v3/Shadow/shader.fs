#version 330 core

in vec3 Normal;
in vec3 FragPosWorld;
in vec2 TexCoords;
out vec4 color;

///////////////////////////////////////////////////////
//  Directional Lighting Fragment Shader
///////////////////////////////////////////////////////

// Structs
struct Material
{
  vec3 ambient;
  vec3 diffuse;
  vec3 specular;
  float     shininess;
};
struct DirectionalLight
{
  vec3 ambient;
  vec3 diffuse;
  vec3 specular;
  vec3 direction;
};

// Functions
vec3 computeDirectionalLight (DirectionalLight light,
                              Material fragMaterial,
                              vec3 fragNormal,
                              vec3 viewDirection)
{
  vec3 ambient = fragMaterial.ambient * light.ambient;
  vec3 diffuse = fragMaterial.diffuse * light.diffuse;
  vec3 specular = fragMaterial.specular * light.specular;

  vec3 lightDirection = -normalize(light.direction);
  float diffAmount = max(0, dot(fragNormal, lightDirection));

  vec3 reflectDir = reflect(-lightDirection, fragNormal);
  float specAmount = max(0, dot(viewDirection, reflectDir));
  float specIntensity = pow(specAmount, fragMaterial.shininess);

  vec3 result = (ambient) + (diffuse * diffAmount) + (specular * specIntensity);
  return result;
}
///////////////////////////////////////////////////////
// End of Directional Lighting Fragment Shader
///////////////////////////////////////////////////////

uniform vec3 cameraPos;
uniform Material material;
uniform DirectionalLight light;
uniform sampler2D image;

void main()
{
  vec3 fragNormal = normalize(Normal);
  vec3 viewDirection = normalize(cameraPos - FragPosWorld);
  vec3 result = computeDirectionalLight(light, material, fragNormal, viewDirection);

  color = texture(image, TexCoords) * vec4(result, 1);
  // color = vec4(fragNormal, 1);
}
