#version 330 core
layout(location=0) in vec3 pos;
layout(location=1) in vec2 tex;
layout(location=2) in vec3 normal;

out vec2 TexCoords;
out vec3 Normal;
out vec3 FragPosWorld;

uniform mat4 m, v, p;

void main()
{
  gl_Position = p*v*m*vec4(pos, 1);
  TexCoords = tex;
  Normal = mat3(transpose(inverse(m))) * normal;
  FragPosWorld = vec3(m * vec4(pos, 1.0));
}
