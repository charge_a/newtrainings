#version 300 es

in highp vec2 Tex;
out highp vec4 color;

uniform sampler2D image;

void main()
{
  color = (texture(image, Tex)); // to prevent negative pos for color.
}
