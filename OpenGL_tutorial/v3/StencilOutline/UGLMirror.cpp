#include "../UGLImpl.h"

namespace UGL {

  const GLfloat SCENE_SCALE = 0.25;

  /**
   * A private class for drawing Mirror Surface/Floor.
   */
  class UGLSurface : public UGLBaseRenderer {
    OGLShader shader;
  public:
    UGLSurface ()
    : shader("v3/StencilOutline/poscolor.vsh", "v3/StencilOutline/boxcolor.fsh")
    {

    }
    void setup () {
      setupVertex(0, VERTEX_SQUARE_POS);
    }
    void render () {
      m = id;
      m = glm::scale(m, glm::vec3(UNI3(4*SCENE_SCALE)));
      m = glm::rotate(m, glm::radians(90.0f), glm::vec3(1,0,0));
      UGLBaseRenderer::render(shader);
    }
  } *surface;

  /**
   * Stencil based mirror implementation
   */
  UGLMirror::UGLMirror(OGLShader _boxShader, OGLShader _mirrorShader)
  : boxShader(_boxShader)
  , mirrorShader(_mirrorShader)
  {
    surface = new UGLSurface();
    CAM::instance()->setCamaraAt(glm::vec3(0, 1, 1));
  }

  UGLMirror::UGLMirror()
  : UGLMirror(
      OGLShader("v3/StencilOutline/poscolor.vsh", "v3/StencilOutline/boxcolor.fsh"),
      OGLShader("v3/StencilOutline/poscolor.vsh", "v3/StencilOutline/stencilMirror.fsh")
    )
  {

  }

  /**
   * Initialise a CUBE vbo and a surface object for mirror.
   */
  void UGLMirror::setup() {
    setupVertex(0, VERTEX_CUBE_POS);
    surface->setup();
    // sfail, dpfail, dppass
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
  }

  /**
   * Render stencil based mirror
   *
   * glDepthMask:
   *    Enable/Disable writing into Depth buffer.
   *
   * glStencilFunc:
   *    Set Front and Back function and reference value for stencil test.
   *    A fragment passes stencil test if STENCIL_BUFFER[frag] vs ref passes function.
   *    ref: [0, 2^bits - 1]
   *    E.g.: glStencilFunc(GL_GEQUAL, 2, 0xFF)
   *          A frag passes stencil test if (STENCIL_BUFFER[frag] & 0xFF)
   *                                        >=
   *                                        (2 & 0xFF)
   *
   * glStencilOp:
   *    Defines Action to take if stencil test fails, depth fails or both pass.
   *    GL_KEEP (Default): STENCIL_BUFFER[frag] is unchanged.
   *    GL_REPLACE: STENCIL_BUFFER[frag] = ref value of glStencilFunc.
   *
   * glStencilMask:
   *    An 8-bit stencil buffer has 8 planes.
   *    This mask specifies what goes into each plane. (Default: 0xFF)
   *    v & mask = value that is written.
   *
   */
  void UGLMirror::render() {

    // 1. Draw Scene with Depth mask enabled. Normal execution
    m = id;
    m = glm::translate(m, glm::vec3(0, .6, 0)*SCENE_SCALE);
    m = glm::scale(m, glm::vec3(1, 1, 1)*SCENE_SCALE);
    m = glm::rotate(m, UGLCounter::get().angle(), glm::vec3(0, 1, 0));
    UGLBaseRenderer::render(boxShader);

    glEnable(GL_STENCIL_TEST);

      // 2. Draw floor with Depth mask updation disabled.
      glDepthMask(GL_FALSE); // so that surface doesn't occulde later rendering.
      glClear(GL_STENCIL_BUFFER_BIT); // remove previous stencil caused by 1st drawing.
        glStencilFunc(GL_ALWAYS, 1, 0xFF);
        glStencilMask(0xFF); // write 1 wherever floor appears.
        surface->render();
      glDepthMask(GL_TRUE);

      // 3. Draw Reflection with Depth mask enabled.
      m = id;
      m = glm::translate(m, glm::vec3(0, -.6, 0)*SCENE_SCALE);
      m = glm::scale(m, glm::vec3(1, -1, 1)*SCENE_SCALE);
      m = glm::rotate(m, UGLCounter::get().angle(), glm::vec3(0, 1, 0));
      glStencilFunc(GL_EQUAL, 1, 0xFF); // Now, accept wherever 1 is written
      glStencilMask(0x00); // without modifying stencil buffer.
      UGLBaseRenderer::render(mirrorShader);

    glDisable(GL_STENCIL_TEST);
    glStencilMask(0xFF); // Enable Stencil buffer
  }
} /* UGL */
