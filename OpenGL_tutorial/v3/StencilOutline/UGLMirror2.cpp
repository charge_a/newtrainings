#include "../UGLImpl.h"

namespace UGL {

  UGLMirror2::UGLMirror2()
  : UGLMirror(
      OGLShader("v3/StencilOutline/texcolor.vsh", "v3/StencilOutline/TexBoxcolor.fsh"),
      OGLShader("v3/StencilOutline/texcolor.vsh", "v3/StencilOutline/TexStencilMirror.fsh")
    )
  , img("texture/face.jpg", UGLTextureUnit::Channels::RGB)
  {
  }

  void UGLMirror2::setup() {
    UGLMirror::setup();
    setupVertex(1, VERTEX_CUBE_TEX);
  }

  void UGLMirror2::render() {
    img.BindTexture(GL_TEXTURE0, boxShader.pu, "image");
    img.BindTexture(GL_TEXTURE0, mirrorShader.pu, "image");
    UGLMirror::render();
  }

} /* UGL */
