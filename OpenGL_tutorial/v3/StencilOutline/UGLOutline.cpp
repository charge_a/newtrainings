#include "../UGLImpl.h"
#include <iostream>

namespace UGL {
  UGLOutline::UGLOutline ()
  : boxShader("v3/StencilOutline/poscolor.vsh", "v3/StencilOutline/boxcolor.fsh")
  , outlineShader("v3/StencilOutline/poscolor.vsh", "v3/StencilOutline/outline.fsh")
  {

  }

  void UGLOutline::setup() {
    setupVertex(0, VERTEX_CUBE_POS);
    glEnable(GL_DEPTH_TEST);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
  }

  void UGLOutline::render() {
    glClear(GL_STENCIL_BUFFER_BIT);

    glStencilFunc(GL_ALWAYS, 1, 0xFF); // render all fragments for which refvalue&mask is ALWAYS true
    glStencilMask(0xFF); // all values are ANDed with mask before being written. 0xFF enables all bits to go as it is.
    m = glm::scale(id, glm::vec3(UNI3(0.5)));
    UGLBaseRenderer::render(boxShader);

    glStencilFunc(GL_NOTEQUAL, 1, 0xFF); // render all fragments where stencil buffer is NOT EQUAL to 1.
    glStencilMask(0x00); // don't update stencil buffer when rendering fragments of scaled up object.
    glDisable(GL_DEPTH_TEST);
    m = glm::scale(id, glm::vec3(UNI3(0.7)));
    UGLBaseRenderer::render(outlineShader);

    glStencilMask(0xFF);
    glEnable(GL_DEPTH_TEST);
  }
} /* UGL */
