#version 300 es

in highp vec4 Pos;
out highp vec4 color;

void main()
{
  color = (Pos + vec4(.5, .5, .5, 0)); // to prevent negative pos for color.
}
