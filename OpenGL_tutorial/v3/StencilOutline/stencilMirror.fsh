#version 300 es

in highp vec4 Pos;
out highp vec4 color;

void main()
{
  color = Pos;
  color *= 0.8; // to darken the reflection a bit.
}
