#version 300 es

layout (location=0) in vec3 position;
layout (location=1) in vec2 tex;

uniform mat4 m, v, p;

out vec2 Tex;

void main()
{
  gl_Position = p*v*m*vec4(position.x, -position.y, position.z, 1.0);
  Tex = tex;
}
