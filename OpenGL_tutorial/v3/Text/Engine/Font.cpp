#include "Font.h"
#include <iostream>

Font::Font() {}

Font::Font(char const * fontSrc)
{
  FT_Library ft;
  if (FT_Init_FreeType(&ft)) {
    std::cerr << "Error: Freetype: Couldn't init freetype" << '\n';
  }

  FT_Face face;
  if (FT_New_Face(ft, fontSrc
                  , 0, &face)) {
    std::cerr << "Error: Freetype: Failed to load font" << '\n';
  }

  FT_Set_Pixel_Sizes(face, 0, 48);
  // FT_Set_Char_Size(face, 50 * 64, 0, 100, 0);

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  for (GLubyte c = 0; c < 128; c++)
  {
    if (FT_Load_Char(face, c, FT_LOAD_RENDER)) {
      std::cerr << "Error: Freetype: failed to load glyph " << c << '\n';
      continue;
    }

    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, face->glyph->bitmap.width, face->glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    mCharMap[c] = {
      texture,
      glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
      glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
      face->glyph->advance.x
    };
  }

  FT_Done_Face(face);
  FT_Done_FreeType(ft);

}

Font::Font(const Font & font)
{
  mCharMap = font.mCharMap;
}

int Font::getOriginX(GLchar c)
{
  Character &ch = mCharMap[c];
  return ch.Bearing.x;
}
int Font::getOriginY(GLchar c)
{
  Character &ch = mCharMap[c];
  return ch.Bearing.y - ch.Size.y;
}
int Font::getWidth(GLchar c)
{
  Character &ch = mCharMap[c];
  return ch.Size.x;
}
int Font::getHeight(GLchar c)
{
  Character &ch = mCharMap[c];
  return ch.Size.y;
}
int Font::getAdvance(GLchar c)
{
  Character &ch = mCharMap[c];
  return ch.Advance >> 6;

}
GLuint Font::getTexture(GLchar c)
{
  Character &ch = mCharMap[c];
  return ch.TextureID;
}
GLfloat Font::computeTextLength(std::string text, float scale)
{
  float width = 0;
  for (std::string::const_iterator c = text.begin(); c != text.end(); c++)
  {
    GLfloat w = getAdvance(*c) * scale;
    width += w;
  }
  return width;
}
