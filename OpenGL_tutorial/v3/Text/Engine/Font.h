#pragma once
#include <ft2build.h>
#include FT_FREETYPE_H
#include <map>
#include "../../UGLImpl.h"
#include "../../Basetypes.h"

struct Character {
  GLuint  TextureID;
  glm::ivec2 Size;
  glm::ivec2 Bearing;
  GLuint  Advance;
};

class Font {
private:
  std::map<GLchar, Character> mCharMap;


public:
  Font ();
  Font (char const * fontSrc);
  Font (const Font & font);
  int getOriginX(GLchar c);
  int getOriginY(GLchar c);
  int getWidth(GLchar c);
  int getHeight(GLchar c);
  int getAdvance(GLchar c);
  GLuint getTexture(GLchar c);
  GLfloat computeTextLength(std::string text, float scale);
};

#define FONT_FACTORY(fontName, fontSrc) \
class FontFactory##fontName {                 \
public:                                       \
  static Font & getFont() {                   \
    static Font * sFont = NULL;               \
    if (sFont == NULL)                        \
      sFont = new Font(fontSrc);              \
    return *sFont;                            \
  }                                           \
};

FONT_FACTORY(Mono, "/usr/share/fonts/truetype/freefont/FreeMono.ttf");
FONT_FACTORY(Sans, "/usr/share/fonts/truetype/freefont/FreeSans.ttf");
FONT_FACTORY(Purisa, "/usr/share/fonts/truetype/tlwg/Purisa.ttf");
FONT_FACTORY(UbuntuL, "/usr/share/fonts/truetype/ubuntu-font-family/Ubuntu-L.ttf");
FONT_FACTORY(NotoSansCJKThin, "/usr/share/fonts/opentype/noto/NotoSansCJK-Thin.ttc");
