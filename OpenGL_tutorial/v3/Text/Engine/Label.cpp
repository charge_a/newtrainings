#include "Label.h"
#include "../../UGLImpl.h"

class LabelRenderer {
private:
  GLuint mVAO;
  GLuint mVBO;
  OGLShader mShader;
  LabelRenderer ()
  : mShader("v3/Text/Engine/shader.vs","v3/Text/Engine/shader.fs")
  {
    glGenVertexArrays(1, &mVAO);
    glBindVertexArray(mVAO);
    glGenBuffers(1, &mVBO);
    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*6*4, NULL, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4*sizeof(GLfloat), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
  }

public:
  static LabelRenderer* instance()
  {
    static LabelRenderer* sInstance = NULL;
    if (sInstance == NULL) {
      sInstance = new LabelRenderer();
    }
    return sInstance;
  }

  void render(std::string text, Bounds b, float scale, Font &font, float3D fontColor) {
    mShader.Use();
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(mVAO);
    // defineAdditionalUniforms(*mShader);
    glUniform3f(glGetUniformLocation(mShader.pu, "textColor"), fontColor.x, fontColor.y, fontColor.z);
    GLint xywh[4];
    glGetIntegerv(GL_VIEWPORT, xywh);
    glm::mat4 ortho = glm::ortho(0.0f, (float)xywh[2], 0.0f, (float)xywh[3]);
    glUniformMatrix4fv(glGetUniformLocation(mShader.pu, "ortho"), 1, GL_FALSE, glm::value_ptr(ortho));
    glm::mat4 m, id;
    // m = glm::rotate(id, UGL::UGLCounter::get().angle(), float3D(0, 1, 0));
    glm::mat4 p = glm::perspective(45.0f, (float)xywh[2] / (float)xywh[3], 0.1f, 100.0f);
    glm::mat4 v = CAM::instance()->getViewMatrix();
    glUniformMatrix4fv(glGetUniformLocation(mShader.pu, "m"), 1, GL_FALSE, glm::value_ptr(m));
    glUniformMatrix4fv(glGetUniformLocation(mShader.pu, "p"), 1, GL_FALSE, glm::value_ptr(p));
    glUniformMatrix4fv(glGetUniformLocation(mShader.pu, "v"), 1, GL_FALSE, glm::value_ptr(v));

    float offset[2] = {b.w-font.computeTextLength(text, scale)/2, b.h};
    float x = 2*b.x - 1 + offset[0];
    float y = 2*b.y - 1 + offset[1];
    for (std::string::const_iterator c = text.begin(); c != text.end(); c++)
    {
      GLfloat xpos = x + font.getOriginX(*c) * scale;
      GLfloat ypos = y + font.getOriginY(*c) * scale;

      GLfloat w = font.getWidth(*c) * scale;
      GLfloat h = font.getHeight(*c) * scale;
      // Update VBO for each character
      GLfloat vertices[6][4] = {
          { xpos,     ypos + h,   0.0, 0.0 },
          { xpos,     ypos,       0.0, 1.0 },
          { xpos + w, ypos,       1.0, 1.0 },

          { xpos,     ypos + h,   0.0, 0.0 },
          { xpos + w, ypos,       1.0, 1.0 },
          { xpos + w, ypos + h,   1.0, 0.0 }
      };
      // Render glyph texture over quad
      glBindTexture(GL_TEXTURE_2D, font.getTexture(*c));
      // Update content of VBO memory
      glBindBuffer(GL_ARRAY_BUFFER, mVBO);
      glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
      glBindBuffer(GL_ARRAY_BUFFER, 0);

      glDrawArrays(GL_TRIANGLES, 0, 6);

      x += font.getAdvance(*c) * scale;
    }
    glBindVertexArray(0);

  }
};

Label::Label (Bounds b)
:mBounds(b)
{
  text = "";
  font = FontFactoryUbuntuL::getFont();
  fontSize = .001;
  // fontSize = 1.0;
  fontColor = float3D(0,1,0);
}

void Label::renderInBounds(const Bounds &b)
{
  // mBounds.h = 48*fontSize;
  // Rectangle rect(Bounds(mBounds.x, mBounds.y, mBounds.w, mBounds.h));
  // rect.color = float4D(1, 0, 0, .5);
  // glDisable(GL_DEPTH_TEST);
  // rect.render();
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  LabelRenderer::instance()->render(text, mBounds.InBounds(b), fontSize, font, fontColor);
  // glEnable(GL_DEPTH_TEST);
}

void Label::render() {
  renderInBounds(BOUNDS_FULL);
}
