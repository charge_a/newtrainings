#pragma once
#include "Font.h"
#include "Rectangle.h"
#include <string>

class Label {
public:
  Bounds mBounds;
  Label (Bounds b);
  void render();
  void renderInBounds(const Bounds &b);
  std::string text;
  Font font;
  float fontSize;
  float3D fontColor;
};
