#include "Line.h"
#include <vector>

namespace UGL {
  class LineRenderer : public UGLBaseRenderer {
  private:
    OGLShader mShader;

  public:
    LineRenderer ()
    : mShader("v3/Text/Engine/rect.vs","v3/Text/Engine/rect.fs")
    {
      mColor = float4D(0,1,0,1);
      mLineWidth = 1;
    }

    void setup() {
      UGLVertex v(&mVertices[0], sizeof(float)*mVertices.size(), 3);
      setupVertex(0, v);
      vertex_geometry = GL_LINES;
    }

    void render() {
      glLineWidth(mLineWidth);
      UGLBaseRenderer::render(mShader);
    }

    void defineAdditionalUniforms(OGLShader& shader) {
      glUniform4f(glGetUniformLocation(shader.pu, "vColor"),
                  mColor.x, mColor.y, mColor.z, mColor.w);

    }

    std::vector<float> mVertices;
    float4D mColor;
    float mLineWidth;
  };
} /* UGL */

LineBuilder::LineBuilder()
{
  mRenderer = new UGL::LineRenderer();
  mLineColor = float4D(0,1,0,1);
  mLineWidth = 1;
}

LineBuilder& LineBuilder::addLine(float x, float y)
{
  mRenderer->mVertices.push_back(x);
  mRenderer->mVertices.push_back(y);
  mRenderer->mVertices.push_back(0);
  return *this;
}

void LineBuilder::setup() {
  mRenderer->mColor = mLineColor;
  mRenderer->mLineWidth = mLineWidth;
  mRenderer->setup();
}

void LineBuilder::render() {
  mRenderer->render();
}
