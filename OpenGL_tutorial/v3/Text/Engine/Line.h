#pragma once
#include "../../Basetypes.h"

namespace UGL {
  class LineRenderer;
} /* UGL */
class LineBuilder {
  UGL::LineRenderer* mRenderer;
public:
  LineBuilder ();
  LineBuilder& addLine(float x, float y);
  void setup();
  void render();

  float4D mLineColor;
  float mLineWidth;
};
