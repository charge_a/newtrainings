#include "Rectangle.h"

namespace UGL {
  class RectangleRenderer : public UGLBaseRenderer {
  private:
    OGLShader mShader;
    RectangleRenderer ()
    : mShader("v3/Text/Engine/rect.vs","v3/Text/Engine/rect.fs")
    {}

  public:

    static RectangleRenderer & instance()
    {
      static RectangleRenderer * sInstance = NULL;
      if (sInstance == NULL) {
        sInstance = new RectangleRenderer();
        sInstance->setup();
      }
      return *sInstance;
    }

    void setup() {
      setupVertex(0, VERTEX_SQUARE_POS);
    }

    void render(Bounds b) {
      m = id;
      m = glm::translate(m, float3D(2*b.x + b.w - 1.0, 2*b.y + b.h - 1.0, 0));
      m = glm::scale(m, float3D(2*b.w, 2*b.h, 1.0));
      UGLBaseRenderer::render(mShader);
    }

    void defineAdditionalUniforms(OGLShader &shader) {
      glUniform4f(glGetUniformLocation(shader.pu, "vColor"),
                  mColor.x, mColor.y, mColor.z, mColor.w);

    }
    float4D mColor;
  };
} /* UGL */

void Rectangle::render() {
  renderInBounds(BOUNDS_FULL);
}

void Rectangle::renderInBounds(const Bounds &b) {
  UGL::RectangleRenderer & r = UGL::RectangleRenderer::instance();
  r.mColor = color;
  r.render(mBounds.InBounds(b));
}
