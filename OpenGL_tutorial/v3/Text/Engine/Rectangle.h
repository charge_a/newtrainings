#pragma once
#include "../../Basetypes.h"

class Rectangle {
private:
  Bounds mBounds;

public:
  Rectangle (Bounds rect)
  : mBounds(rect) {}
  void render();
  void renderInBounds(const Bounds & b);

  float4D color;
};
