#include "Button.h"

namespace UI {
  Button::Button()
  : Element("Button"), mLabel(BOUNDS_ZERO), mBackground(BOUNDS_ZERO)
  {
    mBounds = BOUNDS_ZERO;
  }

  Button::Button (std::string name, Bounds bounds)
  : Element(name), mLabel(bounds), mBackground(bounds)
  {
    mBounds = bounds;
  }

  void Button::renderInBounds(Bounds absoluteBounds) {
    mBackground.renderInBounds(absoluteBounds);
    mLabel.renderInBounds(absoluteBounds);
  }
} /* UI */
