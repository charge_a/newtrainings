#pragma once

#include "Element.h"
namespace UI {
  class Button : public Element {
  public:
    Button ();
    Button (std::string name, Bounds bounds);

    virtual void renderInBounds(Bounds absoluteBounds);

    Label mLabel;
    Rectangle mBackground;
  };
} /* UI */
