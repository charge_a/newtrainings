#include "Element.h"

namespace UI {
  Element::Element(std::string name)
  : mBounds(0,0,0,0), mContainer(NULL), mVisible(true), mOnClick(NULL), mName(name)
  {}

  Element::Element(const Element & element)
  : mBounds(element.mBounds), mContainer(element.mContainer), mVisible(element.mVisible)
  , mOnClick(NULL), mName(element.mName)
  {}




  bool Element::visible()
  {
    if (mContainer) {
      return mContainer->visible() && mVisible;
    }
    return mVisible;
  }

  void Element::addChildElement(UI::Element *element) {
    mSubviews.push_back(element);
    element->mContainer = this;
  }

  void Element::renderInBounds(Bounds absoluteBounds) {

  }

  void Element::renderAll(Bounds absoluteBounds) {
    renderInBounds(absoluteBounds);
    for (Element* elem: mSubviews)
    {
      elem->renderAll(mBounds.InBounds(absoluteBounds));
    }
  }

  void Element::onClick(onClickHandler handler) {
    mOnClick = handler;
  }

  Element* Element::elementForPoint(float x, float y, Bounds absoluteBounds) {
    Bounds b = mBounds.InBounds(absoluteBounds);
    if (b.ContainsPoint(x, y)) {
      for (Element* elem : mSubviews) {
        Element* e = elem->elementForPoint(x, y, b);
        if (e) {
          return e;
        }
      }
      return this;
    }
    return NULL;
  }
} /* UI */
