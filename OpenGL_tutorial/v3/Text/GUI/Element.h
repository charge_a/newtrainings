#pragma once
#include "../Engine/Font.h"
#include "../Engine/Label.h"
#include "../Engine/Rectangle.h"
#include <vector>

#include "IGUIEvents.h"

namespace UI {


  class Element {
  private:


  public:
    typedef void (*onClickHandler)(IGUIEvents*, Element*);
    Element (std::string name);
    Element (const Element& element);
    bool visible();
    void addChildElement(Element* element);
    virtual void renderInBounds(Bounds absoluteBounds);
    void renderAll(Bounds absoluteBounds);
    void onClick(onClickHandler handler);
    Element* elementForPoint(float x, float y, Bounds absoluteBounds);

    Element* mContainer;
    Bounds mBounds;
    bool mVisible;
    int mID;
    std::vector<Element*> mSubviews;
    std::string mName;

    // Events
    onClickHandler mOnClick;
  };
} /* UI */
