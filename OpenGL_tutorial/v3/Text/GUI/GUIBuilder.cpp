#include "GUIBuilder.h"
#include "Button.h"
#include "View.h"
#include "ScrollView.h"

namespace UI {

  GUIBuilder::GUIBuilder(IGUIEvents* controller)
  : mController(controller)
  {
    float topBarHeight = 0.05;
    float topBarCntrlWidth = 0.1;
    float bottomBarHeight = 0.1;

    Element* root = new Element("e_root");
    root->mBounds = BOUNDS_FULL;
    View* topView = new View("e_topview", Bounds(0, 1.0 - topBarHeight, 1.0, topBarHeight));
    topView->mBackground.color = float4D(1,1,1,0.1);
    {
      Button* button = new Button("e_btn_open", Bounds(0.0, 0.0, topBarCntrlWidth, 1.0));
      button->mBackground.color = float4D(UNI3(0.5),1);
      button->mLabel.text = "Open";
      button->onClick(&UICallback::onOpenClick);
      topView->addChildElement(button);
    }
    {
      Button* button = new Button("e_btn_close", Bounds(1.0 - topBarCntrlWidth, 0.0, topBarCntrlWidth, 1.0));
      button->mBackground.color = float4D(UNI3(0.5),1);
      button->mLabel.text = "Close";
      button->onClick(&UICallback::onCloseClick);
      topView->addChildElement(button);
    }

    root->addChildElement(topView);

    View* midView = new View("e_midview", Bounds(0, bottomBarHeight, 1, 1.0 - (topBarHeight + bottomBarHeight)));
    midView->mBackground.color = float4D(0,0,0,0.5);
    {
      ScrollView* sv = new ScrollView("e_sv_left", Bounds(0, 0, 0.5, 1.0));
      sv->mBackground.color = float4D(1,0,0,0.3);
      midView->addChildElement(sv);
    }

    root->addChildElement(midView);

    View* bottomView = new View("e_bottomview", Bounds(0, 0, 1, bottomBarHeight));
    bottomView->mBackground.color = float4D(0,0,0,1);

    // root->addChildElement(bottomView);

    mRoot = root;

  }

  Element* GUIBuilder::getView()
  {
    return mRoot;
  }

  void GUIBuilder::handleMouseClick(double x, double y)
  {
    Element* e = mRoot->elementForPoint(x, y, mRoot->mBounds);
    while (e) {
      // printf("click: at %s\n", e->mName.c_str());
      if (e->mOnClick) e->mOnClick(mController, e);
      e = e->mContainer;
    }
  }
} /* UI */
