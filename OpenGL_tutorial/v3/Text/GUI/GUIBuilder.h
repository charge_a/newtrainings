#pragma once
#include "Element.h"

namespace UI {
  class GUIBuilder {
  public:
    GUIBuilder(IGUIEvents* controller);
    Element* getView();
    void handleMouseClick(double x, double y);


  private:
    IGUIEvents* mController;
    Element* mRoot;
  };
} /* UI */
