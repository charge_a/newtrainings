#include "IGUIEvents.h"
#include "Element.h"
#include "Button.h"
#include "ScrollView.h"

namespace UI {
  namespace UICallback {
    void onOpenClick(IGUIEvents* controller, Element* sender)
    {
      if (dynamic_cast<Button*>(sender)) {
        printf("text = %s\n", dynamic_cast<Button*>(sender)->mLabel.text.c_str());
      }
      controller->onOpenClick();
    }

    void onCloseClick(IGUIEvents* controller, Element* sender) {
      controller->onCloseClick();
    }

    void onScrollUpClick(IGUIEvents* controller, Element* sender) {
      ScrollView* sv = dynamic_cast<ScrollView*>(sender->mContainer);
      if (sv) {
        sv->onScrollUpClick();
      }
    }

    void onScrollDownClick(IGUIEvents* controller, Element* sender) {
      ScrollView* sv = dynamic_cast<ScrollView*>(sender->mContainer);
      if (sv) {
        sv->onScrollDownClick();
      }
    }
  } /* UICallback */

} /* UI */
