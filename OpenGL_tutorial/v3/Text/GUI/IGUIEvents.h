#pragma once
namespace UI {
  class Element;

  class IGUIEvents {
  public:
    virtual void onOpenClick() = 0;
    virtual void onCloseClick() = 0;
  };

  namespace UICallback {
    void onOpenClick(IGUIEvents* controller, Element* sender);
    void onCloseClick(IGUIEvents* controller, Element* sender);
    void onScrollUpClick(IGUIEvents* controller, Element* sender);
    void onScrollDownClick(IGUIEvents* controller, Element* sender);
  } /* UICallback */
} /* UI */
