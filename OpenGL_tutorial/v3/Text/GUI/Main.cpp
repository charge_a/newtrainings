#include "Main.h"

namespace UI {
  UIRenderer::UIRenderer() {}

  void UIRenderer::setup() {

    mGUIBuilder = new GUIBuilder(this);
    mRoot = mGUIBuilder->getView();
  }

  void UIRenderer::render() {
    glDisable(GL_DEPTH_TEST);
    mRoot->renderAll(mRoot->mBounds);
    glEnable(GL_DEPTH_TEST);
  }

  void UIRenderer::handleKeyboard(int key, int action) {
  }

  void UIRenderer::handleMouseMove(double xpos, double ypos) {
  }

  void UIRenderer::handleMouseClick(double xpos, double ypos) {
    mGUIBuilder->handleMouseClick(xpos, ypos);
  }

  // IGUIEvents
  void UIRenderer::onOpenClick() {
    printf("Open clicked\n");
  }

  void UIRenderer::onCloseClick() {
    glfwSetWindowShouldClose(GLGlobalInstances::instance().getWindow(), GL_TRUE);
  }
} /* UI */
