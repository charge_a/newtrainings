#include "GUIBuilder.h"

namespace UI {

  class UIRenderer : public IGUIEvents {

    Element* mRoot;
    GUIBuilder* mGUIBuilder;
  public:
    UIRenderer ();
    void setup();
    void render();
    void handleKeyboard(int key, int action);
    void handleMouseMove(double xpos, double ypos);
    void handleMouseClick(double xpos, double ypos);

    // IGUIEvents
    void onOpenClick();
    void onCloseClick();
  };

} /* UI */
