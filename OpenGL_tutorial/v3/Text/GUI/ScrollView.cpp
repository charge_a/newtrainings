#include "ScrollView.h"
#include "IGUIEvents.h"
namespace UI {
  ScrollView::ScrollView(std::string name, Bounds b): View(name, b) {
    float w = 0.1, h = 0.05;
    mScrollUp = Button(name + "_btn_up", Bounds(1.0-w, 1.0-h, w, h));
    mScrollDown = Button(name + "_btn_down", Bounds(1.0 - w, 0, w, h));
    mScrollUp.mBackground.color = float4D(0,1,0,0.5);
    mScrollDown.mBackground.color = float4D(0,1,0,0.5);
    mScrollUp.mLabel.text = "U";
    mScrollDown.mLabel.text = "D";
    mScrollUp.onClick(&UICallback::onScrollUpClick);
    mScrollDown.onClick(&UICallback::onScrollDownClick);
    addChildElement(&mScrollUp);
    addChildElement(&mScrollDown);
  }
  void ScrollView::renderInBounds(Bounds absoluteBounds) {
    View::renderInBounds(absoluteBounds);
  }

  void ScrollView::onScrollUpClick() {
    printf("scroll up\n");
  }

  void ScrollView::onScrollDownClick() {
    printf("scroll down\n");
  }
} /* UI */
