#pragma once
#include "View.h"
#include "Button.h"
namespace UI {
  class ScrollView : public View {
    Button mScrollUp, mScrollDown;
  public:
    ScrollView(): View() {}
    ScrollView(std::string name, Bounds b);
    virtual void renderInBounds(Bounds absoluteBounds);

    void onScrollUpClick();
    void onScrollDownClick();
  };
} /* UI */
