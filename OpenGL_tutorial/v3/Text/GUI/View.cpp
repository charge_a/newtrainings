#include "View.h"
namespace UI {
  View::View() : View("View", BOUNDS_ZERO) {}

  View::View(std::string name, Bounds bounds) : Element(name), mBackground(bounds) {
    mBounds = bounds;
  }

  void View::renderInBounds(Bounds absoluteBounds) {
    mBackground.renderInBounds(absoluteBounds);
  }
} /* UI */
