#pragma once

#include "Element.h"
namespace UI {
  class View : public Element {
  public:
    View ();
    View (std::string name, Bounds bounds);

    virtual void renderInBounds(Bounds absoluteBounds);

    Rectangle mBackground;
  };
} /* UI */
