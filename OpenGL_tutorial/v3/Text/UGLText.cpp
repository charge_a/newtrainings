#include "../UGLImpl.h"
#include "Engine/Label.h"
#include "Engine/Line.h"

namespace UGL {

  struct ExtraElements {
    LineBuilder mLineBuilder;
  };

  UGLText::UGLText()
  {
    mElem = new ExtraElements();
  }

  void UGLText::setup() {
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    CAM::instance()->setCamaraAt(float3D(0,0,1.80));

    for (int i=0; i<10; i++) {
      mElem->mLineBuilder
            .addLine(0, i/20.0).addLine(0.25+i*i/200.0, i/20.0)
      ;
    }
    mElem->mLineBuilder.mLineWidth = 1;
    mElem->mLineBuilder.setup();
  }

  void UGLText::render() {

    // glDisable(GL_DEPTH_TEST);

    float bar = 30.0/getScreenHeight();
    {
      Rectangle r(Bounds(0, 1-bar, 1, bar));
      r.color = float4D(0,0,1,0.8);
      r.render();
    }
    {
      Rectangle r(Bounds(0, bar, 1, 1-2*bar));
      r.color = float4D(0,0,1,0.6);
      r.render();
    }
    {
      Rectangle r(Bounds(0, 0, 1, bar));
      r.color = float4D(0,0,1,0.1);
      r.render();
    }

    Label label(Bounds(0.25, 0.25, 0.5, 0.5));
    label.text = "Hello";
    label.font = FontFactoryUbuntuL::getFont();
    label.fontSize = .001;
    label.fontColor = float3D(0, 1, 0);
    label.render();

    // mElem->mLineBuilder.render();
    // glEnable(GL_DEPTH_TEST);

  }

} /* UGL */
