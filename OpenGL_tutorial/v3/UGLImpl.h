#pragma once
#include "Framework/UGLBase.h"
#include "Framework/UGLConstant.h"

namespace UGL {
  // Sample rendering cube
  class UGLSample : public UGLBaseRenderer {
  public:
    UGLSample ();
    OGLShader shader;
    void setup();
    void render();
  };
  // stencil buffer for outline
  class UGLOutline : public UGLBaseRenderer {
  public:
    UGLOutline ();
    OGLShader boxShader;
    OGLShader outlineShader;
    void setup();
    void render();
  };
  // mirror effect using stencil
  class UGLMirror : public UGLBaseRenderer {

  public:
    UGLMirror ();
    UGLMirror (OGLShader _boxShader, OGLShader _mirrorShader);
    virtual void setup();
    virtual void render();
    OGLShader boxShader;
    OGLShader mirrorShader;
  };
  class UGLMirror2 : public UGLMirror {
  public:
    UGLMirror2 ();
    void setup();
    void render();
    UGLTextureUnit img;
  };

  // Blending scene
  class UGLBlendingScene: public UGLBaseRenderer {
  public:
    UGLBlendingScene();
    void setup();
    void render();
  };

  // Framebuffer application
  class UGLOffScreen : public UGLBaseRenderer {
  public:
    UGLOffScreen (UGLBaseRenderer *mirror);
    void setup();
    void render();
    void renderOriginal();
    void handleKeyboard(int key, int action);

    GLuint fbo;
    GLuint fboTex;
    OGLShader *fboShader;
    UGLBaseRenderer *sceneToRender;
  };

  // Cubemap application
  class UGLSkyBox : public UGLBaseRenderer {
  public:
    UGLSkyBox ();
    void setup();
    void render();
    OGLShader shader;
    UGLTextureUnitCubeMap cmap;
  };

  // environment mapping cubemap application
  class UGLSkyBoxEnvironment : public UGLBaseRenderer {
  public:
    UGLSkyBoxEnvironment ();
    void setup();
    void defineAdditionalUniforms(OGLShader &);
    void render();
    OGLShader shader;
    UGLTextureUnitCubeMap cmap;
  };

  // Geometry Shaders
  class UGLGeometryExample : public UGLBaseRenderer {
  public:
    UGLGeometryExample ();
    void setup();
    void render();
    void handleKeyboard(int key, int action);
  };

  // Instancing Sample
  class UGLInstancingExample :public UGLBaseRenderer {
  public:
    UGLInstancingExample ();
    void setup();
    void render();
  };
  void handleKeyboard(int key, int action);

  // Cube Fractal
  class UGLCubeFractal : public UGLBaseRenderer {
  public:
    UGLCubeFractal ();
    void setup();
    void render();

  };

  // platform scene
  class UGLShadowScene : public UGLBaseRenderer {
  public:
    UGLShadowScene ();
    void setup();
    void render();
    void defineAdditionalUniforms(OGLShader &shader);
  };

  class UGLMultipassRender : public UGLBaseRenderer {
  public:
    int numPasses;
    UGLMultipassRender ();
    void setup();
    void render();
    void handleKeyboard(int key, int action);
  };

  class UGLBillBoard : public UGLBaseRenderer {
  public:
    UGLBillBoard ();
    void setup();
    void render();
    void defineAdditionalUniforms(OGLShader &shader);
  };

  struct UGLVideoSceneObjects;
  class UGLVideoScene : public UGLBaseRenderer {
    UGLVideoSceneObjects* mO;
  public:
    UGLVideoScene ();
    void setup();
    void render();
    void defineAdditionalUniforms(OGLShader &shader);
  };

  class UGLAxis : public UGLBaseRenderer {
    OGLShader mShader;
  public:
    UGLAxis ();
    void setup();
    void render();
  };

  class ExtraElements;
  class UGLText : public UGLBaseRenderer {
    ExtraElements* mElem;
  public:
    UGLText ();
    void setup();
    void render();
  };
} /* UGL */
