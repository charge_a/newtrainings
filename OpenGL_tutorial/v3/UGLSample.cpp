#include "UGLImpl.h"
#include "Framework/UGLConstant.h"

namespace UGL {

  UGLSample::UGLSample()
  :shader("v3/red.vsh", "v3/red.fsh")
  {

  }

  void UGLSample::setup() {
    UGLBaseRenderer::setup();
    setupVertex(0, VERTEX_CUBE_POS);
  }

  void UGLSample::render() {
    UGLBaseRenderer::render(shader);
  }

} /* UGL */
