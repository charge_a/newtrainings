#include "UGLVideo.h"
#include <iostream>

bool UGLVideo::avInitialied = false;

UGLVideo::UGLVideo(std::string path)
{
  if (!avInitialied) {
    av_register_all();
    avInitialied = true;
  }

  loaded = false;

  fmtContext = NULL;
  if(0 != avformat_open_input(&fmtContext, path.c_str(), NULL, NULL)) {
    fprintf(stderr, "AVFormat not able to open %s\n", path.c_str());
    return;
  }
  if (0 != avformat_find_stream_info(fmtContext, NULL)) {
    fprintf(stderr, "AVFormat not able to find stream info.\n");
    return;
  }
  videoStream = -1;
  for (int i = 0; i < fmtContext->nb_streams; ++i) {
    if (AVMEDIA_TYPE_VIDEO == fmtContext->streams[i]->codec->codec_type) {
      videoStream = i;
      break;
    }
  }
  if (videoStream == -1) {
    fprintf(stderr, "No Video stream found\n");
    return;
  }
  codecCtx = fmtContext->streams[videoStream]->codec;
  codec = avcodec_find_decoder(codecCtx->codec_id);
  if (!codec) {
    fprintf(stderr, "Unsupported codec\n");
    return;
  }
  codecCtx1 = codecCtx;
  codecCtx = avcodec_alloc_context3(codec);
  if (avcodec_copy_context(codecCtx, codecCtx1)) {
    fprintf(stderr, "AVCodec couldn't create a copy\n");
    return;
  }
  if (avcodec_open2(codecCtx, codec, NULL) < 0) {
    fprintf(stderr, "AVCodec couldn't open context.\n");
    return;
  }

  frame = av_frame_alloc();
  frameRGB = av_frame_alloc();
  if (!frame || !frameRGB) {
    fprintf(stderr, "AVFrame couldn't be created.\n");
    return;
  }
  mWidth = codecCtx->width;
  mHeight = codecCtx->height;
  mTargetPixelFormat = PIX_FMT_RGB24;

  int bytesRequired = avpicture_get_size(mTargetPixelFormat, mWidth, mHeight);
  bytes = (uint8_t *)av_malloc(bytesRequired * sizeof(uint8_t));
  avpicture_fill((AVPicture *)frameRGB, bytes, mTargetPixelFormat, mWidth, mHeight);

  swsCtx = sws_getContext(mWidth, mHeight, codecCtx->pix_fmt,
                          mWidth, mHeight, mTargetPixelFormat,
                          SWS_BILINEAR, NULL, NULL, NULL);

  if (!swsCtx) {
    fprintf(stderr, "SWS context not got\n");
    return;
  }

  loaded = true;

  av_dump_format(fmtContext, 0, "abra", 0);
}

UGLVideo::~UGLVideo()
{
  av_free(bytes);
  av_free(frameRGB);
  av_free(frame);
  avcodec_close(codecCtx);
  avcodec_close(codecCtx1);
  avformat_close_input(&fmtContext);
}

int64_t UGLVideo::GetDurationMs()
{
  if (fmtContext) {
    return fmtContext->duration;
  }
  return 0;
}
unsigned int UGLVideo::GetSampleCount()
{
  return 0;
}
unsigned int UGLVideo::GetWidth()
{
  return mWidth;
}
unsigned int UGLVideo::GetHeight()
{
  return mHeight;
}

void* UGLVideo::GetBuffer()
{
  return frameRGB->data[0];
}
unsigned int UGLVideo::GetBufferSize()
{
  return frameRGB->linesize[0];
}
int UGLVideo::ReadNextSample()
{
  if (!loaded) {
    return 0;
  }
  while (av_read_frame(fmtContext, &packet) >= 0) {
    if (packet.stream_index == videoStream) {
      avcodec_decode_video2(codecCtx, frame, &frameFinished, &packet);
      if (frameFinished) {
        sws_scale(swsCtx, (const uint8_t* const*)frame->data,
                  frame->linesize, 0, mHeight,
                  frameRGB->data, frameRGB->linesize);
        sampleIndex++;
        av_free_packet(&packet);
        return sampleIndex;
      }
    }
    av_free_packet(&packet);
  }
  return sampleIndex;
}
