#pragma once
extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
}
#include <string>

class UGLVideo {
  static bool avInitialied;
  bool loaded;
  int mWidth;
  int mHeight;
  AVPixelFormat mTargetPixelFormat;
  AVFormatContext* fmtContext;
  int videoStream;
  AVCodecContext* codecCtx;
  AVCodecContext* codecCtx1;
  AVCodec*           codec;
  AVFrame*          frame;
  AVFrame*       frameRGB;
  uint8_t* bytes;
  SwsContext*     swsCtx;
  AVPacket        packet;
  int      frameFinished;
  int       sampleIndex;


public:
  UGLVideo (std::string path);
  virtual ~UGLVideo ();

  int64_t GetDurationMs();
  unsigned int GetSampleCount();
  unsigned int GetWidth();
  unsigned int GetHeight();

  void* GetBuffer();
  unsigned int GetBufferSize();
  int ReadNextSample();
};
