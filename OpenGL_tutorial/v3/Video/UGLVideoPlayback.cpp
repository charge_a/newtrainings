#include "UGLVideoPlayback.h"

UGLVideoPlayback::UGLVideoPlayback()
{
  mVideo = new UGLVideo("/home/ujjaval/Documents/Bento4-SDK-1-5-0-614.x86_64-unknown-linux/1017662.mp4");
  glGenTextures(1, &mTexture);
}

UGLVideoPlayback::~UGLVideoPlayback()
{
  delete mVideo;
}

int UGLVideoPlayback::GetWidth()
{
  if (mVideo) {
    return mVideo->GetWidth();
  }
  return 0;
}

int UGLVideoPlayback::GetHeight()
{
  if (mVideo) {
    return mVideo->GetHeight();
  }
  return 0;
}

int64_t UGLVideoPlayback::GetDurationMs()
{
  if (mVideo) {
    return mVideo->GetDurationMs() % 1000;
  }
  return 0;
}

GLuint UGLVideoPlayback::GetTexture()
{
  return mTexture;
}

bool UGLVideoPlayback::Render(GLuint texUnit, GLuint texLocation)
{
  glActiveTexture(texUnit);
  glBindTexture(GL_TEXTURE_2D, mTexture);
  glUniform1i(texLocation, texUnit - GL_TEXTURE0);
  if (mVideo->ReadNextSample()) {
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
                  mVideo->GetWidth(),
                  mVideo->GetHeight(),
                  0, GL_RGB, GL_UNSIGNED_BYTE,
                  mVideo->GetBuffer());
    glGenerateMipmap(GL_TEXTURE_2D);
    return true;
  }
  return false;
}
