#pragma once
#include "UGLVideo.h"
#include <GL/glew.h>

class UGLVideoPlayback {
private:
  UGLVideo* mVideo;
  GLuint mTexture;

public:
  UGLVideoPlayback ();
  virtual ~UGLVideoPlayback ();

  int GetWidth();
  int GetHeight();
  int64_t GetDurationMs();
  GLuint GetTexture();

  bool Render(GLuint texUnit, GLuint texLocation);
};
