#include "../UGLImpl.h"
#include "UGLVideoPlayback.h"

namespace UGL {

  struct UGLVideoSceneObjects {
    UGLVideoPlayback *videoPlayback;
    OGLShader *shader;
  };

  UGLVideoScene::UGLVideoScene() {
    mO = new UGLVideoSceneObjects();
  }

  void UGLVideoScene::setup() {
    mO->videoPlayback = new UGLVideoPlayback();
    mO->shader = new OGLShader("v3/Video/shader.vs", "v3/Video/shader.fs");
    setupVertex(0, VERTEX_SQUARE_POS);
    setupVertex(1, VERTEX_SQUARE_TEX);
  }

  void UGLVideoScene::render() {
    float w = mO->videoPlayback->GetWidth();
    float h = mO->videoPlayback->GetHeight();
    m = glm::scale(id, glm::vec3(1.0, h/w, 1.0));
    UGLBaseRenderer::render(*mO->shader);
  }

  void UGLVideoScene::defineAdditionalUniforms(OGLShader &shader) {
    mO->videoPlayback->Render(GL_TEXTURE0, glGetUniformLocation(shader.pu, "image"));
  }

} /* UGL */
