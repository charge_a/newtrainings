extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
}

int main(int argc, char const *argv[]) {
  av_register_all();
  AVFormatContext *pFormatCtx = NULL;

  if (avformat_open_input(&pFormatCtx, "/home/ujjaval/Documents/Bento4-SDK-1-5-0-614.x86_64-unknown-linux/1017662.mp4", NULL, NULL) != 0) {
    return -1;
  }

  if (avformat_find_stream_info(pFormatCtx, NULL) != 0) {
    return -1;
  }

  // av_dump_format(pFormatCtx, 0, "/home/ujjaval/Documents/Bento4-SDK-1-5-0-614.x86_64-unknown-linux/1017662.mp4", 0);

  int i;
  AVCodecContext *pCodecCtxOrig = NULL;
  int videoStream = -1;
  for (i=0; i < pFormatCtx->nb_streams; i++) {
    if (pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
      videoStream = i;
      break;
    }
  }
  if (videoStream == -1) {
    return -1;
  }

  // contains information about the codec the stream is using.
  AVCodecContext *pCodecCtx = pFormatCtx->streams[videoStream]->codec;
  AVCodec *pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
  if (!pCodec) {
    fprintf(stderr, "Unsupported Codec!\n");
    return -1;
  }
  pCodecCtxOrig = pCodecCtx;
  pCodecCtx = avcodec_alloc_context3(pCodec);
  if (avcodec_copy_context(pCodecCtx, pCodecCtxOrig)) {
    fprintf(stderr, "Couldn't copy context!\n" );
    return -1;
  }
  if (avcodec_open2(pCodecCtx, pCodec, NULL) < 0) {
    return -1;
  }

  // extracting frame
  AVFrame *pFrame = NULL;
  pFrame = av_frame_alloc(); // av_frame_alloc requires libavutils
  if (pFrame == NULL) {
    return -1;
  }
  AVFrame *pFrameRGB = NULL;
  pFrameRGB = av_frame_alloc(); // av_frame_alloc requires libavutils
  if (pFrameRGB == NULL) {
    return -1;
  }
  fprintf(stdout, "buffer size: %dx%d\n", pCodecCtx->width, pCodecCtx->height);
  uint8_t *buffer = NULL;
  int numBytes = avpicture_get_size(PIX_FMT_RGB24, pCodecCtx->width, pCodecCtx->height);
  buffer = (uint8_t *)malloc(sizeof(uint8_t) * numBytes);
  avpicture_fill((AVPicture *)pFrameRGB, buffer, PIX_FMT_RGB24, pCodecCtx->width, pCodecCtx->height);

  // Now stream.
  SwsContext *sws_ctx = sws_getContext(pCodecCtx->width,
                                       pCodecCtx->height,
                                       pCodecCtx->pix_fmt,
                                       pCodecCtx->width,
                                       pCodecCtx->height,
                                       PIX_FMT_RGB24,
                                       SWS_BILINEAR,
                                       NULL, NULL, NULL);
  i = 0;
  AVPacket packet;
  int frameFinished;
  while (av_read_frame(pFormatCtx, &packet) >= 0) {
    if (packet.stream_index == videoStream) {
      // decode frame
      avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);
      if (frameFinished) {
        sws_scale(sws_ctx, (const uint8_t *const *)pFrame->data,
                  pFrame->linesize, 0, pCodecCtx->height,
                  pFrameRGB->data, pFrameRGB->linesize);
        ++i;
        // use pFrameRGB->data
        fprintf(stdout, "[%d] linesize: %d, %d, %d\n", i, pFrameRGB->linesize[0], pFrameRGB->linesize[1], pFrameRGB->linesize[2]);
        // linesize: stride for planer data
        // for e.g. if frame is W*H, then
        // -- for YUV420, Y = data[0], U = data[1], Y = data[2]
        //                linesize[0] = W, linesize[1] = linesize[2] = W/2
        // -- for RGB24, there is only 1 plane, data[0]
        //                linesize[0] = W * 3 (channels)
        //

      }
    }
    av_free_packet(&packet);
    if (i > 100) {
      break;
    }
  }

  // Cleanup
  av_free(buffer);
  av_free(pFrameRGB);
  av_free(pFrame);
  avcodec_close(pCodecCtx);
  avcodec_close(pCodecCtxOrig);
  avformat_close_input(&pFormatCtx);


  return 0;
}
