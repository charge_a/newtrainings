#version 330 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 tex;

out vec2 TexCoords;
uniform mat4 m, v, p;

void main()
{
  gl_Position = p*v*m*vec4(pos, 1);
  TexCoords = tex;
}
