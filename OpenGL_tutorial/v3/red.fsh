#version 300 es

in highp vec4 Pos;
out highp vec4 color;

void main()
{
  color = Pos;
}
