# OPENGL Trainings #

In April 2017, I began my journey to learn OpenGL. Being a beginner without any prior experience, it was a challenging yet rewarding task. I scoured the Internet for the best tutorials and easy to get started ways to learn OpenGL, and as a result, founded this repository containing my learnings.

### What is this repository for? ###

Most of the material here has come directly from https://learnopengl.com - an excellent tutorial for beginners trying to make headway into Graphics and OpenGL.

**OpenGL_tutorial/:** This folder contains my attempts of the OpenGL learning. Coding is entirely in C++, with several third-party and open-source libraries mentioned later here. It is unorganized. And that's by design. This repository was initially meant to record my own journey and evolution as I navigated through the tutorial above. It's not meant to substitute the original tutorial, so feel free to go through https://learnopengl.com and the repository mentioned there. Some important files are -

- `Makefile`: use `make out` to build and run the code. use `make clean` to delete all object files.
- `start.cpp`: this function contains the `main()` method.
- `v2/`, `v3/`: these folders depict the code evolution. I first started from a flat structure, with a primitive `basecode/` folder containing base classes and helper functions to build an OpenGL rendering unit. As I moved forward with more detailed tutorial, I began refactoring code to a more structured way, and when I moved to complex windowing application, I redesigned major parts in `v3/`.

**MVC_Sample_GL/**: This is my newest attempt to recreate a windowing application. A windowing application contains a lot of components, such as UI, Event management, resource management, backend code, etc. Many successful windowing frameworks exist today, like GTK, Windows, etc. which apply many design patterns (MVC, Builder, Command, etc.) to not just create a high performance platform, but also an easily extensible and adaptive application framework. Why did I reinvent the wheel? For learning purposes entirely. Being a new project, it's a bit more organized and clean. Some important files -

- `Makefile`: use `make out` to build and run the code. use `make clean` to delete all object files.
- `main.cpp`: contains `main()` where the program starts. Unlike **OpenGL_tutorial**, this contains code specifically for the windowing application framework.
- `base/`: base classes for a "View" - basic building block for all UI elements, containing event callbacks and base rendering methods. `base/graphics/` contains all low-level rendering code.
- `ui_elements/`: contains derived classes from "View" for specialized functions - Button, ScrollView, etc. It's purpose is to create sharable components to be used in the app. It's "work in development".
- `app/`: this folder is meant to code up an app using the building blocks above. Striving for extensibility, it contains a single folder called \<app_name\>, containing a ViewController. It's "work in development".

### What does this code really do?

https://learnopengl.com does a great job to tell beforehand what the final output would look like before beginning a new chapter. In the same spirit, below are some screenshots and link to videos to what you can accomplish by just commenting some lines in the `Makefile` -

##### Blending

[Check out the demo video here](OpenGL_tutorial/demos/video/Blending.mp4)
![123](OpenGL_tutorial/demos/screenshot/Blending.jpg)

##### Blinn-Phong lighting

[Check out the demo video here](OpenGL_tutorial/demos/video/Blinn-Phong lighting.mp4)
![123](OpenGL_tutorial/demos/screenshot/Blinn-Phong%20lighting.jpg)

##### Geometry Shader

[Check out the demo video here](OpenGL_tutorial/demos/video/Geometry Shader.mp4)
![123](OpenGL_tutorial/demos/screenshot/Geometry%20Shader.jpg)

##### Instancing

[Check out the demo video here](OpenGL_tutorial/demos/video/Instancing.mp4)
![123](OpenGL_tutorial/demos/screenshot/Instancing.jpg)

##### Multiple light sources

[Check out the demo video here](OpenGL_tutorial/demos/video/Multi-light.mp4)
![123](OpenGL_tutorial/demos/screenshot/Multi-light.jpg)

##### Particle System - 1

[Check out the demo video here](OpenGL_tutorial/demos/video/Particle System.mp4)
![123](OpenGL_tutorial/demos/screenshot/Particle%20System.jpg)

##### Particle System - 2

[Check out the demo video here](OpenGL_tutorial/demos/video/Particle System-2.mp4)
![123](OpenGL_tutorial/demos/screenshot/Particle%20System-2.jpg)

##### Refraction

[Check out the demo video here](OpenGL_tutorial/demos/video/Refraction.mp4)
![123](OpenGL_tutorial/demos/screenshot/Refraction.jpg)



### How do I get set up?

The steps below are for a linux environment, though you could easily extend them to Windows (Cygwin) or Mac (homebrew).

#### Install glfw3

`sudo apt-get install libglfw3-dev`

If you still get */usr/bin/ld: cannot find -lglfw3*, change *-lglfw3* to *-lglfw*.

#### Install glew

`sudo apt-get update`

`sudo apt-get install libglew-dev`

#### Install freetype

`sudo apt-get install libfreetype6-dev`

#### Build

`make out -j4`

#### Clean

`make clean` 

#### [Optional] Install AVCodec

`sudo apt-get install libavcodec-dev`

`sudo apt-get install libavformat-dev`

`sudo apt-get install libswscale-dev`

### Who do I talk to? ###

* You can contact me at ujjaval.1991@gmail.com. I am the sole developer and open to suggestions for improvement or taking it forward to some next level.

  ​